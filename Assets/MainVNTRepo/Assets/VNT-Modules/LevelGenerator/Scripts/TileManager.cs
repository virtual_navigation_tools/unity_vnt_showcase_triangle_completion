﻿using System.Collections.Generic;
using UnityEngine;

namespace unibi.vnt.levelgen
{
    public class TileManager : MonoBehaviour
    {
        #region Fields

        private Dictionary<int, FloorTile> tiles = new();

        #endregion Fields

        #region Properties

        public Dictionary<int, FloorTile> Tiles { get => tiles; }

        #endregion Properties

        #region Public Methods

        /// <summary>
        /// Clears out all tiles in the manager. By default also destroys all associated tile
        /// objects in the scene.
        /// </summary>
        /// <param name="destroyObjects">
        /// whether to also destroy all associated objects in the scene
        /// </param>
        public void ClearTiles(bool destroyObjects = true)
        {
            /* Since removing tiles involves manipulating the tile dict,
             * we cannot delete tiles while iterating through the tile dict.
             * Therefore, we  collect the IDs first,
             * and then pass on the IDs for removal in a second step.
            */
            var IDs = new HashSet<int>();
            foreach (var tile in tiles)
            {
                IDs.Add(tile.Value.ID);
            }
            DestroyTiles(IDs, destroyObjects);
            tiles = new();
        }

        /// <summary>
        /// Creates a new tile to be managed, with given dimensions and an associated scene object.
        /// </summary>
        /// <param name="dimensions"> the dimensions of the tile in the X-Z plane </param>
        /// <param name="tileObject"> the GameObject representing the tile in the scene </param>
        /// <returns> </returns>
        public FloorTile CreateTile(Rect dimensions, GameObject tileObject)
        {
            var newTile = new FloorTile(dimensions, tileObject.GetInstanceID(), tileObject);
            AddTile(newTile);
            return newTile;
        }

        /// <summary>
        /// Removes a tile from the list of tracked tiles, optionally also destroys its associated
        /// object in the scene.
        /// </summary>
        /// <param name="ID"> the ID of the tile to remove </param>
        /// <param name="destroyAttachedObject">
        /// whether to also destroy its associated object in the scene
        /// </param>
        public void DestroyTile(int ID, bool destroyAttachedObject = false)
        {
            if (tiles.ContainsKey(ID))
            {
                if (destroyAttachedObject)
                {
                    Destroy(tiles[ID].TileObject);
                }
                RemoveTileFromDict(ID);
            }
        }

        /// <summary>
        /// Removes tiles from the list of tracked tiles, optionally also destroys their associated
        /// objects in the scene.
        /// </summary>
        /// <param name="IDs"> the IDs of the tiles to remove </param>
        /// <param name="destroyAttachedObject">
        /// whether to also destroy its associated object in the scene
        /// </param>
        public void DestroyTiles(HashSet<int> IDs, bool destroyAttachedObject = false)
        {
            foreach (var ID in IDs)
            {
                DestroyTile(ID, destroyAttachedObject);
            }
        }

        /// <summary>
        /// Makes a tile visible or invisble in the scene.
        /// </summary>
        /// <param name="ID"> the ID of the tile </param>
        /// <param name="state"> whether to show or hide the tile (visible=true) </param>
        public void SetTileActive(int ID, bool state)
        {
            if (tiles.ContainsKey(ID))
            {
                tiles[ID].TileObject.SetActive(state);
            }
        }

        #endregion Public Methods

        #region Private Methods

        private void AddTile(FloorTile tile)
        {
            tiles.Add(tile.ID, tile);
        }

        private void RemoveTileFromDict(int ID)
        {
            if (tiles.ContainsKey(ID))
            {
                tiles.Remove(ID);
            }
        }

        #endregion Private Methods
    }
}