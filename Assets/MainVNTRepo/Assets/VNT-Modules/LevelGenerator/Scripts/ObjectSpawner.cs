﻿using System;
using System.Collections.Generic;
using unibi.vnt.util;
using UnityEngine;

namespace unibi.vnt.levelgen
{
    public class ObjectSpawner : MonoBehaviour, ILoggable
    {
        #region Fields

        public static EventHandler FloorTilesPlaced;

        [SerializeField]
        private bool isDebug = false;

        [SerializeField]
        private GameObject landmarkPrefab = null;

        [SerializeField]
        private List<GameObject> sceneObjectPrefabs = new();

        [Range(0, 100)]
        [SerializeField]
        private int sceneObjectsPerTile = 0;

        private List<GameObject> spawnedLandmarks = new();
        private List<GameObject> spawnedObjects = new();

        private Vector3 tileBounds;

        [SerializeField]
        private TileManager tileManager = null;

        [SerializeField]
        private GameObject tilePrefab = null;

        [SerializeField]
        private VisibilityController visibilityController = null;

        [SerializeField]
        private GameObject worldParent = null;

        #endregion Fields

        #region Properties

        public ICustomLogger Logger { get; set; }
        public List<GameObject> SpawnedObjects { get => spawnedObjects; }
        public Vector3 TileBounds { get => tileBounds; }
        public GameObject TilePrefab { get => tilePrefab; }

        #endregion Properties

        #region Public Methods

        /// <summary>
        /// Destroys all landmark objects spawned by this Spawner. Also removes those objects from
        /// being tracked by the attached VisibilityController.
        /// </summary>
        public void ClearLandmarks()
        {
            foreach (var obj in spawnedLandmarks)
            {
                spawnedObjects.Remove(obj);
                visibilityController.RemoveObjectsFromTracking(obj);
                Destroy(obj);
            }
            spawnedLandmarks.Clear();
        }

        /// <summary>
        /// Sets up a new floor to be managed by the attached TileManager. Also adds it to the
        /// attached VisibilityController. Spawns a new instance of the attached tilePrefab to
        /// represent the tile in the scene.
        /// </summary>
        /// <param name="xMin"> lower tile bound along the X axis </param>
        /// <param name="xMax"> upper tile bound along the X axis </param>
        /// <param name="zMin"> lower tile bound along the Z axis </param>
        /// <param name="zMax"> upper tile bound along the Z axis </param>
        /// <returns> </returns>
        public FloorTile CreateFloorTile(float xMin, float xMax, float zMin, float zMax)
        {
            Rect square = new Rect(xMin, xMax, zMin, zMax);
            if (isDebug)
            {
                Logger.Log($"creating new Tile at: {square.PrintDimensions()}", LogType.Log, this);
            }
            GameObject tileObj = SpawnNewTileObject();
            FloorTile newTile = tileManager.CreateTile(square, tileObj);
            PlaceObjectsOnTile(newTile);
            visibilityController.AddObjectsToTrack(newTile.TileObject);
            return newTile;
        }

        /// <summary>
        /// Fills the area given by "dimensions" with new tiles. Also adds scene objects (e.g. tufts
        /// of grass) at random positions on each tile. Also adds each new floor tile to the VisibilityController.
        /// </summary>
        /// <param name="dimensions"> the overall bounds of the floor area </param>
        /// <param name="objectsPerSqM"> how many objects to place per square meter of tile </param>
        public void CreateTiledFloor(Rect dimensions, float objectsPerSqM)
        {
            var edgeLengthHorz = tileBounds.x;
            var edgeLengthVert = tileBounds.z;

            sceneObjectsPerTile = (int)Math.Floor(objectsPerSqM * edgeLengthHorz * edgeLengthVert);
            int nHorz = (int)Math.Ceiling(dimensions.XLength / edgeLengthHorz);
            int nVert = (int)Math.Ceiling(dimensions.ZLength / edgeLengthVert);
            if (isDebug)
            {
                Logger.Log($"tiles planned: [x: {nHorz}, z: {nVert}]", LogType.Log, this);
            }

            float halfWidthX = (nHorz * edgeLengthHorz / 2);
            float halfWidthZ = (nVert * edgeLengthVert / 2);
            if (isDebug)
            {
                Logger.Log($"Halfwidth in world units: [x: {halfWidthX}, z: {halfWidthZ}]", LogType.Log, this);
            }

            float xMin = dimensions.Center.x - halfWidthX;
            float xMax = xMin + edgeLengthHorz;

            for (int i = 0; i < nHorz; i++)
            {
                float zMin = dimensions.Center.z - halfWidthZ;
                float zMax = zMin + edgeLengthVert;
                for (int j = 0; j < nVert; j++)
                {
                    _ = CreateFloorTile(xMin, xMax, zMin, zMax);

                    zMin += edgeLengthVert;
                    zMax += edgeLengthVert;
                }

                xMin += edgeLengthHorz;
                xMax += edgeLengthHorz;
            }
            visibilityController.AddObjectsToTrack(spawnedObjects);
            FloorTilesPlaced?.Invoke(this, EventArgs.Empty);
        }

        /// <summary>
        /// Spawns new instances of the attached landmarkPrefab at given positions. Optionally
        /// randomises the Y-rotation of placed objects.
        /// </summary>
        /// <param name="positions"> a list of spawn positions </param>
        /// <param name="rotateRandomly"> whether to randomise object Y-rotation on spawn </param>
        public void PlaceLandmarksAt(List<Vector3> positions, bool rotateRandomly = true)
        {
            foreach (var pos in positions)
            {
                var rot = Quaternion.identity;
                if (rotateRandomly)
                {
                    rot = Quaternion.Euler(0, UnityEngine.Random.Range(0, 359), 0);
                }
                var obj = Instantiate(landmarkPrefab, pos, rot);
                obj.transform.parent = worldParent.transform;
                spawnedObjects.Add(obj);
                spawnedLandmarks.Add(obj);
            }
        }

        /// <summary>
        /// Spawns instances of the attached sceneObjectPrefabs on a given tile.
        /// </summary>
        /// <param name="tile"> the tile object on which to spawn the objects </param>
        public void PlaceObjectsOnTile(FloorTile tile)
        {
            for (int i = 0; i < sceneObjectsPerTile; i++)
            {
                var xPos = UnityEngine.Random.Range(-tile.Dimensions.XLength / 2, tile.Dimensions.XLength / 2);
                var zPos = UnityEngine.Random.Range(-tile.Dimensions.ZLength / 2, tile.Dimensions.ZLength / 2);
                int randomIndex = UnityEngine.Random.Range(0, sceneObjectPrefabs.Count);

                _ = SpawnObject(sceneObjectPrefabs[randomIndex], tile.Dimensions.Center + new Vector3(xPos, 0, zPos),
                    Quaternion.Euler(0, UnityEngine.Random.Range(0, 359), 0),
                    tile.TileObject.transform);
                if (isDebug)
                {
                    Logger.Log($"creating scene object on tile {tile.ID} at: [x: {xPos}, z: {zPos}]", LogType.Log, this);
                }
            }
        }

        /// <summary>
        /// Resets the spawner by destroying all spawned objects, clearing the tiles in the attached
        /// TileManager and removing all old objects from the attached VisibilityController.
        /// </summary>
        public void ResetForNextTrial()
        {
            visibilityController.RemoveObjectsFromTracking(spawnedObjects);
            foreach (var item in spawnedObjects)
            {
                Destroy(item);
            }
            tileManager.ClearTiles();
            spawnedLandmarks.Clear();
        }

        #endregion Public Methods

        #region Private Methods

        private void Awake()
        {
            if (worldParent == null)
            {
                worldParent = Instantiate(new GameObject("worldParent"));
                Logger.Log("no container object found, creating new container object", LogType.Warning, this);
            }
        }

        private void OnEnable()
        {
            tileBounds = tilePrefab.GetComponentInChildren<Renderer>().bounds.size;
        }

        private GameObject SpawnNewTileObject()
        {
            GameObject tileObj = SpawnObject(tilePrefab, new Vector3(), Quaternion.identity, worldParent.transform);
            return tileObj;
        }

        private GameObject SpawnObject(GameObject prefab, Vector3 position, Quaternion rotation, Transform parent)
        {
            var obj = Instantiate(prefab, position,
                rotation);
            obj.transform.parent = parent;
            obj.SetActive(true);
            spawnedObjects.Add(obj);
            return obj;
        }

        #endregion Private Methods
    }
}