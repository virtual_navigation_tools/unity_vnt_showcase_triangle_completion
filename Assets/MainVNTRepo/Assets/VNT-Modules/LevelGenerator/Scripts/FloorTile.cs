﻿using UnityEngine;

namespace unibi.vnt.levelgen
{
    public class FloorTile
    {
        #region Public Constructors

        /// <summary>
        /// Creates a new FloorTile object.
        /// </summary>
        /// <param name="dimensions"> the dimensions of the tile in the X-Z plane </param>
        /// <param name="id"> a unique identifier for the tile </param>
        /// <param name="tileObject"> the GameObject representing the tile in the scene </param>
        public FloorTile(Rect dimensions, int id, GameObject tileObject)
        {
            Dimensions = dimensions;
            ID = id;
            TileObject = tileObject;
            AlignTileObject();
        }

        #endregion Public Constructors

        #region Properties

        public Rect Dimensions { get; private set; }
        public int ID { get; private set; }
        public GameObject TileObject { get; private set; }

        #endregion Properties

        #region Private Methods

        private void AlignTileObject()
        {
            TileObject.transform.position = Dimensions.Center;
        }

        #endregion Private Methods
    }
}