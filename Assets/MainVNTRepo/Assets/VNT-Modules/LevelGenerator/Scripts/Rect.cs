﻿using UnityEngine;

namespace unibi.vnt.levelgen
{
    /// <summary>
    /// A convenience class to define rectangles in the X-Z plane.
    /// </summary>
    public class Rect
    {
        #region Public Constructors

        public Rect(float xMin, float xMax, float zMin, float zMax)
        {
            XMin = xMin;
            XMax = xMax;
            ZMin = zMin;
            ZMax = zMax;
        }

        #endregion Public Constructors

        #region Properties

        public float Area { get => XLength * ZLength; }

        /// <summary>
        /// The centroid of the rectangle in the X-Z plane.
        /// </summary>
        public Vector3 Center
        {
            get => new Vector3
                (
                    XMin + (XLength / 2),
                    0,
                    ZMin + (ZLength / 2)
                );
        }

        public float XLength { get => XMax - XMin; }
        public float XMax { get; private set; }
        public float XMin { get; private set; }
        public float ZLength { get => ZMax - ZMin; }
        public float ZMax { get; private set; }
        public float ZMin { get; private set; }

        #endregion Properties

        #region Public Methods

        /// <summary>
        /// Print a human-readble version of the Rect.
        /// </summary>
        /// <returns> logstring </returns>
        public string PrintDimensions()
        {
            return string.Format("X: {0} <-> {1}, Y: {2} <-> {3}, Center: {4}", XMin, XMax, ZMin, ZMax, Center);
        }

        #endregion Public Methods
    }
}