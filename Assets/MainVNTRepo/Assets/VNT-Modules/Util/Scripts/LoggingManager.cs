﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace unibi.vnt.util
{
    public class LoggingManager : MonoBehaviour, ICustomLogger, ILoggable
    {
        #region Fields

        private List<ICustomLogger> activeLoggers = new();

        [SerializeField]
        private bool autoStart = true;

        private List<ILoggable> loggables = new();

        #endregion Fields

        #region Properties

        public ICustomLogger Logger { get; set; }

        #endregion Properties

        #region Public Methods

        /// <summary>
        /// Attaches this Logger to all Loggables in the scene.
        /// </summary>
        public void AddAllLoggablesInScene()
        {
            loggables.AddRange(FindInterfaces.FindAllInterfacesInScene<ILoggable>());
            AttachManagerToLoggables();
            foreach (var loggable in loggables)
            {
                Logger.Log(string.Format("added new loggable: {0}", loggable.GetType()), LogType.Log, this);
            }
        }

        /// <summary>
        /// Sets up all loggers attached to the same GameObject as this Logger to receive all logs
        /// this Logger receives.
        /// </summary>
        public void AddAttachedLoggers()
        {
            var loggers = gameObject.GetInterfaces<ICustomLogger>().ToList();
            loggers.Remove(this);
            activeLoggers.AddRange(loggers);
            SetActive(true);
            foreach (var logger in loggers)
            {
                logger.Log(string.Format("added new logger: {0}", logger.GetType()), LogType.Log, this);
            }
        }

        /// <summary>
        /// Remove all Loggables attached to this Logger.
        /// </summary>
        public void ClearLoggables()
        {
            loggables.Clear();
        }

        public void Log(string logString, LogType type, object sender, string stackTrace = "", string tag = "")
        {
            foreach (var logger in activeLoggers)
            {
                logger.Log(logString, type, sender, stackTrace, tag);
            }
        }

        public void SetActive(bool state)
        {
            if (activeLoggers.Contains(this))
            {
                throw new System.Exception("loggingmanager tries to activate itself, this should not be possible!");
            }
            foreach (ICustomLogger logger in activeLoggers)
            {
                logger.SetActive(state);
            }
        }

        #endregion Public Methods

        #region Private Methods

        private void AttachManagerToLoggables()
        {
            foreach (ILoggable loggable in loggables)
            {
                loggable.Logger = this;
            }
        }

        private void Awake()
        {
            if (autoStart)
            {
                AddAttachedLoggers();
                AddAllLoggablesInScene();
                AttachManagerToLoggables();
            }
            SetActive(true);
        }

        private void OnDestroy()
        {
            SetActive(false);
        }

        #endregion Private Methods
    }
}