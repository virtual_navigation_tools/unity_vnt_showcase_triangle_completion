﻿namespace unibi.vnt.util
{
    public static class VNTLogFormatter
    {
        #region Properties

        public static string Example_Tag { get => "[<b><color=#92B168>VNT_Example</color></b>]"; }
        public static string VNT_Tag { get => "[<b><color=#92B168>VNT</color></b>]"; }

        #endregion Properties

        #region Public Methods

        public static string FormatVNT(string message, object sender, string tag = "")
        {
            if (tag == "")
            {
                tag = VNT_Tag;
            }
            return string.Format("{0} [<color=#cccccc>{1}</color>]: {2}", tag, sender, message);
        }

        #endregion Public Methods
    }
}