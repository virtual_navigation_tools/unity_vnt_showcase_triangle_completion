﻿using UnityEngine;

namespace unibi.vnt.util
{
    /// <summary>
    /// A simple script to control a player object via CharacterController.
    /// </summary>
    public class SimplePlayerMover : MonoBehaviour
    {
        #region Fields

        [SerializeField]
        private CharacterController characterController = null;

        [Tooltip("In Degrees Per Second"), SerializeField]
        private float MaxAngularVelocity = 3.0F;

        [Tooltip("In Units Per Second"), SerializeField]
        private float MaxForwardVelocity = 6.0F;

        #endregion Fields

        #region Private Methods

        private void Update()
        {
            Vector3 forward = transform.TransformDirection(Vector3.forward);
            float curSpeed = MaxForwardVelocity * Input.GetAxis("Vertical");
            transform.Rotate(0, Input.GetAxis("Horizontal") * MaxAngularVelocity * Time.deltaTime, 0);
            characterController.SimpleMove(forward * curSpeed);
        }

        #endregion Private Methods
    }
}