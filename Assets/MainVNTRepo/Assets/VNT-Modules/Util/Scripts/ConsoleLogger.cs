﻿using UnityEngine;

namespace unibi.vnt.util
{
    public class ConsoleLogger : MonoBehaviour, ICustomLogger
    {
        #region Fields

        [SerializeField]
        private bool isActive = false;

        #endregion Fields

        #region Public Methods

        public void Log(string logString, LogType type, object sender, string tag = "", string stackTrace = "")
        {
            if (!isActive)
            {
                return;
            }

            // how to deal with stack trace if desired:
            //if (stackTrace.Length == 0)
            //{
            //    stackTrace = new System.Diagnostics.StackTrace().ToString();
            //}

            switch (type)
            {
                case LogType.Error:
                    Debug.LogError(VNTLogFormatter.FormatVNT(logString, sender, tag));
                    break;

                case LogType.Assert:
                    Debug.LogAssertion(VNTLogFormatter.FormatVNT(logString, sender, tag));
                    break;

                case LogType.Warning:
                    Debug.LogWarning(VNTLogFormatter.FormatVNT(logString, sender, tag));
                    break;

                case LogType.Log:
                    Debug.Log(VNTLogFormatter.FormatVNT(logString, sender, tag));
                    break;

                case LogType.Exception:
                    Debug.LogException(new System.Exception(VNTLogFormatter.FormatVNT(logString, sender, tag)));
                    break;

                default:
                    break;
            }
        }

        public void SetActive(bool state)
        {
            isActive = state;
        }

        #endregion Public Methods
    }
}