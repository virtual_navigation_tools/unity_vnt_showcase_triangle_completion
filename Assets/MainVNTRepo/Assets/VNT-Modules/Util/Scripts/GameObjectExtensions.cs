using System;
using System.Linq;
using UnityEngine;

namespace unibi.vnt.util
{
    /// <summary>
    /// This in extension class for the Unity GameObject class. This means the methods defined here
    /// can be called on any gameObject. partially based on: https://forum.unity.com/threads/how-to-get-all-components-on-an-object-that-implement-an-interface.101028/
    /// </summary>
    public static class GameObjectExtensions
    {
        #region Public Methods

        /// <summary>
        /// Returns the first monobehaviour that is of the interface type (casted to T)
        /// </summary>
        /// <typeparam name="T"> interface type </typeparam>
        /// <param name="gObj"> </param>
        /// <returns> </returns>
        public static T GetInterface<T>(this GameObject gObj)
        {
            if (!typeof(T).IsInterface)
                throw new SystemException("Specified type is not an interface!");
            return gObj.GetInterfaces<T>().FirstOrDefault();
        }

        /// <summary>
        /// Returns the first instance of the monobehaviour that is of the interface type T (casted
        /// to T)
        /// </summary>
        /// <typeparam name="T"> interface type </typeparam>
        /// <param name="gObj"> </param>
        /// <returns> </returns>
        public static T GetInterfaceInChildren<T>(this GameObject gObj)
        {
            if (!typeof(T).IsInterface)
                throw new SystemException("Specified type is not an interface!");
            return gObj.GetInterfacesInChildren<T>().FirstOrDefault();
        }

        /// <summary>
        /// Returns all monobehaviours (casted to T)
        /// </summary>
        /// <typeparam name="T"> interface type </typeparam>
        /// <param name="gObj"> </param>
        /// <returns> </returns>
        public static T[] GetInterfaces<T>(this GameObject gObj)
        {
            if (!typeof(T).IsInterface)
                throw new SystemException("Specified type is not an interface!");
            var mObjs = gObj.GetComponents<MonoBehaviour>();

            return (from a in mObjs where a.GetType().GetInterfaces().Any(k => k == typeof(T)) select (T)(object)a).ToArray();
        }

        /// <summary>
        /// Gets all monobehaviours in children that implement the interface of type T (casted to T)
        /// </summary>
        /// <typeparam name="T"> interface type </typeparam>
        /// <param name="gObj"> </param>
        /// <returns> </returns>
        public static T[] GetInterfacesInChildren<T>(this GameObject gObj)
        {
            if (!typeof(T).IsInterface)
                throw new SystemException("Specified type is not an interface!");

            var mObjs = gObj.GetComponentsInChildren<MonoBehaviour>();

            return (from a in mObjs where a.GetType().GetInterfaces().Any(k => k == typeof(T)) select (T)(object)a).ToArray();
        }

        /// <summary>
        /// Tries to find an implementation of given Interface on children of provided GameObject.
        /// Returns either first implementation found or null. If no Implementation is found, an
        /// error is printed to console.
        /// </summary>
        /// <typeparam name="T"> interface type </typeparam>
        /// <param name="gameObject"> </param>
        /// <returns> </returns>
        public static T GrabImplementation<T>(this GameObject gameObject)
        {
            T implementation = gameObject.GetInterfaceInChildren<T>();
            if (implementation == null)
            {
                Debug.LogError($"Trying to grab an implementation of {typeof(T)}, but none was found on {gameObject}!");
            }
            return implementation;
        }

        /// <summary>
        /// Sets all renderers attached to GO and its children to given state.
        /// </summary>
        /// <param name="gObj"> </param>
        /// <param name="setEnabled"> </param>
        public static void ToggleAllRenderersInChildren(this GameObject gObj, bool setEnabled)
        {
            var renderers = gObj.GetComponentsInChildren<Renderer>();
            foreach (var renderer in renderers)
            {
                renderer.enabled = setEnabled;
            }
        }

        #endregion Public Methods
    }
}