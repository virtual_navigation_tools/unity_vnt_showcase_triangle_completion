using System.Collections.Generic;
using TMPro;
using UnityEngine;

namespace unibi.vnt.util
{
    public class LogToUI : MonoBehaviour
    {
        #region Fields

        private Queue<string> currentMessages = new();

        [Range(1, 10), SerializeField]
        private int numMessagesVisible = 5;

        [SerializeField]
        private bool PrintDebugLogs = true;

        [SerializeField]
        private TMP_Text textContainer = null;

        #endregion Fields

        #region Public Methods

        /// <summary>
        /// Prints a string to the attached Text Container.
        /// </summary>
        /// <param name="logString"> </param>
        /// <param name="stack"> </param>
        /// <param name="type"> </param>
        public void Log(string logString, string stack, LogType type)
        {
            var excessCount = currentMessages.Count - numMessagesVisible;
            if (excessCount < 0)
            {
                currentMessages.Enqueue(logString);
            }
            else
            {
                for (int i = 0; i < excessCount + 1; i++)
                {
                    currentMessages.Dequeue();
                }
                currentMessages.Enqueue(logString);
            }
            SetText(currentMessages);
        }

        #endregion Public Methods

        #region Private Methods

        private void OnDisable()
        {
            UnsubscribeFromApplicationLogMessages();
        }

        private void OnEnable()
        {
            if (PrintDebugLogs)
            {
                SubscribeToApplicationLogMessages();
            }
        }

        private void SetText(Queue<string> messages)
        {
            string uiText = string.Join("\n", messages.ToArray());
            textContainer.SetText(uiText);
        }

        private void SubscribeToApplicationLogMessages()
        {
            Application.logMessageReceived += Log;
        }

        private void UnsubscribeFromApplicationLogMessages()
        {
            Application.logMessageReceived -= Log;
        }

        #endregion Private Methods
    }
}