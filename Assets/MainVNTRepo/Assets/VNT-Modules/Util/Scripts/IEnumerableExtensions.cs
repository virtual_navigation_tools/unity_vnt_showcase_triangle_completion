﻿using System.Collections.Generic;
using System.Linq;

namespace unibi.vnt.util
{
    // based on the accepted answer here: https://stackoverflow.com/questions/273313/randomize-a-listt/1262619#1262619
    public static class IEnumerableExtensions
    {
        #region Public Methods

        /// <summary>
        /// Returns a new list where the elements are randomly shuffled. Based on the Fisher-Yates
        /// shuffle, which has O(n) complexity. Makes use of the ThreadSafeRandom class.
        /// </summary>
        public static IEnumerable<T> ThreadSafeShuffle<T>(this IEnumerable<T> list)
        {
            var source = list.ToList();
            int n = source.Count;
            var shuffled = new List<T>(n);
            shuffled.AddRange(source);
            while (n > 1)
            {
                n--;
                int k = ThreadSafeRandom.ThreadInternalRandom.Next(n + 1);
                T value = shuffled[k];
                shuffled[k] = shuffled[n];
                shuffled[n] = value;
            }
            return shuffled;
        }

        #endregion Public Methods
    }
}