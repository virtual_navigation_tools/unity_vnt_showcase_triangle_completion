﻿using UnityEngine;

namespace unibi.vnt.util
{
    public interface ICustomLogger
    {
        #region Public Methods

        /// <summary>
        /// Tell the logger to write a log message
        /// </summary>
        /// <param name="logString"> the main message string </param>
        /// <param name="type"> the type of message </param>
        /// <param name="sender"> who sent the message </param>
        /// <param name="tag"> an optional sytle tag </param>
        /// <param name="stackTrace"> an optional stack trace to pass through </param>
        void Log(string logString, LogType type, object sender, string tag = "", string stackTrace = "");

        /// <summary>
        /// Toggle the logger on or off.
        /// </summary>
        /// <param name="state"> true is active </param>
        void SetActive(bool state);

        #endregion Public Methods
    }
}