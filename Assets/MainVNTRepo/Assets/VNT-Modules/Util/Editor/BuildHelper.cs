using System.IO;
using UnityEditor;
using UnityEngine;

namespace unibi.vnt.util
{
    public static class BuildHelper
    {
        #region Fields

        public static readonly string NameModifier = "_deactivated";

        #endregion Fields

        #region Public Methods

        [MenuItem("Tools/VNT/Disable Seperate Assemblies")]
        public static void DisableAssemblies()
        {
            Debug.Log("Starting Assembly deactivation ...");
            var dir = new DirectoryInfo(Application.dataPath);
            FileInfo[] files = dir.GetFiles("*.asmdef", SearchOption.AllDirectories);
            Debug.Log($"found {files.Length} assembly definition files to disable.");
            foreach (var file in files)
            {
                if (file.Extension == ".asmdef")
                {
                    var oldName = file.Name;
                    var newPath = Path.Combine(file.Directory.FullName, oldName + NameModifier);
                    if (File.Exists(newPath))
                    {
                        Debug.LogError($"Cannot create {newPath}, since it already exists!");
                        return;
                    }
                    file.MoveTo(newPath);
                    Debug.Log($" Renamed {oldName} to {newPath}.");
                }
            }
        }

        [MenuItem("Tools/VNT/Enable Seperate Assemblies")]
        public static void EnableAssemblies()
        {
            Debug.Log("Starting Assembly activation ...");
            var dir = new DirectoryInfo(Application.dataPath);
            FileInfo[] files = dir.GetFiles("*.asmdef" + NameModifier, SearchOption.AllDirectories);
            Debug.Log($"found {files.Length} disabled assembly definition files to re-enable.");
            foreach (var file in files)
            {
                if (file.Extension == ".asmdef" + NameModifier)
                {
                    var oldName = file.Name;
                    if (!oldName.EndsWith(NameModifier))
                    {
                        return;
                    }
                    var newName = oldName.Remove(oldName.Length - NameModifier.Length);

                    var newPath = Path.Combine(file.Directory.FullName, newName);
                    if (File.Exists(newPath))
                    {
                        Debug.LogError($"Cannot create {newPath}, since it already exists!");
                        return;
                    }
                    file.MoveTo(newPath);
                    Debug.Log($" Renamed {oldName} to {newPath}.");
                }
            }
        }

        #endregion Public Methods
    }
}