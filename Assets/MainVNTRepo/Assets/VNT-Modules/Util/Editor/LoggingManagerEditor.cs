﻿using UnityEditor;
using UnityEngine;

namespace unibi.vnt.util
{
    [CustomEditor(typeof(LoggingManager))]
    public class LoggingManagerEditor : Editor
    {
        #region Public Methods

        public override void OnInspectorGUI()
        {
            DrawDefaultInspector();
            LoggingManager manager = (LoggingManager)target;
            if (GUILayout.Button("Add Loggers"))
            {
                manager.AddAttachedLoggers();
            }
            if (GUILayout.Button("Add Loggables In Scene"))
            {
                manager.AddAllLoggablesInScene();
            }
            if (GUILayout.Button("Clear Loggables"))
            {
                manager.ClearLoggables();
            }
        }

        #endregion Public Methods
    }
}