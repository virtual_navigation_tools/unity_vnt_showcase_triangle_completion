﻿using unibi.vnt.util;
using UnityEngine;

namespace unibi.vnt.spatial
{
    public class ObjectHitRecorder : MonoBehaviour, ILoggable
    {
        #region Fields

        [SerializeField, Tooltip("needs a GameObject holding an implementation of IRayTargetFinder")]
        private GameObject _targetFinder = null;

        [SerializeField, Tooltip("needs a GameObject holding an implementation of IRayCaster")]
        private GameObject _rayCaster = null;

        private IRayTargetFinder targetFinder => _targetFinder.GrabImplementation<IRayTargetFinder>();
        private IRayCaster rayCaster => _rayCaster.GrabImplementation<IRayCaster>();

        [SerializeField]
        private bool isActive = false;

        public ICustomLogger Logger { get; set; }

        #endregion Fields

        #region Private Methods

        /// <summary>
        /// Toggle whether to record raycast hits.
        /// </summary>
        /// <param name="state"> true means active </param>
        public void SetActive(bool state)
        {
            isActive = state;
        }

        private void HandleTargetHit(object sender, (GameObject, RaycastHit, Ray) data)
        {
            if (!isActive)
            {
                return;
            }
            var target = data.Item1;
            var hit = data.Item2;
            var ray = data.Item3;

            Logger.Log($"ray with direction {ray.direction}" +
                $" from origin {ray.origin} " +
                $"hit object {target.name}" +
                $" at {hit.point} " +
                $"after traveling: {hit.distance} units", LogType.Log, this);
        }

        private void OnEnable()
        {
            targetFinder.OnTargetHit += HandleTargetHit;
        }

        private void OnDisable()
        {
            targetFinder.OnTargetHit -= HandleTargetHit;
        }

        private void Update()
        {
            if (isActive)
            {
                CheckForTargets();
            }
        }

        private void CheckForTargets()
        {
            // create a new ray
            Ray ray = rayCaster.CastRay();
            targetFinder.CheckForTargetHit(ray);
            // show ray in inspector
#if UNITY_EDITOR
            Debug.DrawRay(ray.origin, ray.direction, Color.magenta);
#endif
        }

        #endregion Private Methods
    }
}