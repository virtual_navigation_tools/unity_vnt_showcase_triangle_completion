﻿using UnityEngine;

namespace unibi.vnt.spatial
{
    public interface IRayCaster
    {
        #region Properties

        /// <summary>
        /// The source object from which the ray is cast.
        /// </summary>
        GameObject SourceObject { get; }

        #endregion Properties

        #region Public Methods

        /// <summary>
        /// Causes a ray to be cast from the source object. The specific implementation of the
        /// RayCaster defines how the ray is cast. (e.g. MouseRayCaster)
        /// </summary>
        /// <returns> </returns>
        Ray CastRay();

        #endregion Public Methods
    }
}