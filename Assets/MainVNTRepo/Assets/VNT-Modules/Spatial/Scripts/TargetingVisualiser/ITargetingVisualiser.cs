﻿using UnityEngine;

namespace unibi.vnt.spatial
{
    public interface ITargetingVisualiser
    {
        #region Public Methods

        /// <summary>
        /// Performs visualisation action defined in TargetingVisualiser on target object. (e.g.
        /// change material of target as in HighlightTargetVisualiser)
        /// </summary>
        /// <param name="target"> the target for the visualisation </param>
        void SetTargeted(GameObject target);

        /// <summary>
        /// Stop applying the visualisation to target object.
        /// </summary>
        /// <param name="target"> the target object </param>
        void SetUntargeted(GameObject target);

        #endregion Public Methods
    }
}