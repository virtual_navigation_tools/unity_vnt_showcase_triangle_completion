﻿using System.Collections.Generic;
using UnityEngine;

namespace unibi.vnt.spatial
{
    public interface ITargetCollector
    {
        #region Properties

        /// <summary>
        /// A list of unique target objects currently tracked by the TargetCollector.
        /// </summary>
        HashSet<GameObject> AvailableTargets { get; }

        #endregion Properties

        #region Public Methods

        /// <summary>
        /// Adds targets to be tracked by the TargetCollector.
        /// </summary>
        /// <param name="targets"> the list of new targets </param>
        void AddTargets(List<GameObject> targets);

        /// <summary>
        /// Adds a target to be tracked by the TargetCollector.
        /// </summary>
        /// <param name="target"> the new target </param>
        void AddTargets(GameObject target);

        /// <summary>
        /// Removes all current targets from tracking.
        /// </summary>
        void ClearTargets();

        /// <summary>
        /// Removes targets from tracking.
        /// </summary>
        /// <param name="targets"> the targets to be removed </param>
        void RemoveTargets(List<GameObject> targets);

        /// <summary>
        /// Removes a target from tracking.
        /// </summary>
        /// <param name="target"> the target to be removed </param>
        void RemoveTargets(GameObject target);

        #endregion Public Methods
    }
}