﻿using System.Collections.Generic;
using unibi.vnt.util;
using UnityEngine;

namespace unibi.vnt.spatial
{
    public class SurfaceBasedPointingManager : MonoBehaviour
    {
        #region Fields

        [SerializeField, Tooltip("needs a GameObject holding an implementation of IPointingController")]
        private GameObject _pointingController = null;

        private bool allowPointing = false;

        [SerializeField] private PointerManager pointerManager = null;
        [SerializeField] private KeyCode pointingKey = KeyCode.Mouse0;

        #endregion Fields

        #region Properties

        public GameObject CurrentTarget { get; set; }
        public PointerManager PointerManager { get => pointerManager; }
        private IPointingController pointingController => _pointingController.GrabImplementation<IPointingController>();

        #endregion Properties

        #region Public Methods

        /// <summary>
        /// Add valid pointing targets.
        /// </summary>
        /// <param name="objects"> the list of possible targets </param>
        public void AddObjectsToListOfTargetables(List<GameObject> objects)
        {
            pointingController.TargetCollector.AddTargets(objects);
        }

        /// <summary>
        /// Clear list of valid pointing targets.
        /// </summary>
        public void ClearAllTargetables()
        {
            pointingController.TargetCollector.ClearTargets();
        }

        /// <summary>
        /// Turn off pointing.
        /// </summary>
        public void DisablePointing()
        {
            allowPointing = false;
            pointerManager.HidePointer();
        }

        /// <summary>
        /// Turn on pointing.
        /// </summary>
        public void EnablePointing()
        {
            pointerManager.PointerObject.transform.position = new Vector3(0, -99, 0);
            allowPointing = true;
        }

        /// <summary>
        /// Get current position of pointing marker object.
        /// </summary>
        /// <returns> </returns>
        public Vector3 GetPointerLocation()
        {
            return pointerManager.PointerObject.transform.position;
        }

        /// <summary>
        /// Convenience function to toggle pointing on or off. (calls Enable/DisablePointing internally)
        /// </summary>
        /// <param name="state"> true means active </param>
        public void SetActive(bool state)
        {
            if (state == true)
            {
                EnablePointing();
            }
            else
            {
                DisablePointing();
            }
        }

        #endregion Public Methods

        #region Private Methods

        private void ShowPointerAt(RaycastHit hit)
        {
            Quaternion rotation = pointingController.ComputeHitPointSurfaceNormal(hit);
            pointerManager.PointerObject.transform.position = hit.point;
            pointerManager.PointerObject.transform.rotation = rotation;
            pointerManager.ShowPointer();
        }

        private void Update()
        {
            if (!allowPointing)
            {
                return;
            }
            if (!Input.GetKey(pointingKey))
            {
                return;
            }
            if (pointingController.HitInfo == null)
            {
                return;
            }
            ShowPointerAt(pointingController.HitInfo.GetValueOrDefault());
        }

        #endregion Private Methods
    }
}