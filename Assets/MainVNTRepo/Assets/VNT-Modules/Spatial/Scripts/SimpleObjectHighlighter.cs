﻿using unibi.vnt.util;
using UnityEngine;

namespace unibi.vnt.spatial
{
    public class SimpleObjectHighlighter : MonoBehaviour, ILoggable
    {
        #region Fields

        private GameObject currentTarget = null;

        [SerializeField]
        private bool isActive = false;

        [SerializeField, Tooltip("needs a GameObject holding an implementation of ITargetingVisualiser")]
        private GameObject _visualiser = null;

        [SerializeField, Tooltip("needs a GameObject holding an implementation of ITargetFinder")]
        private GameObject _targetFinder = null;

        [SerializeField, Tooltip("needs a GameObject holding an implementation of IRayCaster")]
        private GameObject _rayCaster = null;

        private IRayTargetFinder targetFinder => _targetFinder.GrabImplementation<IRayTargetFinder>();
        private ITargetingVisualiser visualiser => _visualiser.GrabImplementation<ITargetingVisualiser>();

        private IRayCaster rayCaster => _rayCaster.GrabImplementation<IRayCaster>();

        #endregion Fields

        #region Properties

        public ICustomLogger Logger { get; set; }

        #endregion Properties

        #region Public Methods

        /// <summary>
        /// Toggle the state of the SimpleObjectHighlighter. Objects are only highlighted in active state.
        /// </summary>
        /// <param name="state"> true means active </param>
        public void SetActive(bool state)
        {
            isActive = state;
        }

        #endregion Public Methods

        #region Private Methods

        private GameObject CheckForTargets()
        {
            // create a new ray
            Ray ray = rayCaster.CastRay();
            // show ray in inspector
#if UNITY_EDITOR
            Debug.DrawRay(ray.origin, ray.direction, Color.green);
#endif
            // see what it hits
            return targetFinder.CheckForTargetHit(ray);
        }

        private void DeselectTarget(GameObject target)
        {
            visualiser.SetUntargeted(target);
            Logger.Log($"Object deselected: {target.name}", LogType.Log, this);
        }

        private void DoHighlighting()
        {
            if (currentTarget != null)
            {
                DeselectTarget(currentTarget);
            }
            var newTarget = CheckForTargets();
            if (newTarget != null)
            {
                Logger.Log($"switching to new target: {newTarget.name}", LogType.Log, this);
                currentTarget = newTarget;
                HighlightCurrentTarget();
            }
        }

        private void HighlightCurrentTarget()
        {
            visualiser.SetTargeted(currentTarget);
            Logger.Log($"Object targeted: {currentTarget.name}", LogType.Log, this);
        }

        private void Update()
        {
            if (isActive)
            {
                DoHighlighting();
            }
        }

        #endregion Private Methods
    }
}