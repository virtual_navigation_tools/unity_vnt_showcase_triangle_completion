﻿using unibi.vnt.util;
using UnityEngine;

namespace unibi.vnt.spatial
{
    public class PointerManager : MonoBehaviour, ILoggable
    {
        #region Fields

        private bool isHidden = true;
        private Collider pointerCollider = null;

        [SerializeField]
        private GameObject pointerObject = null;

        [SerializeField, Tooltip("If no pointer object was provided, the prefab will be used to istantiate one.")]
        private GameObject pointerPrefab = null;

        private Renderer pointerRenderer = null;

        #endregion Fields

        #region Properties

        public ICustomLogger Logger { get; set; }
        public bool PointerIsHidden { get => isHidden; }
        public GameObject PointerObject { get => pointerObject; }

        #endregion Properties

        #region Public Methods

        public void HidePointer()
        {
            pointerCollider.enabled = false;
            pointerRenderer.enabled = false;
            isHidden = true;
        }

        public void ShowPointer()
        {
            pointerObject.SetActive(true);
            pointerCollider.enabled = true;
            pointerRenderer.enabled = true;
            isHidden = false;
        }

        #endregion Public Methods

        #region Private Methods

        private void Awake()
        {
            if (pointerObject == null)
            {
                pointerObject = Instantiate(pointerPrefab);
            }
        }

        private void PrepareMarkerObject()
        {
            pointerCollider = pointerObject.GetComponentInChildren<Collider>();
            pointerRenderer = pointerObject.GetComponentInChildren<Renderer>();

            if (pointerCollider == null)
            {
                pointerCollider = pointerObject.AddComponent<MeshCollider>();
                Logger.Log("No Collider found on Pointer Object," +
                    " adding MeshCollider!", LogType.Warning, this);
            }
            if (pointerRenderer == null)
            {
                pointerRenderer = pointerObject.AddComponent<MeshRenderer>();
                Logger.Log("No Renderer found on Pointer Object," +
                    " adding MeshRenderer!", LogType.Warning, this);
            }
        }

        private void Start()
        {
            PrepareMarkerObject();
            HidePointer();
        }

        #endregion Private Methods
    }
}