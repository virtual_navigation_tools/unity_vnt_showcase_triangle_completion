using unibi.vnt.util;
using UnityEngine;
using UnityEngine.UI;

namespace unibi.vnt.spatial
{
    public class SpatialExampleRunner : MonoBehaviour, ILoggable
    {
        #region Fields

        [SerializeField]
        private SimpleObjectHighlighter highlighter = null;

        [SerializeField]
        private Toggle highlightingToggle = null;

        [SerializeField]
        private SurfaceBasedPointingManager pointer = null;

        [SerializeField]
        private Toggle pointingToggle = null;

        #endregion Fields

        #region Properties

        public ICustomLogger Logger { get; set; }

        #endregion Properties

        #region Public Methods

        public void OnHighlightToggled(bool state)
        {
            highlighter.SetActive(state);
        }

        public void OnPointingToggled(bool state)
        {
            pointer.SetActive(state);
        }

        #endregion Public Methods

        #region Private Methods

        private void Awake()
        {
            Application.targetFrameRate = 60;
        }

        // Start is called before the first frame update
        private void Start()
        {
            highlightingToggle.onValueChanged.RemoveAllListeners();
            highlightingToggle.onValueChanged.AddListener(OnHighlightToggled);

            pointingToggle.onValueChanged.RemoveAllListeners();
            pointingToggle.onValueChanged.AddListener(OnPointingToggled);
        }

        #endregion Private Methods
    }
}