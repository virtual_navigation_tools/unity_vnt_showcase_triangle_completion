﻿using System;
using unibi.vnt.guidance;
using unibi.vnt.util;
using UnityEngine;

/// <summary>
/// This class is an example showing how to hook up external components to the event system of the
/// guidance module. In this case, we intercept all guidance events and print them to the console
/// using the VNT logging system.
/// </summary>
public class LogGuidanceEvents : MonoBehaviour, ILoggable
{
    #region Properties

    public ICustomLogger Logger { get; set; }

    #endregion Properties

    #region Private Methods

    private void LogControllerReset(object sender, EventArgs e)
    {
        var log = $"Controller of type {sender.GetType()} has been reset.";
        Logger.Log(log, LogType.Log, sender, VNTLogFormatter.Example_Tag);
    }

    private void LogFirstWaypointReached(object sender, IWaypoint wp)
    {
        var log = $"First waypoint [named '{wp.GetName()}', " +
                  $"placed at {wp.GetGameObject().transform.position}] " +
                   "has been flagged as reached.";
        Logger.Log(log, LogType.Log, sender, VNTLogFormatter.Example_Tag);
    }

    private void LogGuidancePaused(object sender, IWaypoint wp)
    {
        var log = $"Guidance paused by guidance controller. " +
                  $"Last waypoint was {wp.GetName()} at {wp.GetGameObject().transform.position}.";
        Logger.Log(log, LogType.Log, sender, VNTLogFormatter.Example_Tag);
    }

    private void LogGuidanceResumed(object sender, IWaypoint wp)
    {
        var log = $"Guidance resumed by guidance controller. " +
                        $"Next waypoint is {wp.GetName()} at {wp.GetGameObject().transform.position}.";
        Logger.Log(log, LogType.Log, sender, VNTLogFormatter.Example_Tag);
    }

    private void LogGuidanceStarted(object sender, IWaypoint wp)
    {
        var log = "Guidance started by guidance controller. " +
                 $"First waypoint is at {wp.GetGameObject().transform.position}.";
        Logger.Log(log, LogType.Log, sender, VNTLogFormatter.Example_Tag);
    }

    private void LogGuidanceStopped(object sender, IWaypoint wp)
    {
        var log = "Guidance stopped by guidance controller. " +
                 $"Last waypoint was at {wp.GetGameObject().transform.position}.";
        Logger.Log(log, LogType.Log, sender, VNTLogFormatter.Example_Tag);
    }

    private void LogLastWaypointReached(object sender, IWaypoint wp)
    {
        var log = $"Last waypoint [named '{wp.GetName()}', " +
                  $"placed at {wp.GetGameObject().transform.position}] " +
                   "has been flagged as reached by guidance controller.";
        Logger.Log(log, LogType.Log, sender, VNTLogFormatter.Example_Tag);
    }

    private void LogWaypointReached(object sender, Vector3 triggerPos)
    {
        IWaypoint wp = (IWaypoint)sender;
        var log = $"WP '{wp.GetName()}' " +
                  $"[placed at {wp.GetGameObject().transform.position}] " +
                  $"was triggered at {triggerPos}.";
        Logger.Log(log, LogType.Log, sender, VNTLogFormatter.Example_Tag);
    }

    private void OnDestroy()
    {
        // unsub when destroyed
        WaypointWithTriggerRadius.OnReached -= LogWaypointReached;
        GuidanceController.OnFirstWaypointReached -= LogFirstWaypointReached;
        GuidanceController.OnFirstWaypointReached -= LogLastWaypointReached;
        GuidanceController.OnGuidanceStarted -= LogGuidanceStarted;
        GuidanceController.OnGuidanceStopped -= LogGuidanceStopped;
        GuidanceController.OnGuidancePaused -= LogGuidancePaused;
        GuidanceController.OnGuidanceResumed -= LogGuidanceResumed;
        GuidanceController.OnControllerReset -= LogControllerReset;
    }

    private void Start()
    {
        // subscribe to events when created
        WaypointWithTriggerRadius.OnReached += LogWaypointReached;
        GuidanceController.OnFirstWaypointReached += LogFirstWaypointReached;
        GuidanceController.OnLastWaypointReached += LogLastWaypointReached;
        GuidanceController.OnGuidanceStarted += LogGuidanceStarted;
        GuidanceController.OnGuidanceStopped += LogGuidanceStopped;
        GuidanceController.OnGuidancePaused += LogGuidancePaused;
        GuidanceController.OnGuidanceResumed += LogGuidanceResumed;
        GuidanceController.OnControllerReset += LogControllerReset;
    }

    #endregion Private Methods
}