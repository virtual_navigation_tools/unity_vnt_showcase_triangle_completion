﻿using UnityEditor;
using UnityEngine;

/// <summary>
/// This displays some buttons in the inspector, which are hooked up to methods in the ExampleGuidanceUsage.
/// </summary>
[CustomEditor(typeof(ExampleGuidanceUsage))]
public class ExampleGuidanceEditor : Editor
{
    #region Public Methods

    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();
        ExampleGuidanceUsage example = (ExampleGuidanceUsage)target;
        if (GUILayout.Button("Create Waypoints"))
        {
            example.CreateWaypoints();
        }
        if (GUILayout.Button("Init Controller"))
        {
            example.InitialiseController();
        }
        if (GUILayout.Button("Start Guidance"))
        {
            example.StartGuidance();
        }
        if (GUILayout.Button("Stop Guidance"))
        {
            example.StopGuidance();
        }
        if (GUILayout.Button("Pause Guidance"))
        {
            example.PauseGuidance();
        }
        if (GUILayout.Button("Resume Guidance"))
        {
            example.ResumeGuidance();
        }
        if (GUILayout.Button("Force Controller Reset"))
        {
            example.ResetController();
        }
    }

    #endregion Public Methods
}