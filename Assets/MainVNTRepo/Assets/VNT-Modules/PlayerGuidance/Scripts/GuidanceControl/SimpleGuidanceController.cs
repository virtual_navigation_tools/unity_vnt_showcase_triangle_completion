﻿using System.Collections.Generic;
using System.Linq;

// in-house libraries
using unibi.vnt.guidance;
using unibi.vnt.util;
using UnityEngine;

public class SimpleGuidanceController : GuidanceController, ILoggable
{
    #region Fields

    private Arrow arrow;
    private GuidanceState currentState;
    private IWaypoint currentWaypoint;
    private GameObject trackedObject;
    private List<IWaypoint> waypoints;

    #endregion Fields

    #region Public Constructors

    /// <summary>
    /// This is the constructor for this controller. It needs to be set up with an arrow for
    /// guidance visualisiation.
    /// </summary>
    /// <param name="arrow"> the guidance arrow </param>
    public SimpleGuidanceController(Arrow arrow)
    {
        this.arrow = arrow;
        arrow.DisableAndHide();
    }

    #endregion Public Constructors

    #region Properties

    public ICustomLogger Logger { get; set; }
    private bool hasWaypoints => waypoints.Count > 0;
    private bool isFirstWaypoint => currentWaypoint == waypoints.First();
    private bool isInitialised => GetCurrentState() != GuidanceState.Uninitialised;
    private bool isLastWaypoint => currentWaypoint == waypoints.Last();

    #endregion Properties

    #region Public Methods

    public override GuidanceState GetCurrentState() => currentState;

    public override void HandleWaypointReached(object sender, Vector3 triggerPosition)
    {
        if (isInitialised && currentState == GuidanceState.Active)
        {
            IWaypoint wp = (IWaypoint)sender;
            if (wp == currentWaypoint)
            {
                // hide and disable the waypoint that was just reached
                DeactivateWaypoint(currentWaypoint);
                // proceed based on position in waypoint list
                if (isLastWaypoint)
                {
                    // fire first waypoint event if applicable
                    if (isFirstWaypoint)
                    { FirstWaypointReached(this, wp); }
                    // fire last waypoint event
                    LastWaypointReached(this, wp);
                }
                else
                {
                    // fire first waypoint event if applicable
                    if (isFirstWaypoint)
                    { FirstWaypointReached(this, wp); }
                    // advance to next waypoint
                    currentWaypoint = waypoints[waypoints.IndexOf(currentWaypoint) + 1];
                    // activate next waypoint
                    ActivateWaypoint(currentWaypoint);
                }
            }
        }
    }

    public override void Initialise(List<IWaypoint> waypoints, GameObject trackedObject)
    {
        if (waypoints.Count == 0)
        {
            var log = "list of waypoints was empty! " +
                      "controller needs at least one waypoint to function. " +
                      "remember to add waypoints to waypoint list before starting guidance!";
            Logger.Log(log, LogType.Error, this, VNTLogFormatter.Example_Tag);
        }

        this.waypoints = waypoints;
        this.trackedObject = trackedObject;

        arrow.AnchorToPlayer(this.trackedObject);
        arrow.DisableAndHide();

        currentState = GuidanceState.Inactive;
    }

    public override void PauseGuidance()
    {
        if (isInitialised && currentState == GuidanceState.Active)
        {
            DeactivateWaypoint(currentWaypoint);
            currentState = GuidanceState.Paused;
            GuidancePaused(this, currentWaypoint);
        }
    }

    public override void Reset()
    {
        if (hasWaypoints)
        {
            foreach (var waypoint in waypoints)
            {
                waypoint.Delete();
            }
            waypoints.Clear();
            currentWaypoint = null;
        }
        currentState = GuidanceState.Uninitialised;
        arrow.DisableAndHide();
        ControllerReset(this);
    }

    public override void ResumeGuidance()
    {
        if (isInitialised && currentState == GuidanceState.Paused)
        {
            ActivateWaypoint(currentWaypoint);
            currentState = GuidanceState.Active;
            GuidanceResumed(this, currentWaypoint);
        }
    }

    public override void StartGuidance()
    {
        if (isInitialised)
        {
            currentWaypoint = waypoints[0];
            ActivateWaypoint(currentWaypoint);
            currentState = GuidanceState.Active;
            GuidanceStarted(this, currentWaypoint);
        }
    }

    public override void StopGuidance()
    {
        if (isInitialised)
        {
            foreach (var waypoint in waypoints)
            {
                DeactivateWaypoint(waypoint);
            }
            currentWaypoint = waypoints[0];
            arrow.DisableAndHide();
            currentState = GuidanceState.Inactive;
            GuidanceStopped(this, currentWaypoint);
        }
    }

    #endregion Public Methods

    #region Private Methods

    private void ActivateWaypoint(IWaypoint waypoint)
    {
        waypoint.Show();
        waypoint.Enable();
        arrow.SetTarget(waypoint.GetGameObject());
        arrow.PointAtTarget();
    }

    private void DeactivateWaypoint(IWaypoint waypoint)
    {
        waypoint.Hide();
        waypoint.Disable();
        arrow.DisableAndHide();
    }

    #endregion Private Methods
}