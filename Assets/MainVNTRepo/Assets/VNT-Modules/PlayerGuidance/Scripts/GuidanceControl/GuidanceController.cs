﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace unibi.vnt.guidance
{
    /// <summary>
    /// This class provides a template on how to fulfill the contract of the IGuidanceController
    /// interface. It focuses on event-based handling of guidance.
    /// </summary>
    public abstract class GuidanceController : IGuidanceController
    {
        #region Events

        /// <summary>
        /// Fires when the controller is reset.
        /// </summary>
        public static event EventHandler OnControllerReset;

        /// <summary>
        /// Fires when the first waypoint is reached and passes it along.
        /// </summary>
        public static event EventHandler<IWaypoint> OnFirstWaypointReached;

        /// <summary>
        /// Fires when guidance is paused. Passes along the last waypoint before the pause.
        /// </summary>
        public static event EventHandler<IWaypoint> OnGuidancePaused;

        /// <summary>
        /// Fires when guidance is resumed. Passes along the next waypoint to reach.
        /// </summary>
        public static event EventHandler<IWaypoint> OnGuidanceResumed;

        /// <summary>
        /// Fires when guidance is started. Passes along the first waypoint.
        /// </summary>
        public static event EventHandler<IWaypoint> OnGuidanceStarted;

        /// <summary>
        /// Fires when guidance is stopped. Passes along the last waypoint.
        /// </summary>
        public static event EventHandler<IWaypoint> OnGuidanceStopped;

        /// <summary>
        /// Fires when the last waypoint is reached and passes it along.
        /// </summary>
        public static event EventHandler<IWaypoint> OnLastWaypointReached;

        #endregion Events

        #region Properties

        /// <summary>
        /// A property to see the waypoinst currently used by the controller.
        /// </summary>
        public IWaypoint[] Waypoints { get; protected set; }

        #endregion Properties

        #region Public Methods

        public abstract GuidanceState GetCurrentState();

        /// <summary>
        /// A handler for an event which signals that a waypoint has been reached.
        /// </summary>
        /// <param name="sender"> the entity broadcasting the event </param>
        /// <param name="triggerPosition">
        /// the position of the tracked object when it triggered the event
        /// </param>
        public abstract void HandleWaypointReached(object sender, Vector3 triggerPosition);

        public abstract void Initialise(List<IWaypoint> waypoints, GameObject trackedObject);

        public abstract void PauseGuidance();

        public abstract void Reset();

        public abstract void ResumeGuidance();

        public abstract void StartGuidance();

        public abstract void StopGuidance();

        #endregion Public Methods

        #region Protected Methods

        protected virtual void ControllerReset(IGuidanceController sender)
        {
            OnControllerReset?.Invoke(sender, EventArgs.Empty);
        }

        protected virtual void FirstWaypointReached(IGuidanceController sender, IWaypoint wp)
        {
            OnFirstWaypointReached?.Invoke(sender, wp);
        }

        protected virtual void GuidancePaused(IGuidanceController sender, IWaypoint wp)
        {
            OnGuidancePaused?.Invoke(sender, wp);
        }

        protected virtual void GuidanceResumed(IGuidanceController sender, IWaypoint wp)
        {
            OnGuidanceResumed?.Invoke(sender, wp);
        }

        protected virtual void GuidanceStarted(IGuidanceController sender, IWaypoint wp)
        {
            OnGuidanceStarted?.Invoke(sender, wp);
        }

        protected virtual void GuidanceStopped(IGuidanceController sender, IWaypoint wp)
        {
            OnGuidanceStopped?.Invoke(sender, wp);
        }

        protected virtual void LastWaypointReached(IGuidanceController sender, IWaypoint wp)
        {
            OnLastWaypointReached?.Invoke(sender, wp);
        }

        #endregion Protected Methods
    }
}