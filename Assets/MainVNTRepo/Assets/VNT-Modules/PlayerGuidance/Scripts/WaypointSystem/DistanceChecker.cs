﻿using System;
using unibi.vnt.util;
using UnityEngine;

namespace unibi.vnt.guidance
{
    public class DistanceChecker : MonoBehaviour, ILoggable
    {
        #region Fields

        [SerializeField]
        private bool isActive = false;

        private bool isInitialised = false;

        [SerializeField]
        private Mode mode = Mode.Greater;

        [SerializeField]
        private Vector3 targetPosition = new();

        [SerializeField]
        private double thresholdDistance = 0f;

        [SerializeField]
        private Transform trackedTransform = null;

        #endregion Fields

        #region Events

        public static event EventHandler<Vector3> OnThresholdDistanceReached;

        #endregion Events

        #region Enums

        public enum Mode
        { Smaller, Greater }

        #endregion Enums

        #region Properties

        public Mode CurrentMode => mode;
        public bool IsActive => isActive;
        public ICustomLogger Logger { get; set; }
        public double ThresholdDistance => thresholdDistance;

        #endregion Properties

        #region Public Methods

        /// <summary>
        /// This allows us to switch the mode of the checker while it is running.
        /// </summary>
        /// <param name="newMode"> </param>
        public void AdjustMode(Mode newMode)
        {
            mode = newMode;
        }

        /// <summary>
        /// This allows us to set a new threshold distance while the checker is running.
        /// </summary>
        /// <param name="newDistance"> </param>
        public void AdjustThresholdDistance(float newDistance)
        {
            thresholdDistance = newDistance;
        }

        /// <summary>
        /// This allows us to stop the tracker.
        /// </summary>
        public void Disable()
        {
            isActive = false;
        }

        /// <summary>
        /// This allows us to start the tracker.
        /// </summary>
        public void Enable()
        {
            isActive = true;
        }

        /// <summary>
        /// This method must be called before using the DistanceChecker, to ensure its functionality.
        /// </summary>
        /// <param name="trackedTransform">
        /// The position of this transform will be checked against the target position.
        /// </param>
        /// <param name="targetPosition">
        /// This position will be checked against the position of the tracked Transform.
        /// </param>
        /// <param name="thresholdDistance">
        /// The distance which must be under or overshot for the checker to trigger.
        /// </param>
        /// <param name="mode">
        /// whether the checker should fire for distances smaller or larger than the thresholdDistance.
        /// </param>
        public void Initialise
            (
                Transform trackedTransform,
                Vector3 targetPosition,
                double thresholdDistance,
                Mode mode
            )
        {
            this.trackedTransform = trackedTransform;
            this.targetPosition = targetPosition;
            this.thresholdDistance = thresholdDistance;
            this.mode = mode;
            isInitialised = true;
        }

        #endregion Public Methods

        #region Private Methods

        /// <summary>
        /// Here we check whether the threshold distance between tracked Transform and targetPositon
        /// has been under- or overshot and trigger the OnThresholdDistanceReached event if
        /// applicable. It compares squared distances for performance reasons.
        /// </summary>
        private void DoDistanceCheck()
        {
            if (trackedTransform == null)
            {
                return;
            }
            bool isWithinRadius = FastDistanceComparison.IsWithinRadius(targetPosition, trackedTransform.position, (float)thresholdDistance);
            switch (mode)
            {
                case Mode.Greater:
                    if (!isWithinRadius)
                    {
                        OnThresholdDistanceReached?.Invoke(this, trackedTransform.position);
                    }
                    break;

                case Mode.Smaller:
                    if (isWithinRadius)
                    {
                        OnThresholdDistanceReached?.Invoke(this, trackedTransform.position);
                    }
                    break;

                default:
                    var log = $"Distance check is in invalid comparison mode: {mode}";
                    Logger.Log(log, LogType.Error, this);
                    break;
            }
        }

        /// <summary>
        /// This allows us to display the threshold distance in the Inspector.
        /// </summary>
        private void OnDrawGizmos()
        {
            Gizmos.color = Color.red;
            Gizmos.DrawWireSphere(targetPosition, (float)Math.Abs(thresholdDistance));
        }

        /// <summary>
        /// Every frame, we check whether the distance check should be performed and do so as the
        /// case may be.
        /// </summary>
        private void Update()
        {
            if (!isActive)
            {
                return;
            }
            if (!isInitialised)
            {
                return;
            }
            DoDistanceCheck();
        }

        #endregion Private Methods
    }
}