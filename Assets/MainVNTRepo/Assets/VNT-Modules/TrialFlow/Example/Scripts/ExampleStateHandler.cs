﻿using System;
using System.Collections;
using System.Diagnostics;
using unibi.vnt.util;
using UnityEngine;

namespace unibi.vnt.trialflow
{
    /// <summary>
    /// We can easily derive custom state handler classes from the abstract template. We then hook
    /// up our newly created handler to the state it is supposed to handle and fill in the desired
    /// functionality. In this example we use Unity's coroutine functionality to set a waiting
    /// period that can be easily controlled from the Unity editor.
    /// </summary>
    internal class ExampleStateHandler : TrialStateHandler, ILoggable
    {
        #region Fields

        /// <summary>
        /// since the handler is a MonoBehaviour, we can change the wait time in the editor, without
        /// rewriting the code in the state itself.
        /// </summary>
        [SerializeField]
        private int waitTimeInSeconds = 5;

        #endregion Fields

        #region Properties

        /// <summary>
        /// Here we set the state to be handled.
        /// </summary>
        public override ITrialState HandledState => ExampleStateMachine.ExampleState2;

        public ICustomLogger Logger { get; set; }
        public TrialStateMachine StateMachine { get; private set; }

        #endregion Properties

        #region Protected Methods

        protected override void HandleEnter(object sender, TrialStateMachine stateMachine)
        {
            StateMachine = stateMachine;
            TrialState state = (TrialState)sender;
            if (state == HandledState)
            {
                var log = $"We have entered the state {state.Name}";
                Logger.Log(log, LogType.Log, this, VNTLogFormatter.Example_Tag);
                StartCoroutine(RunStateLogicCoroutine());
            }
        }

        protected override void HandleExit(object sender, EventArgs e)
        {
            TrialState state = (TrialState)sender;
            if (state == HandledState)
            {
                var log = $"We are leaving the state {state.Name}";
                Logger.Log(log, LogType.Log, this, VNTLogFormatter.Example_Tag);
                StopCoroutine(RunStateLogicCoroutine());
            }
        }

        /// <summary>
        /// This function runs as a Coroutine (https://docs.unity3d.com/Manual/Coroutines.html), so
        /// its execution can be timed independently. In this example, we set a waiting period
        /// comparable to the one using the async functionality in the
        /// 'ExampleStateWithInternalHandling'. The Coroutine is started in the HandleEnter() method
        /// and stopped in the HandleExit() method of this class.
        /// </summary>
        /// <returns> </returns>
        protected override IEnumerator RunStateLogicCoroutine()
        {
            var log = $"We will now wait for {waitTimeInSeconds} seconds " +
                      $"and then transition to {ExampleStateMachine.ExampleState3.Name}.";
            Logger.Log(log, LogType.Log, this, VNTLogFormatter.Example_Tag);

            #region do waiting and time actual wait duration

            Stopwatch stopWatch = new();
            stopWatch.Start();
            yield return new WaitForSecondsRealtime(waitTimeInSeconds);
            stopWatch.Stop();
            TimeSpan ts = stopWatch.Elapsed;

            #endregion do waiting and time actual wait duration

            log = $"Waited for {ts.TotalSeconds} out of expected {waitTimeInSeconds} seconds, " +
                   "now initiating state transition.";
            Logger.Log(log, LogType.Log, this, VNTLogFormatter.Example_Tag);
            stopWatch.Reset();
            StateMachine.SetState(ExampleStateMachine.ExampleState3); // state transition happens here
        }

        #endregion Protected Methods
    }
}