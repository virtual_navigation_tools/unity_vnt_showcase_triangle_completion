using System;
using System.Collections;
using unibi.vnt.util;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

namespace unibi.vnt.trialflow
{
    /// <summary>
    /// This class showcases how a TrialStateHandler can also be directly responsible for managing
    /// another piece of the program, in this case some simple UI elements. THis way, the user can
    /// directly influence the state of our TrialStateMachine.
    /// </summary>
    internal class ButtonHandler : TrialStateHandler, ILoggable
    {
        #region Fields

        private ButtonID buttonClicked;
        private bool buttonWasClicked = false;

        [SerializeField]
        private Button leftButton = null;

        [SerializeField]
        private TrialFlowExampleRunner machineRunner = null;

        [SerializeField]
        private Button rightButton = null;

        [SerializeField]
        private Toggle toggle = null;

        #endregion Fields

        #region Enums

        private enum ButtonID
        { left, right };

        #endregion Enums

        #region Properties

        public override ITrialState HandledState => ExampleStateMachine.ExampleState4;
        public ICustomLogger Logger { get; set; }

        #endregion Properties

        #region Public Methods

        /// <summary>
        /// Set left button clicked.
        /// </summary>
        public void LeftButtonClicked()
        {
            buttonWasClicked = true;
            buttonClicked = ButtonID.left;
        }

        /// <summary>
        /// Set right button clicked.
        /// </summary>
        public void RightButtonClicked()
        {
            buttonWasClicked = true;
            buttonClicked = ButtonID.right;
        }

        /// <summary>
        /// Calling this tells the state machine runner to turn the state machine on or off.
        /// </summary>
        /// <param name="runMachine"> </param>
        public void StateMachineToggle(bool runMachine)
        {
            machineRunner.RunStateMachine = runMachine;
        }

        #endregion Public Methods

        #region Protected Methods

        protected override void HandleEnter(object sender, TrialStateMachine stateMachine)
        {
            TrialState state = (TrialState)sender;
            if (state == HandledState)
            {
                leftButton.interactable = true;
                rightButton.interactable = true;
                StartCoroutine(RunStateLogicCoroutine());
            }
            else
            {
                leftButton.interactable = false;
                rightButton.interactable = false;
            }
        }

        protected override void HandleExit(object sender, EventArgs e)
        {
            leftButton.interactable = false;
            rightButton.interactable = false;
        }

        protected override IEnumerator RunStateLogicCoroutine()
        {
            var log = "Press the left button button to go another round or the right one to quit.";
            Logger.Log(log, LogType.Log, this, VNTLogFormatter.Example_Tag);

            yield return new WaitUntil(() => buttonWasClicked == true); // lambda function to check state of flag variable
            if (buttonClicked == ButtonID.left)
            {
                log = "You clicked the left button.";
                Logger.Log(log, LogType.Log, this, VNTLogFormatter.Example_Tag);
                machineRunner.HandleLeftButton();
            }
            else
            {
                log = "You clicked the right button.";
                Logger.Log(log, LogType.Log, this, VNTLogFormatter.Example_Tag);
                toggle.isOn = false;
                machineRunner.StopMachine();
            }
            buttonWasClicked = false; // reset flag
        }

        protected override void Start()
        {
            base.Start();

            InitToggle(toggle, StateMachineToggle);
            InitButton(leftButton, LeftButtonClicked);
            InitButton(rightButton, RightButtonClicked);

            leftButton.interactable = false;
            rightButton.interactable = false;
        }

        #endregion Protected Methods

        #region Private Methods

        private void InitButton(Button button, UnityAction listener)
        {
            button.onClick.RemoveAllListeners();
            button.onClick.AddListener(listener);
        }

        private void InitToggle(Toggle toggle, UnityAction<bool> listener)
        {
            toggle.onValueChanged.RemoveAllListeners();
            toggle.onValueChanged.AddListener(listener);
        }

        #endregion Private Methods
    }
}