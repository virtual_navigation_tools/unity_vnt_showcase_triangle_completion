﻿using System;
using System.Collections;
using unibi.vnt.util;
using UnityEngine;

namespace unibi.vnt.trialflow
{
    /// <summary>
    /// We can easily derive custom state handler classes from the abstract template. We then hook
    /// up the handler to the associated state and fill in the desired functionality. In this
    /// example, we use the provided PausableTimer class to separate the waiting logic from the
    /// state handler, to simulate reacting to a user action somewhere else in the program, instead
    /// of setting fixed time period to wait here in the handler class.
    /// </summary>
    internal class ExampleStateHandlerWithExternalManager : TrialStateHandler, ILoggable
    {
        #region Fields

        /// <summary>
        /// since the handler is a Monobehaviour, we can hook it up to any other Monobehaviour
        /// classes it needs to know about directly in the editor.
        /// </summary>
        [SerializeField]
        private SomeExternalManagerClass externalManager = null;

        /// <summary>
        /// We create a flag variable which will be changed when the external manager class fires an event.
        /// </summary>
        private bool managerEventHasFired = false;

        #endregion Fields

        #region Properties

        /// <summary>
        /// Here we set the state to be handled.
        /// </summary>
        public override ITrialState HandledState => ExampleStateMachine.ExampleState3;

        public ICustomLogger Logger { get; set; }

        /// <summary>
        /// A reference to the state machine holding the state we want to handle.
        /// </summary>
        public TrialStateMachine StateMachine { get; private set; }

        #endregion Properties

        #region Protected Methods

        protected override void HandleEnter(object sender, TrialStateMachine stateMachine)
        {
            TrialState state = (TrialState)sender;
            StateMachine = stateMachine;

            if (state == HandledState)
            {
                var log = $"We have entered the state {state.Name}";
                Logger.Log(log, LogType.Log, this, VNTLogFormatter.Example_Tag);
                StartCoroutine(RunStateLogicCoroutine());
            }
        }

        protected override void HandleExit(object sender, EventArgs e)
        {
            TrialState state = (TrialState)sender;
            if (state == HandledState)
            {
                var log = $"We have entered the state {state.Name}";
                Logger.Log(log, LogType.Log, this, VNTLogFormatter.Example_Tag);
                StopCoroutine(RunStateLogicCoroutine());
            }
        }

        protected override void OnDestroy()
        {
            base.OnDestroy(); // make sure the base class method is called
            externalManager.SomeEventToListenFor -= HandleExternalManagerEventFired;
        }

        protected override IEnumerator RunStateLogicCoroutine()
        {
            externalManager.TriggerSomething(); // trigger some behaviour in an external manager class, the handler does not need to know what it is triggering
            yield return new WaitUntil(() => managerEventHasFired == true); // lambda function to check state of flag variable
            StateMachine.SetState(ExampleStateMachine.ExampleState4); // state transition happens here
            managerEventHasFired = false; // reset flag
        }

        protected override void Start()
        {
            base.Start(); // make sure the base class method is called so we actually subscribe to the state events
            if (externalManager == null)
            {
                externalManager = GetComponent<SomeExternalManagerClass>(); // if no manager was assigned in the editor, try to find it
            }

            // here we subscribe to a non-static event, so we only listen to events fired by this
            // instance of the manager
            externalManager.SomeEventToListenFor += HandleExternalManagerEventFired;
        }

        #endregion Protected Methods

        #region Private Methods

        /// <summary>
        /// Here we handle the incoming event fired by the external manager class. To actually
        /// listen for the event, we subscribe to it in the Start() method.
        /// </summary>
        /// <param name="sender"> the object firing the event </param>
        /// <param name="e"> an empty EventArgs object </param>
        private void HandleExternalManagerEventFired(object sender, EventArgs e)
        {
            managerEventHasFired = true;
        }

        #endregion Private Methods
    }
}