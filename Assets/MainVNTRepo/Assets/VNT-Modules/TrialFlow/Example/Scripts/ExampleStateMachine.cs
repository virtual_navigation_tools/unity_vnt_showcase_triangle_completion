﻿using unibi.vnt.util;

namespace unibi.vnt.trialflow
{
    /// <summary>
    /// This state machine builds on the basic implementation by adding a number of custom states.
    /// These states must derive from TrialState and can either handle their behaviour internally or
    /// outsource the state handling to a handler class. We use four custom states here for
    /// illustration purposes. The first one simply handles its logic internally, the second uses an
    /// external handler, while the third uses an external handler and also interacts with further
    /// classes, external to itself. Finally, the fourth one is connect to UI elements to showcase
    /// the handling of user input.
    /// </summary>
    internal class ExampleStateMachine : TrialStateMachine
    {
        #region Fields

        public static ExampleStateWithInternalHandling ExampleState1;

        // note that we can have different types of state in the same machine!
        public static GenericTrialState ExampleState2;

        public static GenericTrialState ExampleState3;

        public static GenericTrialState ExampleState4;

        #endregion Fields

        #region Public Constructors

        /// <summary>
        /// This is the constructor of our state machine. It handles the setup of the machine by
        /// initialising the TrialStates we want our machine to work with.
        /// </summary>
        /// <param name="logger"> </param>
        public ExampleStateMachine(ICustomLogger logger) : base()
        {
            #region init states

            ExampleState1 = new ExampleStateWithInternalHandling(StateNames.SimplestState.ToString(), logger);
            ExampleState2 = new GenericTrialState(StateNames.StateWithHandler.ToString());
            ExampleState3 = new GenericTrialState(StateNames.StateWithHandlerAndManager.ToString());
            ExampleState4 = new GenericTrialState(StateNames.StateWithUserInteraction.ToString());

            #endregion init states
        }

        #endregion Public Constructors

        #region Enums

        /// <summary>
        /// Here we define an enum to have a handy list of the names we have given to our states.
        /// </summary>
        public enum StateNames
        {
            SimplestState,
            StateWithHandler,
            StateWithHandlerAndManager,
            StateWithUserInteraction
        }

        #endregion Enums
    }
}