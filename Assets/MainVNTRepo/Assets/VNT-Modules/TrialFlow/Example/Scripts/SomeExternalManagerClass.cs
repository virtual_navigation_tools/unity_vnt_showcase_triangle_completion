﻿using System;
using System.Diagnostics;
using unibi.vnt.util;
using UnityEngine;

namespace unibi.vnt.trialflow
{
    /// <summary>
    /// This class serves as an example of the state handling system interacting with some other
    /// system in our program. The state handler might trigger some action (like spawning an
    /// obstacle, showing a screen to the user) in an external class and then wait until it gets
    /// confirmation that the the external chain of events has been dealt with. We simulate this
    /// pattern of operation in the ExampleStateHandlerWithExternalManager, which triggers the
    /// setting of a timer in this class and then waits until this class broadcasts that the timer
    /// has run out.
    /// </summary>
    internal class SomeExternalManagerClass : MonoBehaviour, ILoggable
    {
        #region Fields

        private Stopwatch stopWatch = new Stopwatch();

        [SerializeField]
        private PausableTimer timer = null;

        [SerializeField]
        private string timerID = "WaitTimer";

        #endregion Fields

        #region Events

        public event EventHandler SomeEventToListenFor;

        #endregion Events

        #region Properties

        public ICustomLogger Logger { get; set; }

        #endregion Properties

        #region Public Methods

        public void TriggerSomething()
        {
            var log = "Handler has requested some action to be performed " +
                      "by the external manager class.";
            Logger.Log(log, LogType.Log, this, VNTLogFormatter.Example_Tag);
            timer.ID = timerID;
            // start timer with the timeout value set in the editor
            timer.StartWithNewTimeout(timer.TimeoutInSeconds);
        }

        #endregion Public Methods

        #region Private Methods

        private void OnDestroy()
        {
            PausableTimer.HasStarted -= OnTimerStart;
            PausableTimer.HasStopped -= OnTimerDone;
        }

        private void OnTimerDone(object sender, EventArgs e)
        {
            PausableTimer _timer = (PausableTimer)sender; // example of how to extract timer object from event
            stopWatch.Stop();
            TimeSpan ts = stopWatch.Elapsed;
            var log = $"Waited for {ts.TotalSeconds} out of expected {_timer.CurrentValue} seconds, " +
                       "now initiating state transition.";
            Logger.Log(log, LogType.Log, this, VNTLogFormatter.Example_Tag);
            stopWatch.Reset();
            timer.StopAndReset();
            SomeEventToListenFor?.Invoke(sender, e);
        }

        private void OnTimerStart(object sender, EventArgs e)
        {
            PausableTimer _timer = (PausableTimer)sender; // example of how to extract timer object from event
            if (_timer.ID == timerID)
            {
                var log = $"Starting {_timer.ID} to wait {_timer.TimeoutInSeconds} seconds.";
                Logger.Log(log, LogType.Log, this, VNTLogFormatter.Example_Tag);
                stopWatch.Start();
            }
        }

        private void Start()
        {
            PausableTimer.HasStarted += OnTimerStart;
            PausableTimer.HasStopped += OnTimerDone;
        }

        #endregion Private Methods
    }
}