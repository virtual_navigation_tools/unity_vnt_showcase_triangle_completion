﻿using System;
using unibi.vnt.util;
using UnityEngine;

namespace unibi.vnt.trialflow
{
    /// <summary>
    /// This class provides a pausable timer, which can be managed either via script or via the
    /// Unity Editor.
    /// </summary>
    public class PausableTimer : MonoBehaviour, ILoggable
    {
        #region Fields

        [SerializeField]
        private string id = "DefaultTimer";

        [SerializeField]
        private bool isActive = false;

        [SerializeField]
        private bool isPaused = true;

        [SerializeField]
        private bool outputDebugMessages = false;

        [SerializeField]
        private double timeoutInSeconds = 999;

        #endregion Fields

        #region Events

        public static event EventHandler HasPaused;

        public static event EventHandler HasStarted;

        public static event EventHandler HasStopped;

        #endregion Events

        #region Properties

        /// <summary>
        /// The value indicating how many seconds the timer has run.
        /// </summary>
        public double CurrentValue { get; private set; }

        /// <summary>
        /// A flag showing whether the timer has run out yet.
        /// </summary>
        public bool HasReachedTimeout { get => (CurrentValue >= TimeoutInSeconds); }

        /// <summary>
        /// The identifier of the timer instance.
        /// </summary>
        public string ID
        { get => id; set { id = value; } }

        /// <summary>
        /// A flag showing whether the timer is currently running (i.e not inactive or paused).
        /// </summary>
        public bool IsRunning { get => (!isPaused && isActive); }

        public ICustomLogger Logger { get; set; }

        /// <summary>
        /// The value indicating when the timer will run out and stop.
        /// </summary>
        public double TimeoutInSeconds { get => timeoutInSeconds; }

        #endregion Properties

        #region Public Methods

        /// <summary>
        /// Here we can pause the currently running timer. Does nothing if timer is not running or
        /// already paused.
        /// </summary>
        public void Pause()
        {
            if (!isActive)
            {
                var log = "Timer not running, nothing to pause.";
                Logger.Log(log, LogType.Warning, this);
            }
            else if (isActive)
            {
                if (!isPaused)
                {
                    if (outputDebugMessages)
                    {
                        var log = $"Pausing timer at {CurrentValue}";
                        Logger.Log(log, LogType.Log, this);
                    }
                    isPaused = true;
                    HasPaused?.Invoke(this, EventArgs.Empty);
                }
                else
                {
                    var log = "Timer already paused!";
                    Logger.Log(log, LogType.Warning, this);
                }
            }
        }

        /// <summary>
        /// Here we can resume the timer, after it has been paused. Does nothing if timer is not paused.
        /// </summary>
        public void ResumeTimer()
        {
            if (!isActive)
            {
                var log = "Timer not running, nothing to resume.";
                Logger.Log(log, LogType.Warning, this);
            }
            else if (isActive)
            {
                if (isPaused)
                {
                    if (outputDebugMessages)
                    {
                        var log = $"Resuming timer at {CurrentValue}";
                        Logger.Log(log, LogType.Log, this);
                    }
                    isPaused = false;
                }
                else
                {
                    var log = "Timer already running, no need to resume.";
                    Logger.Log(log, LogType.Warning, this);
                }
            }
        }

        /// <summary>
        /// Here we can set a new timeout value, event if the timer is already running. This can be
        /// used for example to grant players additional time for a task.
        /// </summary>
        /// <param name="timeoutInSeconds"> </param>
        public void SetTimeoutValue(double timeoutInSeconds)
        {
            if (isActive)
            {
                var log = "Overwriting timeout while timer is running. This may lead to unexpected behaviour!";
                Logger.Log(log, LogType.Warning, this);
            }
            this.timeoutInSeconds = timeoutInSeconds;
        }

        /// <summary>
        /// This is the main method to start a new timer.
        /// </summary>
        /// <param name="timeoutInSeconds"> </param>
        public void StartWithNewTimeout(double timeoutInSeconds)
        {
            if (isActive)
            {
                var log = $"Timer already active. Ignoring '{nameof(StartWithNewTimeout)}' prompt.";
                Logger.Log(log, LogType.Warning, this);
            }
            else if (!isActive)
            {
                isActive = true;
                isPaused = false;
                CurrentValue = 0f;
                this.timeoutInSeconds = timeoutInSeconds;
                if (outputDebugMessages)
                {
                    var log = $"Starting new timer. Timer will run for {this.timeoutInSeconds} seconds";
                    Logger.Log(log, LogType.Log, this);
                }
                HasStarted?.Invoke(this, EventArgs.Empty);
            }
        }

        /// <summary>
        /// This stops and resets the timer to zero.
        /// Warning: Erases previous timer value, cannot be undone!
        /// </summary>
        public void StopAndReset()
        {
            if (outputDebugMessages)
            {
                var log = "Stopping and Resetting timer.";
                Logger.Log(log, LogType.Log, this);
            }
            CurrentValue = 0f;
            isActive = false;
        }

        #endregion Public Methods

        #region Private Methods

        /// <summary>
        /// Here we check if the timer is currently running, and if it is, we advance the current
        /// timer value.
        /// </summary>
        private void RunTimerIfAllowed()
        {
            if (IsRunning)
            {
                CurrentValue += Time.deltaTime;
            }
        }

        /// <summary>
        /// Here we check if the timer has reached the timeout value, and if it has, we stop the timer.
        /// </summary>
        private void StopTimerIfTimeoutIsReached()
        {
            if (HasReachedTimeout)
            {
                isActive = false;
                if (outputDebugMessages)
                {
                    var log = $"timeout reached at: {CurrentValue}";
                    Logger.Log(log, LogType.Log, this);
                }
                HasStopped?.Invoke(this, EventArgs.Empty);
            }
        }

        private void Update()
        {
            StopTimerIfTimeoutIsReached();
            RunTimerIfAllowed();
            if (outputDebugMessages)
            {
                var log = $"current timer value is: {CurrentValue}," +
                          $" timer is paused: {isPaused}," +
                          $" timer is active: {isActive}";
                Logger.Log(log, LogType.Log, this);
            }
        }

        #endregion Private Methods
    }
}