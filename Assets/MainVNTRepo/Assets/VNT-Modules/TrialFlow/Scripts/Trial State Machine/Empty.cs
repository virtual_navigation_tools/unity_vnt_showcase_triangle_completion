﻿namespace unibi.vnt.trialflow
{
    /// <summary>
    /// This state is used as the default state of the sate machine, nothing happens while the
    /// machine is in this state.
    /// </summary>
    public class Empty : TrialState
    {
        #region Public Constructors

        public Empty() : base("Empty")
        {
        }

        #endregion Public Constructors

        #region Public Methods

        public override void Enter(TrialStateMachine stateMachine)
        { }

        public override void Exit()
        { }

        #endregion Public Methods
    }
}