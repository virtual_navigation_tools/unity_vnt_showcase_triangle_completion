using System;
using System.Collections;
using UnityEngine;

namespace unibi.homingtask.logistics
{
    public class PlayerManager : MonoBehaviour
    {
        #region Fields

        public static EventHandler<bool> TeleportComplete;

        [SerializeField] private bool canRotate = true;
        [SerializeField] private bool canTranslate = true;
        private bool fadeInDone = false;
        private bool fadeOutDOne = false;

        [SerializeField] private GameObject player;
        [SerializeField] private PlayerController playerController = null;
        [SerializeField] private TurnController turnController = null;

        #endregion Fields

        #region Properties

        public GameObject Player
        { get => player; }

        public bool PlayerHasAcknowledged { get; set; }
        public bool PlayerHasApproachedStart { get; set; }
        public bool PlayerHasCorrectFacing { get; set; }
        public bool PlayerHasEndedHoming { get; set; }
        public bool PlayerHasFinishedDisplacement { get; set; }
        public bool PlayerHasGivenHomingConfidence { get; set; }
        public bool PlayerHasGivenPointingConfidence { get; set; }
        public bool PlayerHasPointed { get; set; }
        public bool PlayerHasQuitExploration { get; set; }
        public bool PlayerHasReachedPointingDistance { get; set; }

        #endregion Properties

        #region Public Methods

        public void FreezePlayer(bool canTranslate = false, bool canRotate = false)
        {
            this.canRotate = canRotate;
            this.canTranslate = canTranslate;
        }

        public void SetMaxPlayerRotationVelocity(float velocityInDegreesPerSecond)
        {
            playerController.MaxAngularVelocityInDegreesPerSecond = velocityInDegreesPerSecond;
        }

        public void SetMaxPlayerTranslationVelocity(float velocityInMetersPerSecond)
        {
            playerController.MaxForwardVelocityInUnitsPerSecond = velocityInMetersPerSecond;
        }

        public void TeleportPlayerToPosition(Vector3 pos, bool isDisplacement = false, bool useAnimation = true, bool preserveElevation = true)
        {
            if (preserveElevation)
            {
                pos.y = transform.position.y;
            }

            if (useAnimation)
            {
                StartCoroutine(DoTeleportCoroutine(pos, playerController.Controller));
            }
            else
            {
                playerController.Controller.enabled = false;
                player.transform.position = pos;
                playerController.Controller.enabled = true;
                TeleportComplete?.Invoke(this, useAnimation);
            }
            if (isDisplacement)
            {
                PlayerHasFinishedDisplacement = true;
            }
        }

        public void TurnPlayerTowards(Vector3 direction)
        {
            PlayerHasCorrectFacing = false;
            turnController.StartTurnGuide(direction);
        }

        public void UnFreezePlayer(bool canTranslate = true, bool canRotate = true)
        {
            this.canRotate = canRotate;
            this.canTranslate = canTranslate;
        }

        #endregion Public Methods

        #region Private Methods

        private IEnumerator DoTeleportCoroutine(Vector3 newPos, CharacterController controller, float duration = 1f)
        {
            controller.enabled = false;
            fadeOutDOne = false;
            fadeInDone = false;
            yield return new WaitUntil(() => fadeOutDOne);
            player.transform.position = newPos;
            // teleport effect could go here
            yield return new WaitUntil(() => fadeInDone);
            controller.enabled = true;
            TeleportComplete?.Invoke(this, true);
        }

        private void OnDestroy()
        {
            TurnController.HasTurned -= OnPlayerHasTurned;
        }

        private void OnPlayerHasTurned(object sender, Quaternion e)
        {
            PlayerHasCorrectFacing = true;
        }

        private void Start()
        {
            TurnController.HasTurned += OnPlayerHasTurned;
        }

        private void Update()
        {
            playerController.MovePlayer(canTranslate, canRotate);
        }

        #endregion Private Methods
    }
}