﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using unibi.vnt.trialflow;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

namespace unibi.homingtask.ui
{
    public class InfoScreenManager : MonoBehaviour
    {
        #region Fields

        [SerializeField] private Button button = null;
        [SerializeField] private TMP_Text buttonText = null;
        private bool hasBeenPressed = false;
        [SerializeField] private TMP_Text infoText = null;
        private bool keepSelectedUntilPressed = false;
        private GameObject lastSelected = null;
        [SerializeField] private LocalisationManager localisationManager = null;
        [SerializeField] private Image mainTextbox = null;
        [SerializeField] private List<string> managedScenes = new List<string>();
        [SerializeField] private Slider progressBar = null;
        [SerializeField] private TMP_Text progressText = null;
        private bool showTimer = false;
        [SerializeField] private Slider slider = null;
        [SerializeField] private Image sliderBackground = null;
        [SerializeField] private Slider timeoutBar = null;
        private PausableTimer timer = null;
        [SerializeField] private TMP_Text timerText = null;
        [SerializeField] private TMP_Text toggleInfo = null;
        [SerializeField] private UIStrings uiStrings = null;

        #endregion Fields

        #region Properties

        public Button Button { get => button; }
        public List<string> ManagedScenes { get => managedScenes; set => managedScenes = value; }
        public Slider Slider { get => slider; }
        public UIStrings UiStrings { get => uiStrings; }

        #endregion Properties

        #region Public Methods

        public void ClearScreen()
        {
            HideToggleInfo();
            mainTextbox.gameObject.SetActive(false);
            slider.gameObject.SetActive(false);
            button.gameObject.SetActive(false);
            sliderBackground.gameObject.SetActive(false);
            HideTimer();
        }

        public void HideProgress()
        {
            progressText.gameObject.SetActive(false);
            progressBar.gameObject.SetActive(false);
        }

        public void HideTimer()
        {
            timerText.gameObject.SetActive(false);
            showTimer = false;
            timeoutBar.gameObject.SetActive(false);
        }

        public void HideToggleInfo()
        {
            toggleInfo.gameObject.SetActive(false);
        }

        public void ResetAndShowSlider()
        {
            sliderBackground.gameObject.SetActive(true);
            slider.gameObject.SetActive(true);
            float resetVal = 50;
            slider.SetValueWithoutNotify(resetVal);
            EventSystem.current.SetSelectedGameObject(null);
            EventSystem.current.SetSelectedGameObject(slider.gameObject);
        }

        public void SetButtonForExploration(UnityAction playerHasQuitExplorationListener)
        {
            InitButton(uiStrings.ExploreBtnTxt, playerHasQuitExplorationListener);
        }

        public void SetButtonForFinish(UnityAction playerQuitSessionListener)
        {
            InitButton(uiStrings.OKBtnTxt, playerQuitSessionListener);
        }

        public void SetButtonForHoming(UnityAction playerEndedHomingListener)
        {
            InitButton(uiStrings.HomeBtnTxt, playerEndedHomingListener);
        }

        public void SetButtonForHomingConfidence(UnityAction playerHomingConfidenceListener)
        {
            InitButton(uiStrings.DoneBtnTxt, playerHomingConfidenceListener);
        }

        public void SetButtonForInfo(UnityAction playerHasAcknowledgedListener)
        {
            InitButton(uiStrings.OKBtnTxt, playerHasAcknowledgedListener, keepSelectedUntilPressed: true);
        }

        public void SetButtonForPointing(UnityAction playerHasPointedListener)
        {
            InitButton(uiStrings.PointBtnTxt, playerHasPointedListener);
        }

        public void SetButtonForPointingConfidence(UnityAction playerPointingConfidenceListener)
        {
            // there was a SetActive() here for some reason?
            InitButton(uiStrings.DoneBtnTxt, playerPointingConfidenceListener);
        }

        public void ShowBoxWithText(string text)
        {
            mainTextbox.gameObject.SetActive(true);
            infoText.SetText(text);
        }

        public void ShowProgress(int current, int total)
        {
            progressText.text = string.Format("Trial {0}/{1}", current, total);
            progressText.gameObject.SetActive(true);
            progressBar.gameObject.SetActive(true);
            progressBar.value = (float)current / total;
        }

        public void ShowTimer(PausableTimer timer)
        {
            timerText.gameObject.SetActive(true);
            this.timer = timer;
            showTimer = true;
            timeoutBar.gameObject.SetActive(true);
        }

        public void ShowToggleInfo()
        {
            toggleInfo.text = "(" + uiStrings.ToggleInfoText + ")";
            toggleInfo.gameObject.SetActive(true);
        }

        #endregion Public Methods

        #region Private Methods

        private IEnumerator EnableButtonAfterSeconds(Button button, float duration)
        {
            button.interactable = false;
            yield return new WaitForSecondsRealtime(duration);
            button.interactable = true;
            EventSystem.current.SetSelectedGameObject(null);
            EventSystem.current.SetSelectedGameObject(button.gameObject);
            yield break;
        }

        private void HandleButtonPressed()
        {
            hasBeenPressed = true;
        }

        private void InitButton(string text, UnityAction listener, bool keepSelectedUntilPressed = false)
        {
            button.onClick.RemoveAllListeners();
            button.onClick.AddListener(listener);
            this.keepSelectedUntilPressed = keepSelectedUntilPressed;
            if (keepSelectedUntilPressed)
            {
                hasBeenPressed = false;
                button.onClick.AddListener(HandleButtonPressed);
            }
            buttonText.SetText(text);
            button.gameObject.SetActive(true);
            StartCoroutine(EnableButtonAfterSeconds(button, 0.5f));
        }

        private bool IsInteractableButton(GameObject gameObject)
        {
            var button = gameObject.GetComponent<Button>();
            return gameObject.activeSelf && button != null && button.interactable;
        }

        private void KeepButtonSelectedUntilPressed(Button button)
        {
            if (!keepSelectedUntilPressed)
            {
                return;
            }
            if (hasBeenPressed)
            {
                keepSelectedUntilPressed = false;
                return;
            }
            EventSystem.current.SetSelectedGameObject(null);
            EventSystem.current.SetSelectedGameObject(button.gameObject);
        }

        private void OnDestroy()
        {
            SceneManager.sceneLoaded -= OnSceneLoaded;
        }

        private void OnEnable()
        {
            SceneManager.sceneLoaded += OnSceneLoaded;
        }

        private void OnSceneLoaded(Scene scene, LoadSceneMode mode)
        {
            if (managedScenes.Contains(scene.name))
            {
                uiStrings = localisationManager.GetUiStrings();
            }
        }

        // restore last selected button see here: https://gamedev.stackexchange.com/questions/167463/prevent-ui-button-focus-loss-when-clicking-somewhere-else
        private void RestoreLastSelectedButton()
        {
            if (lastSelected == null)
            {
                lastSelected = EventSystem.current.currentSelectedGameObject;
            }
            if (EventSystem.current.currentSelectedGameObject == null && lastSelected != null)
            {
                if (IsInteractableButton(lastSelected))
                {
                    EventSystem.current.SetSelectedGameObject(lastSelected);
                }
            }
        }

        private void Start()
        {
            ClearScreen();
        }

        private void Update()
        {
            RestoreLastSelectedButton();
            KeepButtonSelectedUntilPressed(button);
            if (!showTimer)
            {
                return;
            }
            if (timer == null)
            {
                return;
            }
            UpdateTimerBarAndText();
        }

        private void UpdateTimerBarAndText()
        {
            timerText.SetText(uiStrings.TimeoutTxt + (int)(timer.TimeoutInSeconds - timer.CurrentValue));
            timeoutBar.value = (float)((timer.TimeoutInSeconds - timer.CurrentValue) / timer.TimeoutInSeconds);
        }

        #endregion Private Methods
    }
}