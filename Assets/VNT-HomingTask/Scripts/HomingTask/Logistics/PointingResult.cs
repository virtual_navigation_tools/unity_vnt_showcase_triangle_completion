﻿using UnityEngine;

namespace unibi.homingtask.logistics
{
    public class PointingResult
    {
        #region Public Constructors

        public PointingResult(Vector3 pointingOrigin, Vector3 estimatedGoalPosition, Vector3 trueGoalPosition, float playerConfidence)
        {
            PointingOrigin = pointingOrigin;
            EstimatedGoalPosition = estimatedGoalPosition;
            TrueGoalPosition = trueGoalPosition;
            PlayerConfidence = playerConfidence;
        }

        #endregion Public Constructors

        #region Properties

        public Vector3 EstimatedGoalPosition { get; private set; }
        public float PlayerConfidence { get; private set; }
        public Vector3 PointingOrigin { get; private set; }
        public Vector3 TrueGoalPosition { get; private set; }

        #endregion Properties

        #region Public Methods

        public float PointingDistance()
        {
            return Vector3.Distance(PointingOrigin, TrueGoalPosition);
        }

        #endregion Public Methods
    }
}