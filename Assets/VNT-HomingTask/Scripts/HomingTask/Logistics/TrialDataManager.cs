﻿using System.Collections.Generic;
using UnityEngine;
using UXF;

namespace unibi.homingtask.logistics
{
    public class TrialDataManager : MonoBehaviour
    {
        #region Fields

        [SerializeField] private TrialDataProcessor dataProcessor;
        private List<PointingResult> pointingResults = new List<PointingResult>();
        [SerializeField] private TrialData trialData;

        #endregion Fields

        #region Properties

        public TrialData TrialData { get => trialData; }

        #endregion Properties

        #region Public Methods

        public void SavePointingResult(Trial currentTrial, int pointingRep)
        {
            var result = GeneratePointingResultForCurrentTrial();
            dataProcessor.WritePointingResultToTrialResults(result, currentTrial, pointingRep);
            pointingResults.Add(result);
        }

        public void SaveTrialResult(Trial currentTrial)
        {
            trialData.StopTime = Time.time;
            var log = GenerateTrialLogForCurrentTrial();
            dataProcessor.WriteMetaDataToTrialResults(currentTrial);
            dataProcessor.WriteHomingLogToTrialResults(log, currentTrial);
            pointingResults.Clear();
        }

        #endregion Public Methods

        #region Private Methods

        private PointingResult GeneratePointingResultForCurrentTrial()
        {
            return new PointingResult(trialData.CurrentPointingPosition,
                trialData.CurrentPointingGoalEstimate,
                trialData.CurrentTrialSettings.GoalPosition,
                trialData.CurrentPointingConfidence);
        }

        private TrialLog GenerateTrialLogForCurrentTrial()
        {
            return new TrialLog(trialData.CurrentTrialSettings.HomingStartPosition,
                trialData.CurrentHomingGoalEstimate,
                trialData.CurrentTrialSettings.GoalPosition,
                pointingResults,
                trialData.CurrentHomingConfidence);
        }

        #endregion Private Methods
    }
}