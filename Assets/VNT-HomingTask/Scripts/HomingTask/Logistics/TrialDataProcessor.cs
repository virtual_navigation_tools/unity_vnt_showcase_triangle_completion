﻿using System;
using unibi.homingtask.settings;
using UnityEngine;
using UXF;

namespace unibi.homingtask.logistics
{
    public class TrialDataProcessor : MonoBehaviour
    {
        #region Fields

        [SerializeField]
        private HeaderStrings headerStrings;

        #endregion Fields

        #region Public Methods

        public void WriteHomingLogToTrialResults(TrialLog homingLog, Trial trial)
        {
            #region write homing result data

            trial.result[headerStrings.startPosHeaderX] = homingLog.StartPosition.x;
            trial.result[headerStrings.startPosHeaderY] = homingLog.StartPosition.y;
            trial.result[headerStrings.startPosHeaderZ] = homingLog.StartPosition.z;
            trial.result[headerStrings.estimateHeaderX] = homingLog.EstimatedGoalPosition.x;
            trial.result[headerStrings.estimateHeaderY] = homingLog.EstimatedGoalPosition.y;
            trial.result[headerStrings.estimateHeaderZ] = homingLog.EstimatedGoalPosition.z;
            trial.result[headerStrings.goalHeaderX] = homingLog.TrueGoalPosition.x;
            trial.result[headerStrings.goalHeaderY] = homingLog.TrueGoalPosition.y;
            trial.result[headerStrings.goalHeaderZ] = homingLog.TrueGoalPosition.z;
            trial.result[headerStrings.confidenceHeader] = homingLog.PlayerConfidence;

            #endregion write homing result data
        }

        public void WriteMetaDataToTrialResults(Trial trial)
        {
            var trialSettings = (TrialSettings)trial.settings.GetObject("TrialSettings");
            trial.result[headerStrings.trialSavedTimestampHeader] = DateTime.Now.ToUniversalTime().ToString("dd/MM/yyyy HH:mm:ss.fff");
            trial.result[headerStrings.numLandmarksHeader] = trialSettings.LandmarkPositions.Count;
            trial.result[headerStrings.trialNameHeader] = trialSettings.Name;
            trial.result[headerStrings.isGuidedHeader] = trialSettings.IsGuidedTrial;
            trial.result[headerStrings.isTrainingHeader] = trialSettings.IsTraining;
            trial.result[headerStrings.displacementModeHeader] = trialSettings.Mode.ToString();
        }

        public void WritePointingResultToTrialResults(PointingResult pResult, Trial trial, int currentRep)
        {
            string pTrialNum = "_ptrial_" + (currentRep).ToString();

            #region write pointing result data

            trial.result[headerStrings.startPosHeaderX + pTrialNum] = pResult.PointingOrigin.x;
            trial.result[headerStrings.startPosHeaderY + pTrialNum] = pResult.PointingOrigin.y;
            trial.result[headerStrings.startPosHeaderZ + pTrialNum] = pResult.PointingOrigin.z;
            trial.result[headerStrings.estimateHeaderX + pTrialNum] = pResult.EstimatedGoalPosition.x;
            trial.result[headerStrings.estimateHeaderY + pTrialNum] = pResult.EstimatedGoalPosition.y;
            trial.result[headerStrings.estimateHeaderZ + pTrialNum] = pResult.EstimatedGoalPosition.z;
            trial.result[headerStrings.goalHeaderX + pTrialNum] = pResult.TrueGoalPosition.x;
            trial.result[headerStrings.goalHeaderY + pTrialNum] = pResult.TrueGoalPosition.y;
            trial.result[headerStrings.goalHeaderZ + pTrialNum] = pResult.TrueGoalPosition.z;
            trial.result[headerStrings.confidenceHeader + pTrialNum] = pResult.PlayerConfidence;

            #endregion write pointing result data
        }

        #endregion Public Methods
    }
}