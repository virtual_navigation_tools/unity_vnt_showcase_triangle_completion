﻿using UnityEngine;
using UnityEngine.UI;

namespace unibi.homingtask.ui
{
    public class RenderUIOnTop : MonoBehaviour
    {
        #region Private Methods

        // Start is called before the first frame update
        private void Start()
        {
            foreach (var image in gameObject.GetComponentsInChildren<Image>())
            {
                image.material = UpdateRenderMaterial(image.materialForRendering);
            }
            foreach (var graphic in gameObject.GetComponentsInChildren<Graphic>())
            {
                graphic.material = UpdateRenderMaterial(graphic.materialForRendering);
            }
        }

        private Material UpdateRenderMaterial(Material materialForRendering)
        {
            UnityEngine.Rendering.CompareFunction comparison = UnityEngine.Rendering.CompareFunction.Always;
            Material updatedMaterial = new Material(materialForRendering);
            updatedMaterial.SetInt("unity_GUIZTestMode", (int)comparison);
            return updatedMaterial;
        }

        #endregion Private Methods
    }
}