﻿using UnityEngine;

namespace unibi.homingtask.logistics
{
    [CreateAssetMenu(fileName = "HeaderStrings", menuName = "ScriptableObjects/DataManagement/HeaderStrings", order = 1)]
    public class HeaderStrings : ScriptableObject
    {
        #region Fields

        public string confidenceHeader = "confidence";
        public string displacementModeHeader = "displacement_mode";
        public string estimateHeaderX = "goal_estimat_x";
        public string estimateHeaderY = "goal_estimate_y";
        public string estimateHeaderZ = "goal_estimate_z";
        public string goalHeaderX = "goal_true_x";
        public string goalHeaderY = "goal_true_y";
        public string goalHeaderZ = "goal_true_z";
        public string isGuidedHeader = "was_guided";
        public string isTrainingHeader = "was_training";
        public string numLandmarksHeader = "num_lm";
        public string startPosHeaderX = "start_pos_x";
        public string startPosHeaderY = "start_pos_y";
        public string startPosHeaderZ = "start_pos_z";
        public string trialNameHeader = "trial_name";
        public string trialSavedTimestampHeader = "save_time";

        #endregion Fields
    }
}