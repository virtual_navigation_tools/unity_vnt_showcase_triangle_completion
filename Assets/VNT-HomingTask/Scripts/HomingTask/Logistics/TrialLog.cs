﻿using System.Collections.Generic;
using UnityEngine;
using static unibi.homingtask.logistics.TrialDataProcessor;

namespace unibi.homingtask.logistics
{
    public class TrialLog
    {
        #region Public Constructors

        public TrialLog(Vector3 startPosition, Vector3 estimatedGoalPosition, Vector3 trueGoalPosition, List<PointingResult> pointingResults, float playerConfidence)
        {
            StartPosition = startPosition;
            EstimatedGoalPosition = estimatedGoalPosition;
            TrueGoalPosition = trueGoalPosition;
            PointingResults = pointingResults;
            PlayerConfidence = playerConfidence;
        }

        #endregion Public Constructors

        #region Properties

        public Vector3 EstimatedGoalPosition { get; private set; }
        public float PlayerConfidence { get; private set; }
        public List<PointingResult> PointingResults { get; private set; }
        public Vector3 StartPosition { get; private set; }
        public Vector3 TrueGoalPosition { get; private set; }

        #endregion Properties
    }
}