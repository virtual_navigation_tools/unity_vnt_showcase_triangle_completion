﻿using unibi.vnt.util;
using UnityEngine;

namespace unibi.homingtask.logistics
{
    public class FeedbackManager : MonoBehaviour
    {
        #region Fields

        [SerializeField] private GameObject estimateMarker = null;
        [SerializeField] private GameObject goalMarker = null;

        #endregion Fields

        #region Public Methods

        public float GetScoreForCurrentTrial(TrialData trialData, bool useFakeGoalForScore)
        {
            return CalculateScore(trialData, useFakeGoalForScore);
        }

        public void HideFeebackObjects()
        {
            goalMarker.SetActive(false);
            goalMarker.ToggleAllRenderersInChildren(setEnabled: false);
            estimateMarker.SetActive(false);
            estimateMarker.ToggleAllRenderersInChildren(setEnabled: false);
        }

        public void ShowHomingError(TrialData trialData, bool useFakeGoal)
        {
            // show "true" goal
            if (useFakeGoal == true)
            {
                goalMarker.transform.position = trialData.CurrentTrialSettings.FakeGoalPosition;
            }
            else
            {
                goalMarker.transform.position = trialData.CurrentTrialSettings.GoalPosition;
            }
            goalMarker.SetActive(true);
            goalMarker.ToggleAllRenderersInChildren(setEnabled: true);

            // show estimate
            estimateMarker.transform.position = trialData.CurrentHomingGoalEstimate;
            estimateMarker.SetActive(true);
            estimateMarker.ToggleAllRenderersInChildren(setEnabled: true);
        }

        #endregion Public Methods

        #region Private Methods

        private float CalculateScore(TrialData trialData, bool useFakeGoalForScore)
        {
            var referencePosition = trialData.CurrentTrialSettings.GoalPosition;
            // overwrite refpos if needed
            if (useFakeGoalForScore)
            {
                referencePosition = trialData.CurrentTrialSettings.FakeGoalPosition;
            }
            var posError = (trialData.CurrentHomingGoalEstimate - referencePosition).magnitude;
            var timeTaken = trialData.StopTime - trialData.StartTime;

            // arbitrary default values
            var basevalue = 1000;
            var basDistance = 50;
            var baseTime = 60;

            return basevalue * (basDistance / posError) * (timeTaken / baseTime);
        }

        private void Start()
        {
            HideFeebackObjects();
        }

        #endregion Private Methods
    }
}