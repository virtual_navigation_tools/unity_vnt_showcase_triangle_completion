﻿using UnityEngine;

namespace unibi.homingtask.logistics
{
    public class PlayerController : MonoBehaviour
    {
        #region Fields

        [SerializeField] private CharacterController characterController = null;
        [SerializeField] private float maxAngularVelocityInDegreesPerSecond = 60f;
        [SerializeField] private float maxForwardVelocityInUnitsPerSecond = 2.5f;
        [SerializeField] private GameObject player;
        [SerializeField] private bool useMouseRotate = false;

        #endregion Fields

        #region Properties

        public CharacterController Controller { get => characterController; }

        public float MaxAngularVelocityInDegreesPerSecond
        {
            get => maxAngularVelocityInDegreesPerSecond;
            set => maxAngularVelocityInDegreesPerSecond = value;
        }

        public float MaxForwardVelocityInUnitsPerSecond
        {
            get => maxForwardVelocityInUnitsPerSecond;
            set => maxForwardVelocityInUnitsPerSecond = value;
        }

        #endregion Properties

        #region Public Methods

        public void MovePlayer(bool canTranslate, bool canRotate)
        {
            if (canTranslate)
            {
                // Move forward / backward
                Vector3 forward = transform.TransformDirection(Vector3.forward);
                float curSpeed = maxForwardVelocityInUnitsPerSecond * Input.GetAxis("Translation");
                characterController.SimpleMove(forward * curSpeed);
            }
            else
            {
                characterController.SimpleMove(Vector3.zero);
            }
            if (canRotate)
            {
                // Rotate around y - axis
                var input = Input.GetAxis("Rotation");
                if (useMouseRotate == true)
                {
                    input = Input.GetAxis("Mouse X");
                }
                player.transform.Rotate(0, input * maxAngularVelocityInDegreesPerSecond * Time.deltaTime, 0);
            }
        }

        #endregion Public Methods
    }
}