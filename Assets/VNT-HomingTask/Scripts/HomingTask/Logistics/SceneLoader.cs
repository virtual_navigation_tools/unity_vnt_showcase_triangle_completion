using UnityEngine;
using UnityEngine.SceneManagement;
using UXF;

namespace unibi.homingtask.logistics
{
    public class SceneLoader : MonoBehaviour
    {
        #region Public Methods

        public void OnUXFSessionStarted(Session session)
        {
            SceneManager.LoadScene("VNT_Session_Selector", LoadSceneMode.Single);
        }

        #endregion Public Methods
    }
}