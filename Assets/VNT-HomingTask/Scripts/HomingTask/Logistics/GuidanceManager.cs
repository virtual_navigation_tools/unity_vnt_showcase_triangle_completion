﻿using System.Collections.Generic;
using unibi.vnt.guidance;
using unibi.vnt.util;
using UnityEngine;

namespace unibi.homingtask.logistics
{
    public class GuidanceManager : MonoBehaviour
    {
        #region Fields

        /// <summary>
        /// this is an arrow object, that can be set to point at the next waypoint.
        /// </summary>
        [SerializeField] private Arrow arrowPrefab;

        /// <summary>
        /// we make use of the IGuidanceController interface and can decide somewhere else, which
        /// implementation to use.
        /// </summary>
        private IGuidanceController controller;

        [SerializeField] private GameObject waypointPrefab;

        #endregion Fields

        #region Properties

        /// <summary>
        /// we can use an external logger to toggle logging and format the output.
        /// </summary>
        public ICustomLogger Logger { get; set; }

        #endregion Properties

        #region Public Methods

        public List<IWaypoint> CreateWaypoints(List<Vector3> positions, GameObject player, float radius = 2)
        {
            var waypoints = new List<IWaypoint>();
            int num = 0;
            foreach (var pos in positions)
            {
                num += 1;
                GameObject newObj = Instantiate(waypointPrefab, pos, Quaternion.identity);
                if (!newObj.TryGetComponent<WaypointWithTriggerRadius>(out var waypoint))
                {
                    waypoint = newObj.AddComponent<WaypointWithTriggerRadius>();
                }

                waypoint.Initialise(player, "WP_" + num, radius);
                waypoints.Add(waypoint);
            }
            return waypoints;
        }

        public void Initialise(List<Vector3> waypointPositions, GameObject player)
        {
            List<IWaypoint> waypoints = CreateWaypoints(waypointPositions, player);
            controller.Reset();
            controller.Initialise(waypoints, player);
        }

        public void PauseGuidance()
        {
            GuidanceState controllerState = controller.GetCurrentState();
            if (controllerState == GuidanceState.Active)
            {
                controller.PauseGuidance();
            }
            else
            {
                var log = string.Format("Cannot pause guidance, controller is in state {0}.",
                                controllerState);
                Logger.Log(log, LogType.Warning, this, VNTLogFormatter.Example_Tag);
            }
        }

        public void ResetController()
        {
            controller.Reset();
        }

        public void ResumeGuidance()
        {
            GuidanceState controllerState = controller.GetCurrentState();
            if (controllerState == GuidanceState.Paused)
            {
                controller.ResumeGuidance();
            }
            else
            {
                var log = string.Format("Cannot resume guidance, controller is in state {0}.",
                                controllerState);
                Logger.Log(log, LogType.Warning, this, VNTLogFormatter.Example_Tag);
            }
        }

        public void StartGuidance()
        {
            GuidanceState controllerState = controller.GetCurrentState();
            if (controllerState == GuidanceState.Inactive && controllerState != GuidanceState.Uninitialised)
            {
                controller.StartGuidance();
            }
            else
            {
                var log = string.Format("Cannot start guidance, controller is in state {0}.",
                                controllerState);
                Logger.Log(log, LogType.Warning, this, VNTLogFormatter.Example_Tag);
            }
        }

        public void StopGuidance()
        {
            GuidanceState controllerState = controller.GetCurrentState();
            if (controllerState == GuidanceState.Active)
            {
                controller.StopGuidance();
            }
            else
            {
                var log = string.Format("Cannot stop guidance, controller is in state {0}.",
                                controllerState);
                Logger.Log(log, LogType.Warning, this, VNTLogFormatter.Example_Tag);
            }
        }

        #endregion Public Methods

        #region Private Methods

        private void Start()
        {
            controller = new HomingTaskGuidanceController(arrowPrefab);
        }

        #endregion Private Methods
    }
}