﻿using UnityEngine;

namespace unibi.homingtask.ui
{
    public class LocalisationManager : MonoBehaviour
    {
        #region Fields

        [SerializeField] private Language currentLanguage = Language.English;
        [SerializeField] private UIStrings englishStrings;
        [SerializeField] private UIStrings germanStrings;

        #endregion Fields

        #region Enums

        private enum Language
        { English, German };

        #endregion Enums

        #region Public Methods

        public void EnableEnglish()
        {
            currentLanguage = Language.English;
        }

        public void EnableGerman()
        {
            currentLanguage = Language.German;
        }

        public UIStrings GetUiStrings()
        {
            switch (currentLanguage)
            {
                case Language.English:
                    return englishStrings;

                case Language.German:
                    return germanStrings;

                default:
                    return englishStrings;
            }
        }

        #endregion Public Methods
    }
}