﻿using UnityEngine;

namespace unibi.homingtask.logistics
{
    public class PersistentObject : MonoBehaviour
    {
        #region Fields

        private static GameObject instance = null;

        #endregion Fields

        #region Public Methods

        public void SetTargetFramerate(string content)
        {
            int fps = int.Parse(content);
            Debug.LogFormat($"setting target fps to: {fps}");
            Application.targetFrameRate = fps;
        }

        #endregion Public Methods

        #region Private Methods

        private void Awake()
        {
            if (instance == null)
            {
                instance = gameObject;
                DontDestroyOnLoad(gameObject);
            }
            else
            {
                Destroy(gameObject);
            }
            int staticFPS = 30;
            Debug.LogFormat($"setting default fps to: {staticFPS}");
            Application.targetFrameRate = staticFPS;
        }

        #endregion Private Methods
    }
}