﻿using System;
using System.Collections;
using unibi.vnt.guidance;
using UnityEngine;

namespace unibi.homingtask.logistics
{
    public class TurnController : MonoBehaviour
    {
        #region Fields

        [SerializeField] private Arrow arrow;
        private bool isTurning;
        [SerializeField] private GameObject player;
        private Vector3 targetDirection;
        private GameObject turnTarget;

        #endregion Fields

        #region Events

        public static event EventHandler<Quaternion> HasTurned;

        #endregion Events

        #region Public Methods

        public void EndTurnGuide()
        {
            arrow.DisableAndHide();
            turnTarget.SetActive(false);
        }

        public void StartTurnGuide(Vector3 targetDirection)
        {
            SetTurnTarget(targetDirection, player);
            arrow.SetTarget(turnTarget);
            arrow.PointAtTarget();
            isTurning = true;
        }

        #endregion Public Methods

        #region Private Methods

        private void CheckPlayerRotation(float toleranceInDegrees)
        {
            var playerRot = player.transform.rotation;
            var angle = Quaternion.Angle(playerRot, Quaternion.LookRotation(targetDirection));
            if (angle <= toleranceInDegrees)
            {
                HandleTurnComplete();
            }
        }

        private void HandleTurnComplete()
        {
            isTurning = false;
            HasTurned?.Invoke(this, Quaternion.Euler(targetDirection));
            StartCoroutine(HighlightCorrectDirection(durationInSeconds: 0.5f));
        }

        private IEnumerator HighlightCorrectDirection(float durationInSeconds)
        {
            var arrowMat = arrow.gameObject.GetComponentInChildren<Renderer>().material;
            var originalColor = arrowMat.color;
            arrowMat.color = new Color(1, 0, 0);
            yield return new WaitForSecondsRealtime(durationInSeconds);
            arrowMat.color = originalColor;
            EndTurnGuide();
        }

        private void OnDestroy()
        {
            arrow.DisableAndHide();
        }

        private void SetTurnTarget(Vector3 targetDir, GameObject player)
        {
            this.targetDirection = targetDir;
            turnTarget.SetActive(true);
            var playerPos = player.transform.position;
            float arrowHeight = arrow.transform.position.y;
            var targetPos = new Vector3(player.transform.position.x, arrowHeight, player.transform.position.z) + this.targetDirection * 10f;
            turnTarget.transform.position = targetPos;
        }

        private void Start()
        {
            turnTarget = new GameObject();
            arrow.DisableAndHide();
            arrow.AnchorToPlayer(player);
        }

        private void Update()
        {
            if (isTurning)
            {
                CheckPlayerRotation(toleranceInDegrees: 5f);
            }
        }

        #endregion Private Methods
    }
}