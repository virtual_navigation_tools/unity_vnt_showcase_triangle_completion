﻿using unibi.homingtask.settings;
using unibi.vnt.trialflow;
using UnityEngine;
using UnityEngine.Events;

namespace unibi.homingtask.ui
{
    public class UIManager : MonoBehaviour
    {
        #region Fields

        [SerializeField] private InfoScreenManager infoScreenManager;

        #endregion Fields

        #region Public Methods

        public float GetCurrentConfidenceSliderValue()
        {
            return infoScreenManager.Slider.value;
        }

        public void SetScreenForDisplacement(PausableTimer displacementTimer)
        {
            Cursor.lockState = CursorLockMode.Locked;
            Cursor.visible = false;
            infoScreenManager.ClearScreen();
            infoScreenManager.ShowTimer(displacementTimer);
        }

        public void SetScreenForExploration(UnityAction responseListener, PausableTimer explorationTimer)
        {
            Cursor.lockState = CursorLockMode.Locked;
            Cursor.visible = false;
            infoScreenManager.ClearScreen();
            infoScreenManager.SetButtonForExploration(responseListener);
            infoScreenManager.ShowTimer(explorationTimer);
            infoScreenManager.ShowToggleInfo();
        }

        public void SetScreenforHomeApproach()
        {
            Cursor.lockState = CursorLockMode.Locked;
            Cursor.visible = false;
            infoScreenManager.ClearScreen();
        }

        public void ShowSessionDoneScreen(UnityAction responseListener)
        {
            Cursor.lockState = CursorLockMode.Locked;
            Cursor.visible = false;
            infoScreenManager.ClearScreen();
            var text = infoScreenManager.UiStrings.SessionDoneTxt;
            infoScreenManager.ShowBoxWithText(text);
            infoScreenManager.SetButtonForFinish(responseListener);
        }

        public void SetScreenForHoming(PausableTimer homingTimer, UnityAction responseListener)
        {
            Cursor.lockState = CursorLockMode.Locked;
            Cursor.visible = false;
            infoScreenManager.ClearScreen();
            infoScreenManager.ShowTimer(homingTimer);
            infoScreenManager.SetButtonForHoming(responseListener);
            infoScreenManager.ShowToggleInfo();
        }

        public void SetScreenForPointing(UnityAction responseListener, PausableTimer pointingTimer)
        {
            Cursor.lockState = CursorLockMode.Confined;
            Cursor.visible = true;
            infoScreenManager.ClearScreen();
            infoScreenManager.SetButtonForPointing(responseListener);
            infoScreenManager.ShowTimer(pointingTimer);
        }

        public void SetScreenForTeleport()
        {
            Cursor.lockState = CursorLockMode.Locked;
            Cursor.visible = false;
            infoScreenManager.ClearScreen();
        }

        public void SetScreenForTrialEnd()
        {
            Cursor.lockState = CursorLockMode.Locked;
            Cursor.visible = false;
            infoScreenManager.ClearScreen();
        }

        public void ShowDisplacementInfo(TrialSettings.DisplacementMode mode, UnityAction responseListener)
        {
            Cursor.lockState = CursorLockMode.Locked;
            Cursor.visible = false;
            infoScreenManager.ClearScreen();
            var text = "";
            if (mode == TrialSettings.DisplacementMode.Walking)
            {
                text = infoScreenManager.UiStrings.WaypointInfoTxt;
            }
            else
            {
                text = infoScreenManager.UiStrings.TeleportInfoTxt;
            }

            infoScreenManager.ShowBoxWithText(text);
            infoScreenManager.SetButtonForInfo(responseListener);
        }

        public void ShowExplorationInfo(UnityAction responseListener)
        {
            Cursor.lockState = CursorLockMode.Locked;
            Cursor.visible = false;

            infoScreenManager.ClearScreen();
            var text = infoScreenManager.UiStrings.ExploreInfoTxt;
            infoScreenManager.ShowBoxWithText(text);
            infoScreenManager.SetButtonForInfo(responseListener);
        }

        public void ShowFeedbackInfo(UnityAction responseListener)
        {
            Cursor.lockState = CursorLockMode.Locked;
            Cursor.visible = false;
            infoScreenManager.ClearScreen();
            var text = infoScreenManager.UiStrings.FeedbackInfoTxt;
            infoScreenManager.ShowBoxWithText(text);
            infoScreenManager.SetButtonForInfo(responseListener);
        }

        public void ShowGoalApproachInfo()
        {
            Cursor.lockState = CursorLockMode.Locked;
            Cursor.visible = false;
            infoScreenManager.ClearScreen();
            var text = infoScreenManager.UiStrings.ApproachInfoText;
            infoScreenManager.ShowBoxWithText(text);
        }

        public void ShowGuidedReturnStartInfo(UnityAction responseListener)
        {
            Cursor.lockState = CursorLockMode.Locked;
            Cursor.visible = false;
            infoScreenManager.ClearScreen();
            var text = infoScreenManager.UiStrings.GuidedReturnInfoText;
            infoScreenManager.ShowBoxWithText(text);
            infoScreenManager.SetButtonForInfo(responseListener);
        }

        public void ShowHomingStartInfo(bool isFirstHomingBout, bool islastHomingBout, UnityAction responseListener)
        {
            Cursor.lockState = CursorLockMode.Locked;
            Cursor.visible = false;
            infoScreenManager.ClearScreen();
            var text = "";
            if (isFirstHomingBout)
            {
                text = infoScreenManager.UiStrings.HomingStartInfoTxt;
            }
            else
            {
                text = infoScreenManager.UiStrings.HomingContinueInfoTxt;
            }
            if (islastHomingBout)
            {
                text += infoScreenManager.UiStrings.HomeReachableInfoTxt;
            }
            infoScreenManager.ShowBoxWithText(text);
            infoScreenManager.SetButtonForInfo(responseListener);
        }

        public void ShowHomingTrialConfidenceRequest(UnityAction responseListener)
        {
            Cursor.lockState = CursorLockMode.Locked;
            Cursor.visible = false;
            infoScreenManager.ClearScreen();
            infoScreenManager.ResetAndShowSlider();
            var text = infoScreenManager.UiStrings.TrialConfTxt;
            infoScreenManager.ShowBoxWithText(text);
            infoScreenManager.SetButtonForHomingConfidence(responseListener);
        }

        public void ShowMockTrialConfidenceRequest(UnityAction responseListener)
        {
            Cursor.lockState = CursorLockMode.Locked;
            Cursor.visible = false;
            infoScreenManager.ClearScreen();
            infoScreenManager.ResetAndShowSlider();
            var text = infoScreenManager.UiStrings.MockTrialConfidenceRequestText;
            infoScreenManager.ShowBoxWithText(text);
            infoScreenManager.SetButtonForHomingConfidence(responseListener);
        }

        public void ShowPointingInfo(UnityAction responseListener)
        {
            Cursor.lockState = CursorLockMode.Locked;
            Cursor.visible = false;
            infoScreenManager.ClearScreen();
            var text = infoScreenManager.UiStrings.PointingInfoTxt;
            infoScreenManager.ShowBoxWithText(text);
            infoScreenManager.SetButtonForInfo(responseListener);
        }

        public void ShowPointingTimeoutInfo(UnityAction responseListener)
        {
            Cursor.lockState = CursorLockMode.Locked;
            Cursor.visible = false;
            infoScreenManager.ClearScreen();
            var text = infoScreenManager.UiStrings.PointingTimeoutTxt;
            infoScreenManager.ShowBoxWithText(text);
            infoScreenManager.SetButtonForInfo(responseListener);
        }

        public void ShowPointingTrialConfidenceRequest(UnityAction responseListener)
        {
            Cursor.lockState = CursorLockMode.Locked;
            Cursor.visible = false;
            infoScreenManager.ClearScreen();
            infoScreenManager.ResetAndShowSlider();
            var text = infoScreenManager.UiStrings.PointingConfTxt;
            infoScreenManager.ShowBoxWithText(text);
            infoScreenManager.SetButtonForPointingConfidence(responseListener);
        }

        public void ShowScoreInfo(float score, UnityAction responseListener)
        {
            Cursor.lockState = CursorLockMode.Locked;
            Cursor.visible = false;
            infoScreenManager.ClearScreen();
            var text = infoScreenManager.UiStrings.ScoreTxt + ((int)score).ToString();
            infoScreenManager.ShowBoxWithText(text);
            infoScreenManager.SetButtonForInfo(responseListener);
        }

        public void ShowTrialTimeOutInfo(UnityAction responseListener)
        {
            Cursor.lockState = CursorLockMode.Locked;
            Cursor.visible = false;
            infoScreenManager.ClearScreen();
            var text = infoScreenManager.UiStrings.TrialTimeoutTxt;
            infoScreenManager.ShowBoxWithText(text);
            infoScreenManager.SetButtonForInfo(responseListener);
        }

        public void ShowTurningInfo()
        {
            infoScreenManager.ClearScreen();
            var text = infoScreenManager.UiStrings.TurnInfoText;
            infoScreenManager.ShowBoxWithText(text);
        }

        public void UpdateTrialProgressBar(int currTrialNum, int totalTrialCount)
        {
            infoScreenManager.ShowProgress(currTrialNum, totalTrialCount);
        }

        #endregion Public Methods
    }
}