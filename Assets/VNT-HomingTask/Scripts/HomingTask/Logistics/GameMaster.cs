using System;
using System.Collections;
using System.Linq;
using unibi.homingtask.menus;
using unibi.homingtask.settings;
using unibi.homingtask.ui;
using unibi.vnt.util;
using UnityEngine;
using UXF;
using Cursor = UnityEngine.Cursor;

namespace unibi.homingtask.logistics
{
    public class GameMaster : MonoBehaviour, ILoggable
    {
        #region Fields

        public static string HomingTaskTag = "[<b><color=#FBCA00>HomingTask</color></b>]";

        public static string MainSceneName = "MainScene";

        [SerializeField]
        private KeyCode pauseKey = KeyCode.Escape;

        private bool playerHasEndedSession = false;

        [SerializeField]
        private UIManager uiManager = null;

        [SerializeField]
        private GameObject UXFscreen = null;

        [SerializeField]
        private GameObject VNTsettingsSelectScreen = null;

        private bool isQuitting;

        #endregion Fields

        #region Properties

        public bool IsPaused { get; private set; }

        public ICustomLogger Logger { get; set; }

        public SessionDefinition VNTsessionDefinition { get; private set; }

        #endregion Properties

        #region Public Methods

        public void DefineSessionStructure(SessionDefinition sessionDef, Session session)
        {
            ParseSessionDefinitionToUXF(sessionDef, session);
            // todo: check if we still need to manage header manually as in WP_A1, if not, we don't
            // need to have this nested call
        }

        public void HandleSettingsSelected(object sender, SessionDefinition sessionDef)
        {
            Logger.Log($"VNT settings '{sessionDef.Name}' have been selected.", LogType.Log, this, HomingTaskTag);
            VNTsessionDefinition = sessionDef;
            Logger.Log("showing UXF UI...", LogType.Log, this, HomingTaskTag);
            VNTsettingsSelectScreen.SetActive(false);
            UXFscreen.SetActive(true);
        }

        public void OnUXFSessionBegin(Session session)
        {
            Logger.Log("Building UXF session object from selected VNT settings...", LogType.Log, this, HomingTaskTag);
            DefineSessionStructure(VNTsessionDefinition, session);
            session.BeginNextTrial();
            ResumeGame();
        }

        public void OnUXFSessionEnd(Session session)
        {
            Logger.Log($"ending UXF session {session.number}", LogType.Log, this, HomingTaskTag);

            if (isQuitting)
            {
                return;
            }

            playerHasEndedSession = false;
            StartCoroutine(WaitForPlayerHasEndedSessionCoroutine());
        }

        public void ParseSessionDefinitionToUXF(SessionDefinition sessionDef, Session session)
        {
            session.settings.SetValue("SessionDefinition", sessionDef);
            foreach (var block in sessionDef.Blocks)
            {
                Block UXFblock = session.CreateBlock(block.TrialSettings.Length);
                UXFblock.settings.SetValue("BlockDefinition", block);
                UXFblock.settings.SetValue("BlockName", block.Name);

                if (block.ShuffleTrials == true)
                {
                    block.TrialSettings = block.TrialSettings.ThreadSafeShuffle().ToArray();
                    Logger.Log($"Trial order shuffled for block '{block.Name}'", LogType.Log, this, HomingTaskTag);
                }
                var trialList = block.TrialSettings;

                for (int i = 0; i < UXFblock.trials.Count; i++)
                {
                    var VNTsettings = trialList[i];
                    var UXFsettings = UXFblock.trials[i].settings;
                    UXFsettings.SetValue("TrialSettings", VNTsettings);
                    // these might be unused in the end
                    UXFsettings.SetValue("TrialName", VNTsettings.Name);
                    UXFsettings.SetValue("DisplacementMode", VNTsettings.Mode);
                    UXFsettings.SetValue("IsTraining", VNTsettings.IsTraining);
                }
            }
        }

        #endregion Public Methods

        #region Private Methods

        private void Awake()
        {
            Application.targetFrameRate = 60;
        }

        private void CheckForPauseKeyPressed()
        {
            // if key was not pressed, return
            if (!Input.GetKeyDown(pauseKey))
            {
                return;
            }

            // if we are already paused and a session is in progress, return to game
            if (IsPaused && Session.instance.InTrial)
            {
                VNTsettingsSelectScreen.SetActive(false);
                ResumeGame();
                return;
            }

            //if we arrive here, pause game
            PauseGame();

            // if can't see session select screen, show it
            if (!VNTsettingsSelectScreen.activeSelf)
            {
                ShowSessionSelector();
            }
        }

        private void HandlePlayerHasEndedSession()
        {
            playerHasEndedSession = true;
        }

        private void HandleSettingsConfirmedDuringSession(object sender, EventArgs e)
        {
            VNTsettingsSelectScreen.SetActive(false);
            ResumeGame();
        }

        private void OnDestroy()
        {
            SessionSelection.OnSettingsSelected -= HandleSettingsSelected;
            Session.instance.onSessionBegin.RemoveListener(OnUXFSessionBegin);
            SessionSelection.OnSettingsConfirmedDuringSession -= HandleSettingsConfirmedDuringSession;
            SessionSelection.OnQuitButtonPressed -= HandleQuitButtonClicked;
        }

        private void PauseGame()
        {
            Time.timeScale = 0f;
            IsPaused = true;
            Cursor.visible = true;
            Cursor.lockState = CursorLockMode.Confined;
            Logger.Log("Game Paused", LogType.Log, this, HomingTaskTag);
        }

        private void ResumeGame()
        {
            Time.timeScale = 1;
            IsPaused = false;
            Logger.Log("Game Resumed", LogType.Log, this, HomingTaskTag);
            Cursor.lockState = CursorLockMode.Locked;
            Cursor.visible = false;
        }

        private void ShowSessionSelector()
        {
            VNTsettingsSelectScreen.SetActive(true);
            UXFscreen.SetActive(false);
        }

        private void Start()
        {
            PauseGame();
            ShowSessionSelector();
            // start listening to session selected event (clicked OK in selection screen)
            SessionSelection.OnSettingsSelected += HandleSettingsSelected;
            // start listening to UXF session begin event (clicked session start in UXF screen)
            Session.instance.onSessionBegin.AddListener(OnUXFSessionBegin);
            // start listening to button clicked during session event in session selection
            SessionSelection.OnSettingsConfirmedDuringSession += HandleSettingsConfirmedDuringSession;
            // start listening to quit button event
            SessionSelection.OnQuitButtonPressed += HandleQuitButtonClicked;
        }

        private void HandleQuitButtonClicked(object sender, EventArgs e)
        {
            isQuitting = true;
            StartCoroutine(ShutDownAndQuitCoroutine());
        }

        private void Update()
        {
            CheckForPauseKeyPressed();
        }

        private IEnumerator WaitForPlayerHasEndedSessionCoroutine()
        {
            uiManager.ShowSessionDoneScreen(HandlePlayerHasEndedSession);
            yield return new WaitUntil(() => playerHasEndedSession == true);
            PauseGame();
            ShowSessionSelector();
            yield break;
        }

        private IEnumerator ShutDownAndQuitCoroutine()
        {
            var session = Session.instance;
            // we only need to prevent data loss if at least 1 trial has been completed
            if (session.hasInitialised && session.CurrentTrial.number > 1)
            {
                Logger.Log("Shutdown request received in running session, " +
                    "waiting for UXF shutdown to complete...", LogType.Warning, this);
                session.End();
                foreach (var tracker in session.trackedObjects)
                {
                    tracker.StopRecording();
                }

                yield return new WaitUntil(() => session.isEnding == false);
                Logger.Log("UXF shutdown complete!", LogType.Log, this);
            }
            //If we are running in a standalone build of the game
#if UNITY_STANDALONE
            //Quit the application
            Application.Quit();
#endif

            //If we are running in the editor
#if UNITY_EDITOR
            //Stop playing the scene
            UnityEditor.EditorApplication.isPlaying = false;
#endif
        }

        #endregion Private Methods
    }
}