﻿using unibi.homingtask.settings;
using UnityEngine;
using UXF;

namespace unibi.homingtask.logistics
{
    [CreateAssetMenu(fileName = "TrialData", menuName = "ScriptableObjects/DataManagement/TrialData", order = 1)]
    public class TrialData : ScriptableObject
    {
        #region Fields

        public float CurrentHomingConfidence;
        public Vector3 CurrentHomingGoalEstimate;
        public float CurrentPointingConfidence;
        public Vector3 CurrentPointingGoalEstimate;
        public Vector3 CurrentPointingPosition;
        public Trial CurrentTrial;
        public TrialSettings CurrentTrialSettings;
        public float StartTime;
        public float StopTime;

        #endregion Fields
    }
}