﻿using UnityEngine;

namespace unibi.homingtask.ui
{
    [CreateAssetMenu(fileName = "UiStrings", menuName = "ScriptableObjects/DataHandling/UIStrings")]
    public class UIStrings : ScriptableObject
    {
        #region Fields

        public string ApproachInfoText = "Please walk onto the goal location.";
        public string DoneBtnTxt = "Done!";
        public string ExploreBtnTxt = "End Exploration!";

        public string ExploreInfoTxt = "Walk around and memorise the location of the goal marker." +
            " You will have to return to this location later," +
            " but the marker will not be visible." +
            "\n Press OK to continue!";

        public string FeedbackInfoTxt = "To see how well you did, check the red and green arrows:" +
            " the red one shows where you started the new fire, " +
            "the green one the old location of the fireplace.";

        public string GuidedReturnInfoText = "Text Goes Here";
        public string HomeBtnTxt = "I'm Home!";
        public string HomeReachableInfoTxt = "When you think you have reached the goal, press the button to finish your return.";
        public string HomingContinueInfoTxt = "Please contine your return to the goal.";

        public string HomingStartInfoTxt = "Please return to the goal location." +
            "\n Click the button to start your return.";

        public string MockTrialConfidenceRequestText = "some text";
        public string OKBtnTxt = "OK";
        public string PointBtnTxt = "Pointing Done!";

        public string PointingConfTxt = "Do you think you placed the marker exactly on the goal?" +
            " Please indicate your confidence below and then click the button to continue.";

        public string PointingInfoTxt = "Please place a marker at the goal location.";
        public string PointingTimeoutTxt = "Pointing timed out! Please be quicker next time.";
        public string ScoreTxt = "Your score for this trial was: ";

        public string SessionDoneTxt = "Congratulations! You  successfully finished the current session. " +
            "Press the button to return to the main menu.";

        public string TeleportInfoTxt = "You will now be teleported to a new position." +
            " \n Press OK to continue!";

        public string TimeoutTxt = "Time left: ";
        public string ToggleInfoText = "Press '*' to show/hide mouse cursor.";

        public string TrialConfTxt = "Do you think you managed to return to the goal location exactly? " +
            "Please indicate your confidence below and then click the button to continue.";

        public string TrialTimeoutTxt = "Trial timed out! Please be quicker next time.";

        public string TurnInfoText = "Turn in the direction indicated by the arrow to continue.";

        public string WaypointInfoTxt = "You will now be guided to a new position." +
                                            " Follow the waypoints until you reach it. " +
            "\n Press OK to continue!";

        #endregion Fields
    }
}