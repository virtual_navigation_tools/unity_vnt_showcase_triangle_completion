using System;
using System.Collections.Generic;
using unibi.homingtask.settings;
using unibi.homingtask.trialflow;
using UnityEngine;
using UXF;

namespace unibi.homingtask.uxfcustom
{
    public class PlayerTracker : Tracker
    {
        #region Fields

        [SerializeField] private TrialManager trialManager;

        #endregion Fields

        #region Properties

        public override IEnumerable<string> CustomHeader => new string[]
        {
            "session_num",
            "block_num",
            "trial_num",
            "trial_name",
            "trial_state",
            "pos_x",
            "pos_y",
            "pos_z",
            "eul_x",
            "eul_y",
            "eul_z",
            "quat_x",
            "quat_y",
            "quat_z",
            "quat_w",
            "timestamp",
            "framecount"
        };

        public override string MeasurementDescriptor => "player_movement";

        #endregion Properties

        #region Protected Methods

        /// <summary>
        /// Returns current position and rotation values
        /// </summary>
        /// <returns> </returns>
        protected override UXFDataRow GetCurrentValues()
        {
            // get position and rotation
            Vector3 pos = gameObject.transform.position;
            Vector3 eul = gameObject.transform.eulerAngles;
            Quaternion quat = gameObject.transform.rotation;

            // get trial settings
            var trialSettings = (TrialSettings)Session.instance.CurrentTrial.settings.GetObject("TrialSettings");

            string format = "0.######";

            // return data as string array
            var values = new UXFDataRow()
            {
                ("session_num", Session.instance.number),
                ("block_num", Session.instance.currentBlockNum),
                ("trial_num", Session.instance.CurrentTrial.number.ToString()),
                ("trial_name", trialSettings.Name),
                ("trial_state", trialManager.TrialStateMachine.CurrentState.Name),
                ("pos_x", pos.x.ToString(format)),
                ("pos_y", pos.y.ToString(format)),
                ("pos_z", pos.z.ToString(format)),
                ("eul_x", eul.x.ToString(format)),
                ("eul_y", eul.y.ToString(format)),
                ("eul_z", eul.z.ToString(format)),
                ("quat_x", quat.x.ToString(format)),
                ("quat_y", quat.y.ToString(format)),
                ("quat_z", quat.z.ToString(format)),
                ("quat_w", quat.w.ToString(format)),
                ("timestamp", DateTime.Now.ToUniversalTime().ToString("dd/MM/yyyy HH:mm:ss.fff")),
                ("framecount", Time.frameCount.ToString())
            };

            return values;
        }

        #endregion Protected Methods
    }
}