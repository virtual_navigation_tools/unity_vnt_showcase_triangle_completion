using unibi.vnt.util;
using UnityEngine;

public class Footsteps : MonoBehaviour
{
    #region Fields

    [SerializeField] private AudioSource footstepsSound;

    [SerializeField] private Mode mode = Mode.DistanceCheck;

    private Vector3 oldPos;

    [SerializeField] private bool playSound = true;

    [SerializeField, Range(0.001f, 1)] private float threshVal = 0.001f;

    #endregion Fields

    #region Enums

    public enum Mode
    {
        InputAxis, DistanceCheck
    }

    #endregion Enums

    #region Private Methods

    private bool hasMoved()
    {
        if (mode == Mode.DistanceCheck)
        {
            return !FastDistanceComparison.IsWithinRadius(oldPos, transform.position, threshVal);
        }
        else if (mode == Mode.InputAxis)
        {
            return Input.GetAxis("Vertical") >= threshVal;
        }
        return false;
    }

    private void Start()
    {
        oldPos = transform.position;
    }

    private void Update()
    {
        if (!playSound)
        {
            footstepsSound.enabled = false;
            return;
        }

        if (hasMoved())
        {
            footstepsSound.enabled = true;
        }
        else
        {
            footstepsSound.enabled = false;
        }

        oldPos = transform.position;
    }

    #endregion Private Methods
}