﻿using System;
using System.Collections;
using unibi.homingtask.logistics;
using unibi.homingtask.ui;
using unibi.vnt.trialflow;
using unibi.vnt.util;
using UnityEngine;

namespace unibi.homingtask.trialflow
{
    public class TimeoutHandler : TrialStateHandler, ILoggable
    {
        #region Fields

        [SerializeField] private PlayerManager playerManager;
        private vnt.trialflow.TrialStateMachine stateMachine;
        [SerializeField] private UIManager uiManager;
        [SerializeField] private WaitManager waitManager;

        #endregion Fields

        #region Properties

        public override ITrialState HandledState => TrialStateMachine.Timeout;
        public ICustomLogger Logger { get; set; }

        #endregion Properties

        #region Protected Methods

        protected override void HandleEnter(object sender, vnt.trialflow.TrialStateMachine stateMachine)
        {
            if (sender != HandledState)
            {
                return;
            }
            Logger.Log("entering State 'Timeout' ...", LogType.Log, this, GameMaster.HomingTaskTag);

            // save state machine ref
            this.stateMachine = stateMachine;

            StartCoroutine(RunStateLogicCoroutine());
        }

        protected override void HandleExit(object sender, EventArgs e)
        {
            if (sender != HandledState)
            {
                return;
            }
            Logger.Log("leaving State 'Timeout' ...", LogType.Log, this, GameMaster.HomingTaskTag);
        }

        protected override IEnumerator RunStateLogicCoroutine()
        {
            Logger.Log("starting coroutine in state 'Timeout' ...", LogType.Log, this, GameMaster.HomingTaskTag);
            playerManager.FreezePlayer();
            playerManager.PlayerHasAcknowledged = false;
            uiManager.ShowTrialTimeOutInfo(HandleInfoAcknowledged);
            yield return StartCoroutine(waitManager.WaitForUserOK());
            playerManager.UnFreezePlayer();
            SetNextTrialState(stateMachine, TrialStateMachine.TrialEnd);
            yield break;
        }

        #endregion Protected Methods

        #region Private Methods

        private void HandleInfoAcknowledged()
        {
            playerManager.PlayerHasAcknowledged = true;
        }

        #endregion Private Methods
    }
}