using System;
using System.Collections;
using unibi.homingtask.logistics;
using unibi.homingtask.settings;
using unibi.homingtask.ui;
using unibi.vnt.guidance;
using unibi.vnt.trialflow;
using unibi.vnt.util;
using UnityEngine;
using UXF;

namespace unibi.homingtask.trialflow
{
    public class HomingHandler : TrialStateHandler, ILoggable
    {
        #region Fields

        private TrialSettings currentTrialSettings;
        [SerializeField] private PlayerManager playerManager;
        [SerializeField] private Tracker playerTracker;
        [SerializeField] private DistanceChecker pointingDistanceChecker;
        private vnt.trialflow.TrialStateMachine stateMachine;
        [SerializeField] private TrialDataManager trialDataManager;
        [SerializeField] private TrialManager trialManager = null;
        [SerializeField] private UIManager uiManager;
        [SerializeField] private WaitManager waitManager;

        #endregion Fields

        #region Properties

        public override ITrialState HandledState => TrialStateMachine.Homing;
        public ICustomLogger Logger { get; set; }

        #endregion Properties

        #region Protected Methods

        protected override void HandleEnter(object sender, vnt.trialflow.TrialStateMachine stateMachine)
        {
            if (sender != HandledState)
            {
                return;
            }
            Logger.Log("entering State 'Homing' ...", LogType.Log, this, GameMaster.HomingTaskTag);

            DistanceChecker.OnThresholdDistanceReached += HandlePointingDistanceReached;

            // get settings object
            currentTrialSettings = (TrialSettings)Session.instance.CurrentTrial.settings.GetObject("TrialSettings");
            trialDataManager.TrialData.CurrentTrialSettings = currentTrialSettings;

            // save state machine ref
            this.stateMachine = stateMachine;

            StartCoroutine(RunStateLogicCoroutine());
        }

        protected override void HandleExit(object sender, EventArgs e)
        {
            if (sender != HandledState)
            {
                return;
            }
            Logger.Log("leaving State 'Homing' ...", LogType.Log, this, GameMaster.HomingTaskTag);

            DistanceChecker.OnThresholdDistanceReached -= HandlePointingDistanceReached;

            if (playerManager.PlayerHasEndedHoming || waitManager.HomingTimerHasReachedTimeout())
            {
                waitManager.StopHomingTimer();
            }
            else
            {
                waitManager.PauseHomingTimer();
            }
            playerTracker.PauseRecording();

            if (pointingDistanceChecker.IsActive)
            {
                pointingDistanceChecker.Disable();
            }
        }

        protected override IEnumerator RunStateLogicCoroutine()
        {
            Logger.Log("starting coroutine in state 'Homing' ...", LogType.Log, this, GameMaster.HomingTaskTag);

            playerManager.PlayerHasReachedPointingDistance = false;
            if (TrialHasPointing() && !IsLastPointingTrial())
            {
                // since we advance the counter here, we must not also advance it at the end of pointing!
                trialManager.AdvancePointingCounter();
                StartPointingDistanceCheck();
            }

            #region show info

            playerManager.FreezePlayer();

            if (IsFirstHomingBout())
            {
                playerManager.TeleportPlayerToPosition(
                    currentTrialSettings.HomingStartPosition,
                    isDisplacement: false,
                    useAnimation: false);
            }

            playerManager.PlayerHasAcknowledged = false;
            uiManager.ShowHomingStartInfo(
                IsFirstHomingBout(),
                IsLastHomingBout(),
                HandleInfoAcknowledged);

            yield return StartCoroutine(waitManager.WaitForUserOK());
            playerManager.UnFreezePlayer();

            playerTracker.ResumeRecording();

            #endregion show info

            #region manage timer

            if (IsFirstPointingTrial() || !TrialHasPointing())
            {
                waitManager.StartHomingTimer(currentTrialSettings.HomingTimeout);
            }
            else
            {
                waitManager.ResumeHomingTimer();
            }

            #endregion manage timer

            playerManager.PlayerHasEndedHoming = false;
            uiManager.SetScreenForHoming(waitManager.HomingTimer, HandlePlayerEndedHoming);

            // wait until either pointing is reached, timer ran out or goal estimate was given
            yield return StartCoroutine(waitManager.WaitForPointingReachedOrHomingAborted());

            #region handle state transitions

            if (waitManager.HomingTimerHasReachedTimeout())
            {
                SetNextTrialState(stateMachine, TrialStateMachine.Timeout);
                yield break;
            }

            if (playerManager.PlayerHasReachedPointingDistance)
            {
                SetNextTrialState(stateMachine, TrialStateMachine.Pointing);
                yield break;
            }

            if (playerManager.PlayerHasEndedHoming)
            {
                Debug.Log("check: homing end processing..");
                playerTracker.PauseRecording();
                playerManager.FreezePlayer();
                playerManager.PlayerHasGivenHomingConfidence = false;
                uiManager.ShowHomingTrialConfidenceRequest(HandlePlayerHasGivenHomingConfidence);
                yield return StartCoroutine(waitManager.WaitForUserSetHomingConfidence());
                trialDataManager.SaveTrialResult(Session.instance.CurrentTrial);
                Debug.Log("check: trial result saved..");
                playerManager.UnFreezePlayer();
                SetNextTrialState(stateMachine, TrialStateMachine.TrialEnd);
                yield break;
            }

            #endregion handle state transitions
        }

        #endregion Protected Methods

        #region Private Methods

        private void HandleInfoAcknowledged()
        {
            playerManager.PlayerHasAcknowledged = true;
        }

        private void HandlePlayerEndedHoming()
        {
            Debug.Log("check: homing end registered");
            playerManager.PlayerHasEndedHoming = true;
            trialDataManager.TrialData.CurrentHomingGoalEstimate = playerManager.Player.transform.position;
        }

        private void HandlePlayerHasGivenHomingConfidence()
        {
            playerManager.PlayerHasGivenHomingConfidence = true;
            trialDataManager.TrialData.CurrentHomingConfidence = uiManager.GetCurrentConfidenceSliderValue();
        }

        private void HandlePointingDistanceReached(object sender, Vector3 pos)
        {
            if (!(DistanceChecker)sender == pointingDistanceChecker)
            {
                return;
            }
            playerManager.PlayerHasReachedPointingDistance = true;
            pointingDistanceChecker.Disable();
        }

        private bool IsFirstHomingBout()
        {
            return IsFirstPointingTrial() || !TrialHasPointing();
        }

        private bool IsFirstPointingTrial()
        {
            return trialManager.CurrentPointingRep == 1;
        }

        private bool IsLastHomingBout()
        {
            return IsLastPointingTrial() || !TrialHasPointing();
        }

        private bool IsLastPointingTrial()
        {
            return trialManager.CurrentPointingRep == currentTrialSettings.PointingDistances.Count;
        }

        private void StartPointingDistanceCheck()
        {
            pointingDistanceChecker.Initialise(
                playerManager.Player.transform,
                currentTrialSettings.HomingStartPosition, // toggle pointing center here
                currentTrialSettings.PointingDistances[trialManager.CurrentPointingRep - 1],
                DistanceChecker.Mode.Greater); // toggle here
            pointingDistanceChecker.Enable();
        }

        private bool TrialHasPointing()
        {
            return currentTrialSettings.PointingDistances.Count > 0;
        }

        #endregion Private Methods
    }
}