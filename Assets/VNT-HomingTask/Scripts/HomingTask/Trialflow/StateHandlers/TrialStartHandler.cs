﻿using System;
using System.Collections;
using System.Linq;
using unibi.homingtask.levelgen;
using unibi.homingtask.logistics;
using unibi.homingtask.settings;
using unibi.homingtask.ui;
using unibi.vnt.guidance;
using unibi.vnt.trialflow;
using unibi.vnt.util;
using UnityEngine;
using UXF;

namespace unibi.homingtask.trialflow
{
    public class TrialStartHandler : TrialStateHandler, ILoggable
    {
        #region Fields

        private const float defaultRotationVel = 60f;
        private TrialSettings currentTrialSettings = null;
        private GameObject goalMarker = null;
        private IWaypoint goalWaypoint = null;
        [SerializeField] private LevelManager levelManager;
        [SerializeField] private PlayerManager playerManager;
        [SerializeField] private Tracker playerTracker;
        [SerializeField] private Tracker pointerTracker;
        private vnt.trialflow.TrialStateMachine stateMachine = null;
        [SerializeField] private UIManager uiManager;
        [SerializeField] private WaitManager waitManager;

        #endregion Fields

        #region Properties

        public override ITrialState HandledState => TrialStateMachine.TrialStart;
        public float LastStartTime { get; private set; }
        public ICustomLogger Logger { get; set; }

        #endregion Properties

        #region Protected Methods

        protected override void HandleEnter(object sender, vnt.trialflow.TrialStateMachine stateMachine)
        {
            if (sender != HandledState)
            {
                return;
            }
            Logger.Log("entering State 'TrialStart' ...", LogType.Log, this, GameMaster.HomingTaskTag);
            // save state machine ref
            this.stateMachine = stateMachine;

            // get settings object
            var settings = (TrialSettings)Session.instance.CurrentTrial.settings.GetObject("TrialSettings");
            // save it on class level for access in coroutine
            currentTrialSettings = settings;
            // set scene for trial
            levelManager.SetScene(settings);

            // set walk speed for trial
            playerManager.SetMaxPlayerTranslationVelocity(settings.ForwardVelInMPS);
            playerManager.SetMaxPlayerRotationVelocity(defaultRotationVel);

            // start player tracker
            playerTracker.StartRecording();

            // stop pointing tracker since UXF has started it on trial start
            pointerTracker.StopRecording();

            // start main logic coroutine
            StartCoroutine(RunStateLogicCoroutine());
        }

        protected override void HandleExit(object sender, EventArgs e)
        {
            if (sender != HandledState)
            {
                return;
            }
            // pause player tracker
            playerTracker.PauseRecording();
            WaypointWithTriggerRadius.OnReached -= HandleApproachComplete;
            Logger.Log("leaving State 'TrialStart' ...", LogType.Log, this, GameMaster.HomingTaskTag);
        }

        protected override IEnumerator RunStateLogicCoroutine()
        {
            Logger.Log("starting coroutine in state 'TrialStart' ...", LogType.Log, this, GameMaster.HomingTaskTag);
            //update trial progress ui
            uiManager.UpdateTrialProgressBar(Session.instance.currentTrialNum, Session.instance.Trials.Count());
            // wait for world setup to finish
            playerManager.FreezePlayer();
            yield return StartCoroutine(waitManager.WaitForSetupDone());
            // make goal visible
            levelManager.SetGoalVisible(true);

            #region player placement

            // handle player placement at trial start
            uiManager.SetScreenForTeleport();
            Vector3 returnStartDir = GetGoalPosToHomingStartDirection();
            float offsetInMeters = 10;
            Vector3 setupPosition = currentTrialSettings.GoalPosition - (returnStartDir * offsetInMeters);
            playerManager.TeleportPlayerToPosition(
                setupPosition, useAnimation: false);

            #endregion player placement

            playerManager.PlayerHasApproachedStart = false;
            playerManager.FreezePlayer(canRotate: true);
            uiManager.ShowTurningInfo();
            Vector3 goalDir = currentTrialSettings.GoalPosition - setupPosition;
            playerManager.TurnPlayerTowards(goalDir);
            yield return StartCoroutine(waitManager.WaitForTurnComplete());

            // instruct player to walk to start
            uiManager.SetScreenforHomeApproach();
            uiManager.ShowGoalApproachInfo();
            // unfreeze player
            playerManager.UnFreezePlayer();
            // wait for player to reach start
            CreateWaypointOnGoal();
            yield return StartCoroutine(waitManager.WaitForApproachComplete());
            // freeze player again
            playerManager.FreezePlayer();
            // note trial start time
            LastStartTime = Time.time;
            // transition to next state
            if (currentTrialSettings.IsTraining)
            {
                SetNextTrialState(stateMachine, TrialStateMachine.Exploration);
                yield break;
            }
            else
            {
                SetNextTrialState(stateMachine, TrialStateMachine.Displacement);
                yield break;
            }
        }

        #endregion Protected Methods

        #region Private Methods

        private void CreateWaypointOnGoal()
        {
            goalMarker = new GameObject("goalWaypointHolder");
            goalMarker.transform.position = currentTrialSettings.GoalPosition;
            WaypointWithTriggerRadius waypoint = goalMarker.GetComponent<WaypointWithTriggerRadius>();
            if (!waypoint)
            {
                waypoint = goalMarker.AddComponent<WaypointWithTriggerRadius>();
            }
            waypoint.Initialise(playerManager.Player, "goal", 2);
            goalWaypoint = waypoint;
            goalWaypoint.Enable();
            WaypointWithTriggerRadius.OnReached += HandleApproachComplete;
        }

        private Vector3 GetGoalPosToHomingStartDirection()
        {
            var dir = currentTrialSettings.HomingStartPosition - currentTrialSettings.GoalPosition;
            return dir.normalized;
        }

        private void HandleApproachComplete(object sender, Vector3 position)
        {
            IWaypoint waypoint = (IWaypoint)sender;
            if (waypoint == goalWaypoint)
            {
                playerManager.PlayerHasApproachedStart = true;
                goalWaypoint.Delete();
            }
        }

        #endregion Private Methods
    }
}