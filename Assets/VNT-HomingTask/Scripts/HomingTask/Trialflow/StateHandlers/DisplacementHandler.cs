﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using unibi.homingtask.levelgen;
using unibi.homingtask.logistics;
using unibi.homingtask.settings;
using unibi.homingtask.ui;
using unibi.vnt.guidance;
using unibi.vnt.trialflow;
using unibi.vnt.util;
using UnityEngine;
using UXF;

namespace unibi.homingtask.trialflow
{
    public class DisplacementHandler : TrialStateHandler, ILoggable
    {
        #region Fields

        public static EventHandler<bool> TeleportComplete;
        private TrialSettings currentTrialSettings;
        [SerializeField] private GuidanceManager guidanceManager;
        [SerializeField] private LevelManager levelManager;
        [SerializeField] private PlayerManager playerManager;
        [SerializeField] private Tracker playerTracker;
        private vnt.trialflow.TrialStateMachine stateMachine;
        [SerializeField] private UIManager uiManager;
        [SerializeField] private WaitManager waitManager;

        #endregion Fields

        #region Properties

        public override ITrialState HandledState => TrialStateMachine.Displacement;

        public ICustomLogger Logger { get; set; }

        #endregion Properties

        #region Protected Methods

        protected override void HandleEnter(object sender, vnt.trialflow.TrialStateMachine stateMachine)
        {
            if (sender != HandledState)
            {
                return;
            }
            Logger.Log("entering State 'Displacement' ...", LogType.Log, this, GameMaster.HomingTaskTag);

            // subscribe to guidance event
            GuidanceController.OnLastWaypointReached += HandleLastWaypointreached;

            // save state machine ref
            this.stateMachine = stateMachine;

            // get settings object
            currentTrialSettings = (TrialSettings)Session.instance.CurrentTrial.settings.GetObject("TrialSettings");

            if (currentTrialSettings.ShowLandmarksDuringDisplacement == false)
            {
                levelManager.RemoveLandmarks();
            }

            // unpause player tracker
            playerTracker.ResumeRecording();

            // start main logic coroutine
            StartCoroutine(RunStateLogicCoroutine());
        }

        protected override void HandleExit(object sender, EventArgs e)
        {
            if (sender != HandledState)
            {
                return;
            }
            Logger.Log("leaving State 'Displacement' ...", LogType.Log, this, GameMaster.HomingTaskTag);
            // unsubsubscribe from guidance event
            GuidanceController.OnLastWaypointReached -= HandleLastWaypointreached;
            // reset guidance for cleanup
            guidanceManager.StopGuidance();
            guidanceManager.ResetController();

            // pause tracker
            playerTracker.PauseRecording();
        }

        protected override IEnumerator RunStateLogicCoroutine()
        {
            Logger.Log("starting coroutine in state 'Displacement' ...", LogType.Log, this, GameMaster.HomingTaskTag);

            #region prep work

            playerManager.FreezePlayer();
            uiManager.SetScreenForTeleport();
            playerManager.TeleportPlayerToPosition(currentTrialSettings.GoalPosition, useAnimation: false);

            playerManager.PlayerHasAcknowledged = false;
            uiManager.ShowDisplacementInfo(currentTrialSettings.Mode, HandleInfoAcknowledged);
            yield return base.StartCoroutine(waitManager.WaitForUserOK());

            levelManager.SetGoalVisible(false);

            playerManager.UnFreezePlayer();
            uiManager.SetScreenForDisplacement(waitManager.DisplacementTimer);

            #endregion prep work

            #region displace by walking or teleport

            if (currentTrialSettings.Mode is TrialSettings.DisplacementMode.Walking)
            {
                List<Vector3> waypointPositions = UpdateWaypointsWithHomingStartIfNecessary(currentTrialSettings.WaypointPositions);
                guidanceManager.Initialise(waypointPositions, playerManager.Player);
                guidanceManager.StartGuidance();
            }
            else
            {
                playerManager.FreezePlayer();
                playerManager.TeleportPlayerToPosition(currentTrialSettings.HomingStartPosition, isDisplacement: true, useAnimation: false);
                uiManager.SetScreenForTeleport();
            }

            playerManager.PlayerHasFinishedDisplacement = false;
            yield return StartCoroutine(waitManager.WaitForDisplacementCompleteOrTimeout(currentTrialSettings.DisplacementTimeout));
            playerManager.UnFreezePlayer();

            #endregion displace by walking or teleport

            #region decide which state to go to

            if (playerManager.PlayerHasFinishedDisplacement)
            {
                if (currentTrialSettings.IsGuidedTrial)
                {
                    SetNextTrialState(stateMachine, TrialStateMachine.GuidedReturn);
                    yield break;
                }
                else
                {
                    SetNextTrialState(stateMachine, TrialStateMachine.Homing);
                    yield break;
                }
            }
            else
            {
                SetNextTrialState(stateMachine, TrialStateMachine.Timeout);
                yield break;
            }

            #endregion decide which state to go to
        }

        #endregion Protected Methods

        #region Private Methods

        private void HandleInfoAcknowledged()
        {
            playerManager.PlayerHasAcknowledged = true;
        }

        private void HandleLastWaypointreached(object sender, IWaypoint waypoint)
        {
            playerManager.PlayerHasFinishedDisplacement = true;
        }

        private List<Vector3> UpdateWaypointsWithHomingStartIfNecessary(List<Vector3> waypointPositions)
        {
            if (!(waypointPositions.Last() == currentTrialSettings.HomingStartPosition))
            {
                waypointPositions.Add(currentTrialSettings.HomingStartPosition);
            }
            return waypointPositions;
        }

        #endregion Private Methods
    }
}