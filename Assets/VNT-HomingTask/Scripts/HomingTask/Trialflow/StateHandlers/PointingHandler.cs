﻿using System;
using System.Collections;
using unibi.homingtask.levelgen;
using unibi.homingtask.logistics;
using unibi.homingtask.settings;
using unibi.homingtask.ui;
using unibi.vnt.spatial;
using unibi.vnt.trialflow;
using unibi.vnt.util;
using UnityEngine;
using UXF;

namespace unibi.homingtask.trialflow
{
    public class PointingHandler : TrialStateHandler, ILoggable
    {
        #region Fields

        private TrialSettings currentTrialSettings;
        [SerializeField] private LevelManager levelManager;
        [SerializeField] private PlayerManager playerManager;
        [SerializeField] private Tracker pointerTracker;
        private vnt.trialflow.TrialStateMachine stateMachine;
        [SerializeField] private SurfaceBasedPointingManager surfaceBasedPointingManager;
        [SerializeField] private TrialDataManager trialDataManager;
        [SerializeField] private TrialManager trialManager = null;
        [SerializeField] private UIManager uiManager;
        [SerializeField] private WaitManager waitManager;

        #endregion Fields

        #region Properties

        public override ITrialState HandledState => TrialStateMachine.Pointing;
        public ICustomLogger Logger { get; set; }

        #endregion Properties

        #region Protected Methods

        protected override void HandleEnter(object sender, vnt.trialflow.TrialStateMachine stateMachine)
        {
            if (sender != HandledState)
            {
                return;
            }
            Logger.Log("entering State 'Pointing' ...", LogType.Log, this, GameMaster.HomingTaskTag);

            playerManager.PlayerHasAcknowledged = false;
            playerManager.PlayerHasPointed = false;

            // get settings object
            currentTrialSettings = (TrialSettings)Session.instance.CurrentTrial.settings.GetObject("TrialSettings");
            trialDataManager.TrialData.CurrentTrialSettings = currentTrialSettings;

            // save state machine ref
            this.stateMachine = stateMachine;

            if (pointerTracker.CurrentState == TrackerState.Paused)
            {
                pointerTracker.ResumeRecording();
            }
            else
            {
                pointerTracker.StartRecording();
            }

            StartCoroutine(RunStateLogicCoroutine());
        }

        protected override void HandleExit(object sender, EventArgs e)
        {
            if (sender != HandledState)
            {
                return;
            }
            Logger.Log("leaving State 'Pointing' ...", LogType.Log, this, GameMaster.HomingTaskTag);
            pointerTracker.PauseRecording();
            surfaceBasedPointingManager.ClearAllTargetables();
        }

        protected override IEnumerator RunStateLogicCoroutine()
        {
            Logger.Log("starting coroutine in state 'Pointing' ...", LogType.Log, this, GameMaster.HomingTaskTag);

            playerManager.FreezePlayer(canRotate: true);
            playerManager.PlayerHasAcknowledged = false;
            uiManager.ShowPointingInfo(HandleInfoAcknowledged);
            yield return StartCoroutine(waitManager.WaitForUserOK());
            uiManager.SetScreenForPointing(HandlePlayerHasPointed, waitManager.PointingTimer);
            surfaceBasedPointingManager.AddObjectsToListOfTargetables(levelManager.GetCurrentTiles());

            surfaceBasedPointingManager.EnablePointing();
            pointerTracker.ResumeRecording();

            yield return StartCoroutine(waitManager.WaitForPointingDoneOrTimeout(currentTrialSettings.PointingTimeout));

            surfaceBasedPointingManager.DisablePointing();
            pointerTracker.PauseRecording();

            if (waitManager.PointingTimer.HasReachedTimeout)
            {
                uiManager.ShowPointingTimeoutInfo(HandleInfoAcknowledged);
                playerManager.PlayerHasAcknowledged = false;
                yield return StartCoroutine(waitManager.WaitForUserOK());
            }
            else if (playerManager.PlayerHasPointed)
            {
                playerManager.PlayerHasGivenPointingConfidence = false;
                uiManager.ShowPointingTrialConfidenceRequest(HandlePlayerHasGivenPointingConfidence);
                yield return StartCoroutine(waitManager.WaitForUserSetPointingConfidence());
                trialDataManager.SavePointingResult(Session.instance.CurrentTrial, trialManager.CurrentPointingRep);
            }
            playerManager.UnFreezePlayer();
            SetNextTrialState(stateMachine, TrialStateMachine.Homing);
            yield break;
        }

        #endregion Protected Methods

        #region Private Methods

        private void HandleInfoAcknowledged()
        {
            playerManager.PlayerHasAcknowledged = true;
        }

        private void HandlePlayerHasGivenPointingConfidence()
        {
            playerManager.PlayerHasGivenPointingConfidence = true;
            trialDataManager.TrialData.CurrentPointingConfidence = uiManager.GetCurrentConfidenceSliderValue();
        }

        private void HandlePlayerHasPointed()
        {
            var pointingPlayerPos = playerManager.Player.transform.position;
            var pointingEstimate = surfaceBasedPointingManager.GetPointerLocation();
            trialDataManager.TrialData.CurrentPointingGoalEstimate = pointingEstimate;
            trialDataManager.TrialData.CurrentPointingPosition = pointingPlayerPos;
            playerManager.PlayerHasPointed = true;
        }

        #endregion Private Methods
    }
}