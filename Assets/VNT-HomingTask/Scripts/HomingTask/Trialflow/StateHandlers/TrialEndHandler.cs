﻿using System;
using System.Collections;
using unibi.homingtask.logistics;
using unibi.homingtask.settings;
using unibi.homingtask.ui;
using unibi.vnt.trialflow;
using unibi.vnt.util;
using UnityEngine;
using UXF;

namespace unibi.homingtask.trialflow
{
    public class TrialEndHandler : TrialStateHandler, ILoggable
    {
        #region Fields

        private TrialSettings currentTrialSettings;
        [SerializeField] private FeedbackManager feedbackManager;
        [SerializeField] private PlayerManager playerManager;
        [SerializeField] private Tracker playerTracker;
        [SerializeField] private Tracker pointerTracker;
        private vnt.trialflow.TrialStateMachine stateMachine;
        [SerializeField] private TrialDataManager trialDataManager;
        [SerializeField] private UIManager uiManager;
        [SerializeField] private WaitManager waitManager;

        #endregion Fields

        #region Properties

        public override ITrialState HandledState => TrialStateMachine.TrialEnd;
        public ICustomLogger Logger { get; set; }

        #endregion Properties

        #region Protected Methods

        protected override void HandleEnter(object sender, vnt.trialflow.TrialStateMachine stateMachine)
        {
            if (sender != HandledState)
            {
                return;
            }
            Logger.Log("entering State 'TrialEnd' ...", LogType.Log, this, GameMaster.HomingTaskTag);

            // save state machine ref
            this.stateMachine = stateMachine;

            // get settings object
            currentTrialSettings = (TrialSettings)Session.instance.CurrentTrial.settings.GetObject("TrialSettings");

            StartCoroutine(RunStateLogicCoroutine());
        }

        protected override void HandleExit(object sender, EventArgs e)
        {
            if (sender != HandledState)
            {
                return;
            }
            Logger.Log("leaving State 'TrialEnd' ...", LogType.Log, this, GameMaster.HomingTaskTag);

            // these should be redundant because UXF stops trackers at trial end, but we do it just
            // to be save
            playerTracker.StopRecording();
            pointerTracker.StopRecording();
        }

        protected override IEnumerator RunStateLogicCoroutine()
        {
            Logger.Log("starting coroutine in state 'TrialEnd' ...", LogType.Log, this, GameMaster.HomingTaskTag);
            uiManager.SetScreenForTrialEnd();
            if (!waitManager.HomingTimerHasReachedTimeout())
            {
                if (currentTrialSettings.ShowError == true)
                {
                    feedbackManager.ShowHomingError(trialDataManager.TrialData, currentTrialSettings.ShowFakeGoalPosition);
                    playerManager.PlayerHasAcknowledged = false;
                    uiManager.ShowFeedbackInfo(HandleInfoAcknowledged);
                    yield return StartCoroutine(waitManager.WaitForUserOK());
                }
                if (currentTrialSettings.ShowScore == true)
                {
                    var score = feedbackManager.GetScoreForCurrentTrial(trialDataManager.TrialData, currentTrialSettings.ShowFakeGoalPosition);
                    uiManager.ShowScoreInfo(score, HandleInfoAcknowledged);
                    yield return StartCoroutine(waitManager.WaitForUserOK());
                }
                feedbackManager.HideFeebackObjects();
            }
            Session.instance.CurrentTrial.End();
            SetNextTrialState(stateMachine, TrialStateMachine.Empty);
            yield break;
        }

        #endregion Protected Methods

        #region Private Methods

        private void HandleInfoAcknowledged()
        {
            playerManager.PlayerHasAcknowledged = true;
        }

        #endregion Private Methods
    }
}