﻿using System;
using System.Collections;
using unibi.homingtask.logistics;
using unibi.homingtask.settings;
using unibi.homingtask.ui;
using unibi.vnt.trialflow;
using unibi.vnt.util;
using UnityEngine;
using UXF;

namespace unibi.homingtask.trialflow
{
    public class ExplorationHandler : TrialStateHandler, ILoggable
    {
        #region Fields

        private TrialSettings currentTrialSettings = null;
        [SerializeField] private PlayerManager playerManager;
        [SerializeField] private Tracker playerTracker;
        private vnt.trialflow.TrialStateMachine stateMachine = null;
        [SerializeField] private UIManager uiManager;
        [SerializeField] private WaitManager waitManager;

        #endregion Fields

        #region Properties

        public override ITrialState HandledState => TrialStateMachine.Exploration;

        public ICustomLogger Logger { get; set; }

        #endregion Properties

        #region Protected Methods

        protected override void HandleEnter(object sender, vnt.trialflow.TrialStateMachine stateMachine)
        {
            if (sender != HandledState)
            {
                return;
            }
            Logger.Log("entering State 'Exploration' ...", LogType.Log, this, GameMaster.HomingTaskTag);
            // save state machine ref
            this.stateMachine = stateMachine;
            // get settings object
            currentTrialSettings = (TrialSettings)Session.instance.CurrentTrial.settings.GetObject("TrialSettings");
            // unpause player tracker
            playerTracker.ResumeRecording();
            // start main logic coroutine
            StartCoroutine(RunStateLogicCoroutine());
        }

        protected override void HandleExit(object sender, EventArgs e)
        {
            if (sender != HandledState)
            {
                return;
            }
            Logger.Log("leaving State 'Exploration' ...", LogType.Log, this, GameMaster.HomingTaskTag);
            // pause player tracker
            playerTracker.PauseRecording();
        }

        protected override IEnumerator RunStateLogicCoroutine()
        {
            Logger.Log("starting coroutine in state 'Exploration' ...", LogType.Log, this, GameMaster.HomingTaskTag);
            playerManager.PlayerHasAcknowledged = false;
            uiManager.ShowExplorationInfo(HandleInfoAcknowledged);
            yield return StartCoroutine(waitManager.WaitForUserOK());
            playerManager.UnFreezePlayer();
            playerManager.PlayerHasQuitExploration = false;
            uiManager.SetScreenForExploration(HandlePlayerEndedExploration, waitManager.ExplorationTimer);
            yield return StartCoroutine(waitManager.WaitForExplorationDoneOrTimeout(currentTrialSettings.ExplorationTimeout));
            SetNextTrialState(stateMachine, TrialStateMachine.Displacement);
            yield break;
        }

        #endregion Protected Methods

        #region Private Methods

        private void HandleInfoAcknowledged()
        {
            playerManager.PlayerHasAcknowledged = true;
        }

        private void HandlePlayerEndedExploration()
        {
            playerManager.PlayerHasQuitExploration = true;
        }

        #endregion Private Methods
    }
}