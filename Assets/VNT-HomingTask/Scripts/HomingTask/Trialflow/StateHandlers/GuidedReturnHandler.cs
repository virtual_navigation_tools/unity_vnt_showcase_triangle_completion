﻿using System;
using System.Collections;
using System.Collections.Generic;
using unibi.homingtask.levelgen;
using unibi.homingtask.logistics;
using unibi.homingtask.settings;
using unibi.homingtask.ui;
using unibi.vnt.guidance;
using unibi.vnt.trialflow;
using unibi.vnt.util;
using UnityEngine;
using UXF;

namespace unibi.homingtask.trialflow
{
    public class GuidedReturnHandler : TrialStateHandler, ILoggable
    {
        #region Fields

        private TrialSettings currentTrialSettings;
        [SerializeField] private GuidanceManager guidanceManager;
        [SerializeField] private LevelManager levelManager;
        [SerializeField] private PlayerManager playerManager;
        [SerializeField] private Tracker playerTracker;
        [SerializeField] private DistanceChecker pointingDistanceChecker;
        private vnt.trialflow.TrialStateMachine stateMachine;
        [SerializeField] private TrialDataManager trialDataManager;
        [SerializeField] private UIManager uiManager;
        [SerializeField] private WaitManager waitManager;

        #endregion Fields

        #region Properties

        public override ITrialState HandledState => TrialStateMachine.GuidedReturn;
        public ICustomLogger Logger { get; set; }

        #endregion Properties

        #region Protected Methods

        protected override void HandleEnter(object sender, vnt.trialflow.TrialStateMachine stateMachine)
        {
            if (sender != HandledState)
            {
                return;
            }
            Logger.Log("entering State 'GuidedReturn' ...", LogType.Log, this, GameMaster.HomingTaskTag);

            // subscribe to guidance event
            GuidanceController.OnLastWaypointReached += HandleLastWaypointreached;

            // get settings object
            currentTrialSettings = (TrialSettings)Session.instance.CurrentTrial.settings.GetObject("TrialSettings");
            trialDataManager.TrialData.CurrentTrialSettings = currentTrialSettings;

            // save state machine ref
            this.stateMachine = stateMachine;
            StartCoroutine(RunStateLogicCoroutine());
        }

        protected override void HandleExit(object sender, EventArgs e)
        {
            if (sender != HandledState)
            {
                return;
            }
            Logger.Log("leaving State 'GuidedReturn' ...", LogType.Log, this, GameMaster.HomingTaskTag);
            // unsubsubscribe from guidance event
            GuidanceController.OnLastWaypointReached -= HandleLastWaypointreached;
            // reset guidance for cleanup
            guidanceManager.StopGuidance();
            guidanceManager.ResetController();
            waitManager.StopHomingTimer();
        }

        protected override IEnumerator RunStateLogicCoroutine()
        {
            Logger.Log("starting coroutine in state 'GuidedReturn' ...", LogType.Log, this, GameMaster.HomingTaskTag);
            playerManager.TeleportPlayerToPosition(
                currentTrialSettings.HomingStartPosition,
                isDisplacement: false,
                useAnimation: false);

            playerTracker.ResumeRecording();

            playerManager.FreezePlayer();
            playerManager.PlayerHasAcknowledged = false;
            uiManager.ShowGuidedReturnStartInfo(HandleInfoAcknowledged);

            yield return StartCoroutine(waitManager.WaitForUserOK());
            playerManager.UnFreezePlayer();
            waitManager.StartHomingTimer(currentTrialSettings.HomingTimeout);
            uiManager.SetScreenForDisplacement(waitManager.HomingTimer);

            levelManager.SetGoalVisible(true);

            guidanceManager.Initialise(new List<Vector3>() { currentTrialSettings.GoalPosition }, playerManager.Player);
            guidanceManager.StartGuidance();

            playerManager.PlayerHasFinishedDisplacement = false;
            yield return base.StartCoroutine(waitManager.WaitForDisplacementCompleteOrTimeout(currentTrialSettings.HomingTimeout));
            if (playerManager.PlayerHasFinishedDisplacement)
            {
                playerTracker.PauseRecording();
                playerManager.FreezePlayer();
                playerManager.PlayerHasAcknowledged = false;
                playerManager.PlayerHasGivenHomingConfidence = false;
                uiManager.ShowMockTrialConfidenceRequest(HandlePlayerHasGivenHomingConfidence);

                yield return StartCoroutine(waitManager.WaitForUserSetHomingConfidence());

                trialDataManager.SaveTrialResult(Session.instance.CurrentTrial);
                playerManager.UnFreezePlayer();
                SetNextTrialState(stateMachine, TrialStateMachine.TrialEnd);
                yield break;
            }
            else if (waitManager.HomingTimerHasReachedTimeout())
            {
                playerTracker.PauseRecording();
                SetNextTrialState(stateMachine, TrialStateMachine.Timeout);
                yield break;
            }
        }

        #endregion Protected Methods

        #region Private Methods

        private void HandleInfoAcknowledged()
        {
            playerManager.PlayerHasAcknowledged = true;
        }

        private void HandleLastWaypointreached(object sender, IWaypoint waypoint)
        {
            playerManager.PlayerHasFinishedDisplacement = true;
        }

        private void HandlePlayerHasGivenHomingConfidence()
        {
            playerManager.PlayerHasGivenHomingConfidence = true;
            trialDataManager.TrialData.CurrentHomingConfidence = uiManager.GetCurrentConfidenceSliderValue();
        }

        #endregion Private Methods
    }
}