﻿using unibi.homingtask.logistics;
using unibi.vnt.util;
using UnityEngine;
using UXF;

namespace unibi.homingtask.trialflow
{
    /// <summary>
    /// the trial manager receives the trial begin event call from UXF and causes our own state
    /// machine to go into the trial start state. For there on, the different state handlers will
    /// define the flow of the trial until the handler for the trialend state passes control back to
    /// UXF, by calling the end method on the current trial object.
    /// </summary>
    public class TrialManager : MonoBehaviour, ILoggable
    {
        #region Fields

        private TrialStateMachine trialStateMachine = new TrialStateMachine();

        #endregion Fields

        #region Properties

        public int CurrentPointingRep { get; private set; } = 0;

        public ICustomLogger Logger { get; set; }
        public TrialStateMachine TrialStateMachine { get => trialStateMachine; }

        #endregion Properties

        #region Public Methods

        public void AdvancePointingCounter()
        {
            CurrentPointingRep += 1;
        }

        public void OnSessionEnd()
        {
            trialStateMachine.SetState(TrialStateMachine.Empty);
        }

        public void OnTrialBegin(Trial trial)
        {
            Logger.Log("Starting trial state machine with state 'TrialStart'...", LogType.Log, this, GameMaster.HomingTaskTag);
            trialStateMachine.SetState(TrialStateMachine.TrialStart);
            CurrentPointingRep = 0;
        }

        #endregion Public Methods

        #region Private Methods

        private void OnDestroy()
        {
            Session.instance.onTrialBegin.RemoveListener(OnTrialBegin);
        }

        private void Start()
        {
            Session.instance.onTrialBegin.AddListener(OnTrialBegin);
        }

        #endregion Private Methods
    }
}