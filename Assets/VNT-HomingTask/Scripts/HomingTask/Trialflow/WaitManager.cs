﻿using System;
using System.Collections;
using unibi.homingtask.logistics;
using unibi.vnt.levelgen;
using unibi.vnt.trialflow;
using unibi.vnt.util;
using UnityEngine;

namespace unibi.homingtask.trialflow
{
    public class WaitManager : MonoBehaviour, ILoggable
    {
        #region Fields

        [SerializeField] private PausableTimer displacementTimer = null;
        [SerializeField] private PausableTimer explorationTimer = null;
        [SerializeField] private PausableTimer homingTimer = null;
        [SerializeField] private PlayerManager playerManager = null;
        [SerializeField] private PausableTimer pointingTimer = null;
        private bool setupDone = false;
        private bool teleportEffectDone = false;
        private bool timeoutInCurrentTrial = false;
        [SerializeField] private PausableTimer uiTimer = null;

        #endregion Fields

        #region Properties

        public PausableTimer DisplacementTimer { get => displacementTimer; }
        public PausableTimer ExplorationTimer { get => explorationTimer; }
        public PausableTimer HomingTimer { get => homingTimer; }
        public ICustomLogger Logger { get; set; }
        public PausableTimer PointingTimer { get => pointingTimer; }

        #endregion Properties

        #region Public Methods

        public bool HomingTimerHasReachedTimeout()
        {
            return timeoutInCurrentTrial;
        }

        public void PauseHomingTimer()
        {
            homingTimer.Pause();
        }

        public void ResumeHomingTimer()
        {
            homingTimer.ResumeTimer();
        }

        public void StartHomingTimer(float timeout)
        {
            homingTimer.StartWithNewTimeout(timeout);
        }

        public void StopHomingTimer()
        {
            homingTimer.StopAndReset();
        }

        public IEnumerator WaitForApproachComplete()
        {
            Logger.Log("waiting for player to reach the starting position...", LogType.Log, this, GameMaster.HomingTaskTag);
            yield return new WaitUntil(() => playerManager.PlayerHasApproachedStart);
        }

        public IEnumerator WaitForDisplacementComplete()
        {
            Logger.Log("waiting for player to finish displacement...", LogType.Log, this, GameMaster.HomingTaskTag);
            yield return new WaitUntil(() => playerManager.PlayerHasFinishedDisplacement);
        }

        public IEnumerator WaitForDisplacementCompleteOrTimeout(double displacementTimeout)
        {
            Logger.Log("waiting for player to finish displacement...", LogType.Log, this, GameMaster.HomingTaskTag);
            displacementTimer.StartWithNewTimeout(displacementTimeout);
            yield return new WaitUntil(() => playerManager.PlayerHasFinishedDisplacement || displacementTimer.HasReachedTimeout);
            displacementTimer.StopAndReset();
        }

        public IEnumerator WaitForExplorationDoneOrTimeout(double explorationTimeout)
        {
            Logger.Log("waiting for player to finish exploration...", LogType.Log, this, GameMaster.HomingTaskTag);
            explorationTimer.StartWithNewTimeout(explorationTimeout);
            yield return new WaitUntil(() => playerManager.PlayerHasQuitExploration || explorationTimer.HasReachedTimeout);
            explorationTimer.StopAndReset();
        }

        public IEnumerator WaitForHomingDoneOrTimeout()
        {
            Logger.Log("waiting for player to finish homing...", LogType.Log, this, GameMaster.HomingTaskTag);
            timeoutInCurrentTrial = false;
            yield return new WaitUntil(() => playerManager.PlayerHasEndedHoming || homingTimer.HasReachedTimeout);
            if (homingTimer.HasReachedTimeout)
            {
                timeoutInCurrentTrial = true;
            }
            homingTimer.StopAndReset();
        }

        public IEnumerator WaitForPointingDoneOrTimeout(double pointingTimeout)
        {
            Logger.Log("waiting for player to finish pointing...", LogType.Log, this, GameMaster.HomingTaskTag);
            pointingTimer.StartWithNewTimeout(pointingTimeout);
            yield return new WaitUntil(() => playerManager.PlayerHasPointed || pointingTimer.HasReachedTimeout);
            pointingTimer.StopAndReset();
        }

        public IEnumerator WaitForPointingReachedOrHomingAborted()
        {
            Logger.Log("waiting for player to trigger pointing or end trial...", LogType.Log, this, GameMaster.HomingTaskTag);
            timeoutInCurrentTrial = false;
            yield return new WaitUntil(() => playerManager.PlayerHasReachedPointingDistance
            || homingTimer.HasReachedTimeout
            || playerManager.PlayerHasEndedHoming);
            if (homingTimer.HasReachedTimeout)
            {
                timeoutInCurrentTrial = true;
            }
            if (homingTimer.HasReachedTimeout || playerManager.PlayerHasEndedHoming)
            {
                homingTimer.StopAndReset();
            }
        }

        public IEnumerator WaitForPointingReachedOrTimeout()
        {
            Logger.Log("waiting for player to trigger pointing...", LogType.Log, this, GameMaster.HomingTaskTag);
            timeoutInCurrentTrial = false;
            yield return new WaitUntil(() => playerManager.PlayerHasReachedPointingDistance || homingTimer.HasReachedTimeout);
            if (homingTimer.HasReachedTimeout)
            {
                timeoutInCurrentTrial = true;
                homingTimer.StopAndReset();
            }
        }

        public IEnumerator WaitForSetupDone()
        {
            Logger.Log("waiting for setup to complete...", LogType.Log, this, GameMaster.HomingTaskTag);
            yield return new WaitUntil(() => setupDone == true);
            setupDone = false;
        }

        public IEnumerator WaitForTeleportComplete()
        {
            Logger.Log("waiting for teleport to complete...", LogType.Log, this, GameMaster.HomingTaskTag);
            yield return new WaitUntil(() => teleportEffectDone == true);
            teleportEffectDone = false;
        }

        public IEnumerator WaitForTurnComplete()
        {
            Logger.Log("waiting for turn to complete...", LogType.Log, this, GameMaster.HomingTaskTag);
            yield return new WaitUntil(() => playerManager.PlayerHasCorrectFacing);
        }

        public IEnumerator WaitForUserOK()
        {
            Logger.Log("waiting for player OK...", LogType.Log, this, GameMaster.HomingTaskTag);
            yield return new WaitUntil(() => playerManager.PlayerHasAcknowledged);
        }

        public IEnumerator WaitForUserOKOrTimeout(double pointingTimeout)
        {
            Logger.Log("waiting for player OK...", LogType.Log, this, GameMaster.HomingTaskTag);
            uiTimer.StartWithNewTimeout(pointingTimeout);
            yield return new WaitUntil(() => playerManager.PlayerHasAcknowledged || uiTimer.HasReachedTimeout);
            uiTimer.StopAndReset();
        }

        public IEnumerator WaitForUserSetHomingConfidence()
        {
            Logger.Log("waiting for player homing confidence input...", LogType.Log, this, GameMaster.HomingTaskTag);
            yield return new WaitUntil(() => playerManager.PlayerHasGivenHomingConfidence);
        }

        public IEnumerator WaitForUserSetPointingConfidence()
        {
            Logger.Log("waiting for player pointing confidence input...", LogType.Log, this, GameMaster.HomingTaskTag);
            yield return new WaitUntil(() => playerManager.PlayerHasGivenPointingConfidence);
        }

        #endregion Public Methods

        #region Private Methods

        private void OnDestroy()
        {
            SceneSetup.SetupDone -= OnSetupDone;
            DisplacementHandler.TeleportComplete -= OnTeleportComplete;
        }

        private void OnSetupDone(object sender, EventArgs e)
        {
            setupDone = true;
        }

        private void OnTeleportComplete(object sender, bool usedAnimation)
        {
            if (usedAnimation)
            {
                teleportEffectDone = true;
            }
        }

        private void Start()
        {
            SceneSetup.SetupDone += OnSetupDone;
            DisplacementHandler.TeleportComplete += OnTeleportComplete;
        }

        #endregion Private Methods
    }
}