using unibi.vnt.trialflow;

namespace unibi.homingtask.trialflow
{
    public enum TrialStates
    {
        TrialStart, Exploration, Displacement, Homing, Pointing, GuidedReturn, Timeout, TrialEnd
    }

    public class TrialStateMachine : vnt.trialflow.TrialStateMachine
    {
        #region Fields

        public static ITrialState Displacement = new GenericTrialState(TrialStates.Displacement.ToString());
        public static ITrialState Exploration = new GenericTrialState(TrialStates.Exploration.ToString());
        public static ITrialState GuidedReturn = new GenericTrialState(TrialStates.GuidedReturn.ToString());
        public static ITrialState Homing = new GenericTrialState(TrialStates.Homing.ToString());
        public static ITrialState Pointing = new GenericTrialState(TrialStates.Pointing.ToString());
        public static ITrialState Timeout = new GenericTrialState(TrialStates.Timeout.ToString());
        public static ITrialState TrialEnd = new GenericTrialState(TrialStates.TrialEnd.ToString());
        public static ITrialState TrialStart = new GenericTrialState(TrialStates.TrialStart.ToString());

        #endregion Fields
    }
}