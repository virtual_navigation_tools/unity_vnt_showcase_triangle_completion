using System.Collections.Generic;
using unibi.homingtask.settings;
using unibi.vnt.levelgen;
using UnityEngine;

namespace unibi.homingtask.levelgen
{
    /// <summary>
    /// This class handles the different components which provide level generator functionalities
    /// </summary>
    public class LevelManager : MonoBehaviour
    {
        #region Fields

        [SerializeField] private DistanceBasedTileManager distanceBasedTileManager = null;
        [SerializeField] private int floorAreaRadius = 25;
        private bool isRunning = false;
        [SerializeField] private GameObject playerObject = null;
        [SerializeField] private SceneSetup sceneSetup = null;
        [SerializeField] private bool updateTiles = false;

        #endregion Fields

        #region Public Methods

        public List<GameObject> GetCurrentTiles()
        {
            var tileObjects = new List<GameObject>();
            foreach (var tile in distanceBasedTileManager.Tiles.Values)
            {
                tileObjects.Add(tile.TileObject);
            }
            return tileObjects;
        }

        public void SetGoalVisible(bool state)
        {
            sceneSetup.SetGoalVisible(state);
        }

        public void SetScene(TrialSettings settings)
        {
            sceneSetup.SetSceneForTrial(
                settings.LandmarkPositions,
                settings.GoalPosition,
                settings.HomingStartPosition,
                settings.WaypointPositions,
                settings.FoliagePerSqM);
        }

        #endregion Public Methods

        #region Internal Methods

        internal void RemoveLandmarks()
        {
            sceneSetup.RemoveLandmarks();
        }

        #endregion Internal Methods

        #region Private Methods

        private void Update()
        {
            if (updateTiles == true && isRunning == false)
            {
                distanceBasedTileManager.StartDistanceBasedTileHandling(playerObject, floorAreaRadius);
                isRunning = true;
            }
            else if (updateTiles == false && isRunning == true)
            {
                distanceBasedTileManager.StopDistanceBasedTileHandling();
                isRunning = false;
            }
        }

        #endregion Private Methods
    }
}