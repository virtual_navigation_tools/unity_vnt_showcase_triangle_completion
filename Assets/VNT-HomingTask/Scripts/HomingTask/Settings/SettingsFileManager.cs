﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using unibi.homingtask.logistics;
using unibi.vnt.util;
using UnityEngine;

namespace unibi.homingtask.settings
{
    public class SettingsFileManager : MonoBehaviour, ILoggable
    {
        #region Fields

        [SerializeField] private List<string> filesToIgnore = new List<string>() { };
        [SerializeField] private string folderName = "JSON_Input";
        private string sourceDir;

        #endregion Fields

        #region Properties

        public ICustomLogger Logger { get; set; }
        public Dictionary<string, SerialisableSessionDefinition> SessionDefinitions { get; private set; }

        #endregion Properties

        #region Public Methods

        public void UpdateSessionDefinitions()
        {
            if (SessionDefinitions == null)
            {
                SessionDefinitions = new Dictionary<string, SerialisableSessionDefinition>();
            }
            List<string> files = FileTools.GetFiles(sourceDir, "*.json", filesToIgnore.ToArray()).ToList();
            foreach (var file in files)
            {
                var key = Path.GetFileNameWithoutExtension(file);
                var value = JsonUtility.FromJson<SerialisableSessionDefinition>(File.ReadAllText(file));
                SessionDefinitions.Add(key, value);
            }
        }

        #endregion Public Methods

        #region Private Methods

        private void Awake()
        {
            sourceDir = Path.Combine(Application.persistentDataPath, folderName);
            UpdateSessionDefinitions();
        }

        private void Start()
        {
            // print traget folder
            Logger.Log($"VNT settings source dir is: {sourceDir}", LogType.Log, this, GameMaster.HomingTaskTag);
        }

        #endregion Private Methods
    }
}