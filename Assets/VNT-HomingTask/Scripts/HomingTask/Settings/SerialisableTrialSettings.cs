﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace unibi.homingtask.settings
{
    [System.Serializable]
    public class SerialisableTrialSettings
    {
        #region Fields

        public float DisplacementTimeout;
        public float ExplorationTimeout;
        public SerialisableVector3 FakeGoalPosition;
        public float FoliagePerSqM;
        public float ForwardVelInMPS;
        public SerialisableVector3 GoalPosition;
        public SerialisableVector3 HomingStartPosition;
        public float HomingTimeout;
        public float InfoTimeout;
        public bool IsGuidedTrial;
        public bool IsTraining;
        public SerialisableVector3[] LandmarkPositions;
        public SerialisableVector3[] LandmarkPositionsShifted;
        public string Mode;
        public string Name;
        public float[] PointingDistances;
        public float PointingTimeout;
        public bool ShowError;
        public bool ShowFakeGoalPosition;
        public bool ShowLandmarksDuringDisplacement;
        public bool ShowScore;
        public SerialisableVector3[] WaypointPositions;

        #endregion Fields

        #region Public Constructors

        public SerialisableTrialSettings(string name,
            SerialisableVector3 goalPosition,
            SerialisableVector3 fakeGoalPosition,
            SerialisableVector3 homingStartPosition,
            float[] pointingDistances,
            SerialisableVector3[] landmarkPositions,
            SerialisableVector3[] landmarkPositionsShifted,
        SerialisableVector3[] waypointPositions,
            string mode,
            float foliagePerSqM,
            float forwardVelInMPS,
            bool isTraining,
            bool showLandmarks,
            bool showError,
            bool showScore,
            bool showFakeGoalPosition,
            float homingTimeout,
            float pointingTimeout,
            float displacementTimeout,
            float infoTimeout,
            float explorationTimeout,
            bool isGuidedTrial)
        {
            Name = name;
            GoalPosition = goalPosition;
            FakeGoalPosition = fakeGoalPosition;
            HomingStartPosition = homingStartPosition;
            PointingDistances = pointingDistances;
            LandmarkPositions = landmarkPositions;
            LandmarkPositionsShifted = landmarkPositionsShifted;
            WaypointPositions = waypointPositions;
            Mode = mode;
            FoliagePerSqM = foliagePerSqM;
            ForwardVelInMPS = forwardVelInMPS;
            IsTraining = isTraining;
            ShowLandmarksDuringDisplacement = showLandmarks;
            ShowError = showError;
            ShowScore = showScore;
            ShowFakeGoalPosition = showFakeGoalPosition;
            HomingTimeout = homingTimeout;
            PointingTimeout = pointingTimeout;
            DisplacementTimeout = displacementTimeout;
            InfoTimeout = infoTimeout;
            ExplorationTimeout = explorationTimeout;
            IsGuidedTrial = isGuidedTrial;
        }

        #endregion Public Constructors

        #region Public Methods

        public static SerialisableTrialSettings CreateFromJSON(string jsonString)
        {
            return JsonUtility.FromJson<SerialisableTrialSettings>(jsonString);
        }

        public TrialSettings ConvertToSO()
        {
            TrialSettings settingSO = ScriptableObject.CreateInstance<TrialSettings>();

            #region get mode from string

            TrialSettings.DisplacementMode mode;
            if (string.Equals(this.Mode, "Teleport", System.StringComparison.CurrentCultureIgnoreCase))
            {
                mode = TrialSettings.DisplacementMode.Teleport;
            }
            else if (string.Equals(this.Mode, "Walking", System.StringComparison.CurrentCultureIgnoreCase))
            {
                mode = TrialSettings.DisplacementMode.Walking;
            }
            else
            {
                mode = TrialSettings.DisplacementMode.Teleport;
                Debug.LogWarningFormat("{0} is not a valid displacement mode. Deafaulting to 'Teleport' for current trial...", this.Mode);
            }

            #endregion get mode from string

            settingSO.Init(
                ToVector3(this.GoalPosition),
                ToVector3(this.FakeGoalPosition),
                ToVector3(this.HomingStartPosition),
                this.PointingDistances.ToList(),
                ToVector3List(this.LandmarkPositions),
                ToVector3List(this.LandmarkPositionsShifted),
                ToVector3List(this.WaypointPositions),
                mode,
                this.FoliagePerSqM,
                this.ForwardVelInMPS,
                this.IsTraining,
                this.ShowLandmarksDuringDisplacement,
                this.ShowError,
                this.ShowScore,
                this.ShowFakeGoalPosition,
                this.Name,
                this.HomingTimeout,
                this.PointingTimeout,
                this.DisplacementTimeout,
                this.InfoTimeout,
                this.ExplorationTimeout,
                this.IsGuidedTrial
                );

            return settingSO;
        }

        #endregion Public Methods

        #region Private Methods

        private static Vector3 ToVector3(SerialisableVector3 input)
        {
            return new Vector3
                (
                    input.X,
                    input.Y,
                    input.Z
                );
        }

        private static List<Vector3> ToVector3List(SerialisableVector3[] input)
        {
            List<Vector3> outList = new List<Vector3>();
            for (int i = 0; i < input.Length; i++)
            {
                outList.Add(ToVector3(input[i]));
            }
            return outList;
        }

        #endregion Private Methods
    }
}