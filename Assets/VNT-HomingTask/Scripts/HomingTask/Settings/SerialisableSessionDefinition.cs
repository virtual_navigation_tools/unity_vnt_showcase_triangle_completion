﻿using System.Collections.Generic;
using UnityEngine;

namespace unibi.homingtask.settings
{
    [System.Serializable]
    public class SerialisableSessionDefinition
    {
        #region Fields

        public SerialisableBlockDefinition[] Blocks;
        public string Name;

        #endregion Fields

        #region Public Constructors

        public SerialisableSessionDefinition(string name, SerialisableBlockDefinition[] blocks)
        {
            Name = name;
            Blocks = blocks;
        }

        #endregion Public Constructors

        #region Public Methods

        public static SerialisableSessionDefinition CreateFromJSON(string jsonString)
        {
            return JsonUtility.FromJson<SerialisableSessionDefinition>(jsonString);
        }

        public SessionDefinition ConvertToSO()
        {
            SessionDefinition sessionSO = ScriptableObject.CreateInstance<SessionDefinition>();
            List<BlockDefinition> blockList = new List<BlockDefinition>();
            foreach (var block in this.Blocks)
            {
                blockList.Add(block.ConvertToSO());
            }
            sessionSO.Init(blockList.ToArray(), this.Name);

            //string assetPathAndName = AssetDatabase.GenerateUniqueAssetPath("Assets//ScriptableObjects//SettingsObjects//Runtime//CurrentSession_RuntimeSO.asset");
            //AssetDatabase.CreateAsset(sessionSO, assetPathAndName);
            //AssetDatabase.SaveAssets();
            //AssetDatabase.Refresh();

            return sessionSO;
        }

        #endregion Public Methods
    }
}