﻿using UnityEngine;

namespace unibi.homingtask.settings
{
    [System.Serializable]
    public class SerialisableVector3
    {
        #region Fields

        public float X;
        public float Y;
        public float Z;

        #endregion Fields

        #region Public Constructors

        public SerialisableVector3(float x, float y, float z)
        {
            X = x;
            Y = y;
            Z = z;
        }

        #endregion Public Constructors

        #region Public Methods

        public static SerialisableVector3 CreateFromVector3(Vector3 vector3)
        {
            return new SerialisableVector3(vector3.x, vector3.y, vector3.z);
        }

        public static SerialisableVector3[] CreateFromVector3(Vector3[] vector3s)
        {
            SerialisableVector3[] outArray = new SerialisableVector3[vector3s.Length];
            for (int i = 0; i < outArray.Length; i++)
            {
                outArray[i] = SerialisableVector3.CreateFromVector3(vector3s[i]);
            }
            return outArray;
        }

        public override string ToString()
        {
            return string.Format("X: {0}, Y: {1}, Z: {2}", X, Y, Z);
        }

        #endregion Public Methods
    }
}