﻿using UnityEngine;

namespace unibi.homingtask.settings
{
    [CreateAssetMenu(fileName = "SessionDefinition", menuName = "ScriptableObjects/DataHandling/SessionDefinition")]
    public class SessionDefinition : ScriptableObject
    {
        #region Fields

        public BlockDefinition[] Blocks;
        public string Name;

        #endregion Fields

        #region Public Methods

        public void Init(BlockDefinition[] blocks, string name)
        {
            Blocks = blocks;
            Name = name;
            this.name = Name;
        }

        public SerialisableSessionDefinition MakeSerialisable()
        {
            SerialisableBlockDefinition[] blocks = new SerialisableBlockDefinition[this.Blocks.Length];
            for (int i = 0; i < blocks.Length; i++)
            {
                blocks[i] = this.Blocks[i].MakeSerialisable();
            }
            SerialisableSessionDefinition sessionDef = new SerialisableSessionDefinition(this.Name, blocks);
            return sessionDef;
        }

        #endregion Public Methods
    }
}