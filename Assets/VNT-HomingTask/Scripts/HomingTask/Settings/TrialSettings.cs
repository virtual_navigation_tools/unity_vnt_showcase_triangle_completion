﻿using System.Collections.Generic;
using UnityEngine;

namespace unibi.homingtask.settings
{
    [CreateAssetMenu(fileName = "TrialSettings", menuName = "ScriptableObjects/DataHandling/TrialSettings")]
    public class TrialSettings : ScriptableObject
    {
        #region Fields

        public float DisplacementTimeout;
        public float ExplorationTimeout;
        public Vector3 FakeGoalPosition;
        public float FoliagePerSqM;
        public float ForwardVelInMPS;
        public Vector3 GoalPosition;
        public Vector3 HomingStartPosition;
        public float HomingTimeout;
        public float InfoTimeout;
        public bool IsGuidedTrial;
        public bool IsTraining;
        public List<Vector3> LandmarkPositions;
        public List<Vector3> LandmarkPositionsShifted;
        public DisplacementMode Mode;
        public string Name;
        public List<float> PointingDistances;
        public float PointingTimeout;
        public bool ShowError;
        public bool ShowFakeGoalPosition;
        public bool ShowLandmarksDuringDisplacement;
        public bool ShowScore;
        public List<Vector3> WaypointPositions;

        #endregion Fields

        #region Enums

        public enum DisplacementMode
        { Walking, Teleport }

        #endregion Enums

        #region Public Methods

        public void Init(Vector3 goalPosition,
            Vector3 fakeGoalPosition,
            Vector3 homingStartPosition,
            List<float> pointingDistances,
            List<Vector3> landmarkPositions,
            List<Vector3> landmarkPositionsShifted,
            List<Vector3> waypointPositions,
            DisplacementMode mode,
            float foliagePerSqM,
            float forwardVelInMPS,
            bool isTraining,
            bool showLandmarks,
            bool showError,
            bool showScore,
            bool showFakeGoalPosition,
            string name,
            float homingTimeout,
            float pointingTimeout,
            float displacementTimeout,
            float infoTimeout,
            float explorationTimeout,
            bool isGuidedTrial)
        {
            GoalPosition = goalPosition;
            FakeGoalPosition = fakeGoalPosition;
            HomingStartPosition = homingStartPosition;
            PointingDistances = pointingDistances;
            LandmarkPositions = landmarkPositions;
            LandmarkPositionsShifted = landmarkPositionsShifted;
            WaypointPositions = waypointPositions;
            Mode = mode;
            IsTraining = isTraining;
            ShowLandmarksDuringDisplacement = showLandmarks;
            ShowError = showError;
            ShowScore = showScore;
            ShowFakeGoalPosition = showFakeGoalPosition;
            Name = name;
            FoliagePerSqM = foliagePerSqM;
            ForwardVelInMPS = forwardVelInMPS;
            HomingTimeout = homingTimeout;
            PointingTimeout = pointingTimeout;
            DisplacementTimeout = displacementTimeout;
            InfoTimeout = infoTimeout;
            ExplorationTimeout = explorationTimeout;
            this.name = Name;
            IsGuidedTrial = isGuidedTrial;
        }

        public SerialisableTrialSettings MakeSerialisable()
        {
            return new SerialisableTrialSettings(this.Name,
                SerialisableVector3.CreateFromVector3(this.GoalPosition),
                SerialisableVector3.CreateFromVector3(this.FakeGoalPosition),
                SerialisableVector3.CreateFromVector3(this.HomingStartPosition),
                this.PointingDistances.ToArray(),
                SerialisableVector3.CreateFromVector3(this.LandmarkPositions.ToArray()),
                SerialisableVector3.CreateFromVector3(this.LandmarkPositionsShifted.ToArray()),
                SerialisableVector3.CreateFromVector3(this.WaypointPositions.ToArray()),
                this.Mode.ToString(),
                this.FoliagePerSqM,
                this.ForwardVelInMPS,
                this.IsTraining,
                this.ShowLandmarksDuringDisplacement,
                this.ShowError,
                this.ShowScore,
                this.ShowFakeGoalPosition,
                this.HomingTimeout,
                this.PointingTimeout,
                this.DisplacementTimeout,
                this.InfoTimeout,
                this.ExplorationTimeout,
                this.IsGuidedTrial);
        }

        #endregion Public Methods
    }
}