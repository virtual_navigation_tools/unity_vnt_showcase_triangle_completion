﻿using UnityEngine;

namespace unibi.homingtask.settings
{
    [CreateAssetMenu(fileName = "BlockDefinition", menuName = "ScriptableObjects/DataHandling/BlockDefinition")]
    public class BlockDefinition : ScriptableObject
    {
        #region Fields

        public string Name;
        public bool ShuffleTrials;
        public TrialSettings[] TrialSettings;

        #endregion Fields

        #region Public Methods

        public void Init(TrialSettings[] trialSettings, string name, bool doShuffle)
        {
            TrialSettings = trialSettings;
            Name = name;
            this.name = Name;
            ShuffleTrials = doShuffle;
        }

        public SerialisableBlockDefinition MakeSerialisable()
        {
            SerialisableTrialSettings[] trials = new SerialisableTrialSettings[this.TrialSettings.Length];
            for (int i = 0; i < trials.Length; i++)
            {
                trials[i] = this.TrialSettings[i].MakeSerialisable();
            }
            SerialisableBlockDefinition blockDef = new SerialisableBlockDefinition(this.Name, trials, ShuffleTrials);
            return blockDef;
        }

        #endregion Public Methods
    }
}