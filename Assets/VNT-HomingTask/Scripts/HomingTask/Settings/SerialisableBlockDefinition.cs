﻿using System.Collections.Generic;
using UnityEngine;

namespace unibi.homingtask.settings
{
    [System.Serializable]
    public class SerialisableBlockDefinition
    {
        #region Fields

        public string Name;
        public bool ShuffleTrials;
        public SerialisableTrialSettings[] TrialSettings;

        #endregion Fields

        #region Public Constructors

        public SerialisableBlockDefinition(string name, SerialisableTrialSettings[] trialSettings, bool doShuffle)
        {
            Name = name;
            TrialSettings = trialSettings;
            ShuffleTrials = doShuffle;
        }

        #endregion Public Constructors

        #region Public Methods

        public static SerialisableBlockDefinition CreateFromJSON(string jsonString)
        {
            return JsonUtility.FromJson<SerialisableBlockDefinition>(jsonString);
        }

        public BlockDefinition ConvertToSO()
        {
            BlockDefinition blockSO = ScriptableObject.CreateInstance<BlockDefinition>();
            List<TrialSettings> trialList = new List<TrialSettings>();
            foreach (var trial in this.TrialSettings)
            {
                trialList.Add(trial.ConvertToSO());
            }
            blockSO.Init(trialList.ToArray(), this.Name, ShuffleTrials);

            return blockSO;
        }

        #endregion Public Methods
    }
}