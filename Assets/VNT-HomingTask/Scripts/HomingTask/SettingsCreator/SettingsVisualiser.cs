﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace unibi.homingtask.settings
{
    [ExecuteInEditMode]
    public class SettingsVisualiser : MonoBehaviour
    {
        #region Fields

        private GameObject goalObject;
        [SerializeField] private GameObject homeMarker = null;
        private GameObject homeStartObject;
        [SerializeField] private GameObject landmarkMarker = null;
        private List<GameObject> landmarkObjects = new List<GameObject>();
        [SerializeField] private GameObject objectContainer = null;
        [SerializeField] private TrialSettings settings = null;
        [SerializeField] private GameObject shiftedLandmarkMarker = null;
        [SerializeField] private GameObject startMarker = null;
        [SerializeField] private GameObject waypointMarker = null;
        private List<GameObject> waypointObjects = new List<GameObject>();

        #endregion Fields

        #region Public Methods

        public void ClearAll()
        {
            List<Transform> toRemove = objectContainer.GetComponentsInChildren<Transform>().ToList();
            toRemove.Remove(objectContainer.transform); //ignore container
            // destroy list items starting from the end so we don't mess up the indices of the iterator
            for (int i = toRemove.Count - 1; i >= 0; i--)
            {
                DestroyImmediate(toRemove[i].gameObject);
            }
        }

        public void VisualiseSettings()
        {
            ClearAll();
            goalObject = Instantiate(homeMarker);
            goalObject.transform.position = settings.GoalPosition;
            goalObject.transform.SetParent(objectContainer.transform);

            homeStartObject = Instantiate(startMarker);
            homeStartObject.transform.position = settings.HomingStartPosition;
            homeStartObject.transform.SetParent(objectContainer.transform);

            foreach (var pos in settings.WaypointPositions)
            {
                var newObj = Instantiate(waypointMarker);
                newObj.transform.position = pos;
                newObj.transform.SetParent(objectContainer.transform);
                waypointObjects.Add(newObj);
            }

            foreach (var pos in settings.LandmarkPositions)
            {
                var newObj = Instantiate(landmarkMarker);
                newObj.transform.position = pos;
                newObj.transform.SetParent(objectContainer.transform);
                landmarkObjects.Add(newObj);
            }

            foreach (var pos in settings.LandmarkPositionsShifted)
            {
                var newObj = Instantiate(shiftedLandmarkMarker);
                newObj.transform.position = pos;
                newObj.transform.SetParent(objectContainer.transform);
                landmarkObjects.Add(newObj);
            }
        }

        #endregion Public Methods

        #region Private Methods

        private void OnDrawGizmos()
        {
            foreach (var dist in settings.PointingDistances)
            {
                Gizmos.color = Color.red;
                Gizmos.DrawWireSphere(settings.HomingStartPosition, (float)Math.Abs(dist));
            }
        }

        #endregion Private Methods
    }
}