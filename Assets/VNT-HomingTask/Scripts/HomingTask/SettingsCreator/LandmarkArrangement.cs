﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace unibi.homingtask.settings
{
    public class LandmarkArrangement : MonoBehaviour
    {
        #region Fields

        [SerializeField] private float minDistanceBetweenObjectsInMeters = 5;
        [SerializeField] private int numberLandmarks = 0;
        [SerializeField] private float placemantRadiusInM = 50f;
        private List<Vector3> positions = new List<Vector3>();
        private List<Vector3> savedPositions = new List<Vector3>();
        [SerializeField] private TrialSettings settings = null;
        [SerializeField, Range(-180, 180)] private float shiftRotationInDegrees = 0;

        #endregion Fields

        #region Public Methods

        public void ClearLandmarks()
        {
            Debug.LogWarning("clearing previous landmarks!");
            settings.LandmarkPositions.Clear();
            positions.Clear();
            savedPositions.Clear();
        }

        public void ClearShiftedLandmarks()
        {
            Debug.LogWarning("clearing previous sifted landmarks!");
            settings.LandmarkPositionsShifted.Clear();
        }

        public void CopyLandmarks()
        {
            Debug.Log("Saving current landmark positions ...");
            savedPositions = new List<Vector3>(settings.LandmarkPositions);
        }

        public void CreateShiftedLandmarks()
        {
            List<Vector3> points = settings.LandmarkPositions;
            var pivot = settings.HomingStartPosition;
            var rot = new Vector3(0, shiftRotationInDegrees, 0);
            foreach (var point in points)
            {
                settings.LandmarkPositionsShifted.Add(RotatePointAroundPivot(point, pivot, rot));
            }
        }

        public void GenerateLandmarksForSettings(bool clearLandmarks = false, bool resetSeed = false)
        {
            if (resetSeed == true)
            {
                ReseedRandom();
            }
            // clear old landmarks if true
            if (clearLandmarks == true)
            {
                ClearLandmarks();
            }
            Vector3 centroid = CalculateCOM();
            Debug.LogFormat("centroid at: {0}", centroid);
            settings.LandmarkPositions.AddRange(GenerateLandmarksAround(centroid));
            settings.LandmarkPositions.Distinct();
        }

        public void MirrorLandmarksAlongZAxis()
        {
            for (int i = 0; i < settings.LandmarkPositions.Count; i++)
            {
                settings.LandmarkPositions[i] = Vector3.Scale(settings.LandmarkPositions[i], new Vector3(-1, 1, 1));
            }
        }

        public void PasteLandmarks()
        {
            settings.LandmarkPositions = savedPositions;
            Debug.Log("applying saved landmark positions ...");
        }

        public void ReseedRandom()
        {
            Debug.LogWarning("reseeding RNG!");
            UnityEngine.Random.InitState(DateTime.Now.Millisecond);
        }

        #endregion Public Methods

        #region Private Methods

        private bool AreTooClose(Vector3 pos, Vector3 newPos)
        {
            Vector3 offset = pos - newPos;
            float sqrMagnitude = offset.sqrMagnitude;
            float sqrDist = minDistanceBetweenObjectsInMeters * minDistanceBetweenObjectsInMeters;
            if (sqrMagnitude < sqrDist)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        private Vector3 CalculateCOM()
        {
            List<Vector3> points = new List<Vector3> { settings.GoalPosition, settings.HomingStartPosition };
            points.AddRange(settings.WaypointPositions);
            points = points.Distinct().ToList();
            var com_x = 0f;
            var com_z = 0f;
            for (int i = 0; i < points.Count; i++)
            {
                com_x += points[i].x;
                com_z += points[i].z;
            }
            return new Vector3(com_x / points.Count, 0, com_z / points.Count);
        }

        private PolygonCollider2D CreateColliderForPathSegment(Vector2 start, Vector2 end)
        {
            Vector2 along = (end - start).normalized;
            Vector2 across = Vector2.Perpendicular(along).normalized;
            var cornerA = start
                - along * minDistanceBetweenObjectsInMeters / 2
                + across * minDistanceBetweenObjectsInMeters / 2;
            var cornerB = start
                - along * minDistanceBetweenObjectsInMeters / 2
                - across * minDistanceBetweenObjectsInMeters / 2;
            var cornerC = end
                + along * minDistanceBetweenObjectsInMeters / 2
                - across * minDistanceBetweenObjectsInMeters / 2;
            var cornerD = end
                + along * minDistanceBetweenObjectsInMeters / 2
                + across * minDistanceBetweenObjectsInMeters / 2;
            //Debug.LogFormat("corners are: {0},{1},{2},{3}",cornerA,cornerB,cornerC,cornerD);
            PolygonCollider2D blockedPath = gameObject.AddComponent<PolygonCollider2D>();
            blockedPath.points = new Vector2[] { cornerA, cornerB, cornerC, cornerD };
            return blockedPath;
        }

        private List<Vector3> GenerateLandmarksAround(Vector3 centroid)
        {
            var failedTries = 0;
            var cutOffCount = 5000;
            while (failedTries < cutOffCount && positions.Count < numberLandmarks)
            {
                //var xPos = UnityEngine.Random.Range(-centroid.x - placemantRadiusInM, centroid.x + placemantRadiusInM);
                //var zPos = UnityEngine.Random.Range(-centroid.z - placemantRadiusInM, centroid.z + placemantRadiusInM);
                var newPos2D = (UnityEngine.Random.insideUnitCircle * placemantRadiusInM);
                var newPos = (new Vector3(newPos2D.x, centroid.y, newPos2D.y)) + centroid;
                Debug.LogFormat("Trying to place at {0}", newPos);
                if (IsValid(newPos))
                {
                    positions.Add(newPos);
                }
                else
                {
                    failedTries += 1;
                }
            }
            if (failedTries == cutOffCount && positions.Count < numberLandmarks)
            {
                Debug.LogWarningFormat("Landmark generator exited after {0} attempts with {1}/{2} object created",
                    failedTries,
                    positions.Count,
                    numberLandmarks);
            }
            return positions;
        }

        private bool IsInBlockedArea(Vector3 newPos)
        {
            if (AreTooClose(newPos, settings.HomingStartPosition))
            {
                Debug.LogFormat("{0} was too close to homing start", newPos);
                return true;
            }
            if (AreTooClose(newPos, settings.GoalPosition))
            {
                Debug.LogFormat("{0} was too close to goal", newPos);
                return true;
            }
            if (settings.Mode == TrialSettings.DisplacementMode.Walking && IsOnGuidancePath(newPos))
            {
                Debug.LogFormat("{0} was too close to path", newPos);
                return true;
            }
            else
            {
                return false;
            }
        }

        private bool IsOnGuidancePath(Vector3 newPos)
        {
            List<Vector3> points = new List<Vector3>() { settings.GoalPosition };
            points.AddRange(settings.WaypointPositions);
            points.Add(settings.GoalPosition);

            bool isOnGuidancePath = false;
            for (int i = 1; i < points.Count; i++)
            {
                var start = new Vector2(points[i - 1].x, points[i - 1].z);
                var end = new Vector2(points[i].x, points[i].z);
                var blockedPath = CreateColliderForPathSegment(start, end);
                if (blockedPath.OverlapPoint(new Vector2(newPos.x, newPos.z)))
                {
                    isOnGuidancePath = true;
                    DestroyImmediate(blockedPath);
                    break;
                }
                else
                {
                    DestroyImmediate(blockedPath);
                }
            }

            return isOnGuidancePath;
        }

        private bool IsValid(Vector3 newPos)
        {
            bool failedDistanceCheck = false;
            foreach (var pos in positions)
            {
                if (AreTooClose(pos, newPos))
                {
                    failedDistanceCheck = true;
                    Debug.LogFormat("{0} was too close to {1}", newPos, pos);
                    break;
                }
            }
            if (failedDistanceCheck || IsInBlockedArea(newPos))
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        /// adapted from https://discussions.unity.com/t/rotate-a-vector-around-a-certain-point/81225
        private Vector3 RotatePointAroundPivot(Vector3 point, Vector3 pivot, Vector3 rotation)
        {
            // get vector to pivot
            var pivotDir = point - pivot;
            // do rotation
            var newPoint = Quaternion.Euler(rotation) * pivotDir + pivot;
            return newPoint;
        }

        #endregion Private Methods
    }
}