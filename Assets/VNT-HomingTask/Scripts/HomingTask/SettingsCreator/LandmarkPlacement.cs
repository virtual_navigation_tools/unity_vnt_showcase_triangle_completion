﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace unibi.homingtask.settings
{
    [ExecuteInEditMode]
    public class LandmarkPlacement : MonoBehaviour
    {
        #region Fields

        [Range(0, 100)][SerializeField] private float distance = 10;
        private GameObject goalMarker = null;
        [SerializeField] private Material goalMat = null;
        [SerializeField] private Vector3 goalPosition = new Vector3(0, 0, 0);
        [SerializeField] private List<Vector3> landmarkPositions = null;
        [SerializeField] private Material markerMat = null;
        private List<GameObject> markers = new List<GameObject>();
        [SerializeField] private TrialSettings trialSettings = null;

        #endregion Fields

        #region Public Methods

        public void ApplyLandmarksToTrial()
        {
            trialSettings.LandmarkPositions = landmarkPositions;
        }

        public void ArrangeAround()
        {
            int nObjects = landmarkPositions.Count;

            float angleBetween = Mathf.PI * 2f / nObjects;
            float currentAngle = 0f;
            for (int i = 0; i < landmarkPositions.Count; i++)
            {
                float x = distance * Mathf.Cos(currentAngle);
                float z = distance * Mathf.Sin(currentAngle);
                landmarkPositions[i] = new Vector3(x, 0, z);
                currentAngle += angleBetween;
            }
            UpdateScene();
        }

        public void ClearScene()
        {
            foreach (var marker in markers)
            {
                DestroyImmediate(marker);
            }
            if (goalMarker != null)
            {
                DestroyImmediate(goalMarker);
            }
        }

        public void UpdateScene()
        {
            ClearScene();
            UpdateGoalMarker();
            UpdateMarkers();
            goalMarker.transform.position = goalPosition;
            for (int i = 0; i < landmarkPositions.Count; i++)
            {
                markers[i].transform.position = landmarkPositions[i];
            }
        }

        #endregion Public Methods

        #region Private Methods

        private void Start()
        {
            UpdateScene();
        }

        private void UpdateGoalMarker()
        {
            if (goalMarker == null)
            {
                goalMarker = GameObject.CreatePrimitive(PrimitiveType.Sphere);
                goalMarker.GetComponentInChildren<Renderer>().material = goalMat;
            }
        }

        private void UpdateMarkers()
        {
            foreach (var marker in markers)
            {
                DestroyImmediate(marker);
            }
            markers.Clear();
            foreach (var pos in landmarkPositions)
            {
                GameObject newMarker = GameObject.CreatePrimitive(PrimitiveType.Sphere);
                newMarker.GetComponentInChildren<Renderer>().material = markerMat;
                markers.Add(newMarker);
            }
        }

        #endregion Private Methods
    }
}