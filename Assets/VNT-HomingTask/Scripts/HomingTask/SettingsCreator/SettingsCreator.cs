﻿using System.IO;
using UnityEngine;

namespace unibi.homingtask.settings
{
    [ExecuteInEditMode]
    public class SettingsCreator : MonoBehaviour
    {
        #region Fields

        [SerializeField] private string fileName = "defaultSession.json";

        [SerializeField] private string folderPath = null;

        [SerializeField] private SessionDefinition sessionDef = null;

        #endregion Fields

        #region Public Methods

        public void InitOutputFolder()
        {
            if (string.IsNullOrEmpty(folderPath))
            {
                folderPath = Path.Combine(Application.dataPath, "SettingsFiles");
            }

            if (!Directory.Exists(folderPath))
            {
                Directory.CreateDirectory(folderPath);
                Debug.Log($"Creating settings folder at {folderPath}");
            }
        }

        public void SaveCurrentSessionDefinition()
        {
            if (sessionDef != null)
            {
                string path = Path.Combine(folderPath, fileName);
                if (!File.Exists(path))
                {
                    Directory.CreateDirectory(Path.GetDirectoryName(path));
                    File.Create(path).Close();
                }
                SerialisableSessionDefinition sessionToSave = sessionDef.MakeSerialisable();
                string jsonstring = JsonUtility.ToJson(sessionToSave);

                File.WriteAllText(path, jsonstring);
                Debug.LogFormat("File saved as: {0}", path);
            }
        }

        #endregion Public Methods
    }
}