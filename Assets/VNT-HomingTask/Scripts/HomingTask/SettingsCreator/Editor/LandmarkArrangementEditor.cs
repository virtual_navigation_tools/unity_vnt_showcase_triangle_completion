﻿using UnityEditor;
using UnityEngine;

namespace unibi.homingtask.settings
{
    [CustomEditor(typeof(LandmarkArrangement))]
    public class LandmarkArrangementEditor : Editor
    {
        #region Public Methods

        public override void OnInspectorGUI()
        {
            DrawDefaultInspector();
            LandmarkArrangement arrangement = (LandmarkArrangement)target;
            if (GUILayout.Button("Reseed RNG"))
            {
                arrangement.ReseedRandom();
            }
            if (GUILayout.Button("Generate Landmarks"))
            {
                arrangement.GenerateLandmarksForSettings(false, true);
            }
            if (GUILayout.Button("Mirror Landmarks"))
            {
                arrangement.MirrorLandmarksAlongZAxis();
            }
            if (GUILayout.Button("Create Shifted Landmarks"))
            {
                arrangement.CreateShiftedLandmarks();
            }
            if (GUILayout.Button("Copy Landmarks"))
            {
                arrangement.CopyLandmarks();
            }
            if (GUILayout.Button("Paste Landmarks"))
            {
                arrangement.PasteLandmarks();
            }
            if (GUILayout.Button("Clear Landmarks"))
            {
                arrangement.ClearLandmarks();
            }
            if (GUILayout.Button("Clear Shifted Landmarks"))
            {
                arrangement.ClearShiftedLandmarks();
            }
        }

        #endregion Public Methods
    }
}