﻿using UnityEditor;
using UnityEngine;

namespace unibi.homingtask.settings
{
    [CustomEditor(typeof(LandmarkPlacement))]
    public class LandmarkPlacementEditor : Editor
    {
        #region Public Methods

        public override void OnInspectorGUI()
        {
            DrawDefaultInspector();
            LandmarkPlacement placement = (LandmarkPlacement)target;
            if (GUILayout.Button("Update Scene"))
            {
                placement.UpdateScene();
            }
            if (GUILayout.Button("Arrange Around"))
            {
                placement.ArrangeAround();
            }
            if (GUILayout.Button("Apply to Trial"))
            {
                placement.ApplyLandmarksToTrial();
            }
            if (GUILayout.Button("Clear Scene"))
            {
                placement.ClearScene();
            }
        }

        #endregion Public Methods
    }
}