﻿using UnityEditor;
using UnityEngine;

namespace unibi.homingtask.settings
{
    [CustomEditor(typeof(SettingsCreator))]
    public class SettingsCreatorEditor : Editor
    {
        #region Public Methods

        public override void OnInspectorGUI()
        {
            DrawDefaultInspector();
            SettingsCreator creator = (SettingsCreator)target;
            if (GUILayout.Button("Save Current Settings"))
            {
                creator.SaveCurrentSessionDefinition();
            }
            if (GUILayout.Button("Init Output Folder"))
            {
                creator.InitOutputFolder();
            }
        }

        #endregion Public Methods
    }
}