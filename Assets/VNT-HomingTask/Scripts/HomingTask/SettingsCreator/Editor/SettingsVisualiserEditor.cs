﻿using UnityEditor;
using UnityEngine;

namespace unibi.homingtask.settings
{
    [CustomEditor(typeof(SettingsVisualiser))]
    public class SettingsVisualiserEditor : Editor
    {
        #region Public Methods

        public override void OnInspectorGUI()
        {
            DrawDefaultInspector();
            SettingsVisualiser visualiser = (SettingsVisualiser)target;
            if (GUILayout.Button("Visualise Settings"))
            {
                visualiser.VisualiseSettings();
            }
            if (GUILayout.Button("Clear All"))
            {
                visualiser.ClearAll();
            }
        }

        #endregion Public Methods
    }
}