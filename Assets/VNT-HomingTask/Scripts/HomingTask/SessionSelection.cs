﻿using System;
using System.Collections.Generic;
using System.Linq;
using TMPro;
using unibi.homingtask.settings;
using unibi.vnt.util;
using UnityEngine;
using UXF;

namespace unibi.homingtask.menus
{
    public class SessionSelection : MonoBehaviour, ILoggable
    {
        #region Fields

        [SerializeField]
        private string defaultOkButtonText = "Select this session";

        [SerializeField]
        private TextMeshProUGUI okButtonText = null;

        [SerializeField]
        private string runningSessionOKButtonText = "Continue session";

        [SerializeField]
        private TMP_Dropdown sessionDropDown = null;

        [SerializeField]
        private SettingsFileManager settingsFileManager = null;

        #endregion Fields

        #region Events

        public static event EventHandler OnQuitButtonPressed;

        public static event EventHandler OnSettingsConfirmedDuringSession;

        public static event EventHandler<SessionDefinition> OnSettingsSelected;

        #endregion Events

        #region Properties

        public ICustomLogger Logger { get; set; }

        #endregion Properties

        #region Public Methods

        public void OnOkButtonPressed()
        {
            if (Session.instance.InTrial)
            {
                OnSettingsConfirmedDuringSession?.Invoke(this, EventArgs.Empty);
                return;
            }
            string sessionName = sessionDropDown.options[sessionDropDown.value].text;
            SessionDefinition sessionDef = settingsFileManager.SessionDefinitions[sessionName].ConvertToSO();
            OnSettingsSelected?.Invoke(this, sessionDef);
        }

        public void OnQuitBtnPressed()
        {
            OnQuitButtonPressed?.Invoke(this, EventArgs.Empty);
        }

        #endregion Public Methods

        #region Private Methods

        private void OnEnable()
        {
            if (Session.instance == null)
            {
                return;
            }
            if (Session.instance.InTrial)
            {
                sessionDropDown.interactable = false;
                okButtonText.text = runningSessionOKButtonText;
                return;
            }
            sessionDropDown.interactable = true;
            okButtonText.text = defaultOkButtonText;
        }

        private void Start()
        {
            Cursor.visible = true;
            Cursor.lockState = CursorLockMode.Confined;

            UpdateDropdown(sessionDropDown, settingsFileManager.SessionDefinitions.Keys.ToList());
        }

        private void UpdateDropdown(TMP_Dropdown dropdown, List<string> items)
        {
            List<TMP_Dropdown.OptionData> options = new List<TMP_Dropdown.OptionData>();
            foreach (var item in items)
            {
                options.Add(new TMP_Dropdown.OptionData(item));
            }
            dropdown.ClearOptions();
            dropdown.AddOptions(options);
        }

        #endregion Private Methods
    }
}