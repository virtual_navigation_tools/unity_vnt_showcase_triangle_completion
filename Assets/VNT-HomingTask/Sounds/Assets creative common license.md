We provide two sound assets, which are under a creative common license:

# Campfire:
"_campfire medium slight forest slap echo.flac_" by kyles is marked with CC0 1.0.

To view the terms, visit [CC0 1.0](https://creativecommons.org/publicdomain/zero/1.0/?ref=openverse)

# Steps:
"_Footsteps on leaves.wav_" by ceberation is licensed under CC BY 3.0.

To view a copy of this license,
 visit [CC BY 3.0](https://creativecommons.org/licenses/by/3.0/?ref=openverse)
