# -*- coding: utf-8 -*-
"""
:AUTHORS: Martin M. Müller
:ORGANIZATION: Department of Neurobiology, Bielefeld University
:CONTACT: martin.mueller@uni-bielefeld.de
:SINCE: Mon Aug  8 10:09:40 2022
"""
# external tools
import pandas as pd
import seaborn as sns
import scipy
import matplotlib.pyplot as plt
from IPython import get_ipython


from numpy import var, mean, sqrt
from pandas import Series

# from stackoverflow
def cohend(d1: Series, d2: Series) -> float:

    # calculate the size of samples
    n1, n2 = len(d1), len(d2)

    # calculate the variance of the samples
    s1, s2 = var(d1, ddof=1), var(d2, ddof=1)

    # calculate the pooled standard deviation
    s = sqrt(((n1 - 1) * s1 + (n2 - 1) * s2) / (n1 + n2 - 2))

    # calculate the means of the samples
    u1, u2 = mean(d1), mean(d2)

    # return the effect size
    return (u1 - u2) / s

"""
####################### clear workspace #######################################
"""
plt.close("all")
get_ipython().magic("reset -sf")

"""
####################### toggle stats ##########################################
"""
do_stats = False

"""
####################### prep vars #############################################
"""
## TODO: package data for publication and update path here accordingly
root = "D://Data//Work//MMXX_results//"
# participant IDs
IDs = [
    "MM02_AK01",
    "MM02_AK05",
    "MM02_AK06",
    "MM02_AK08",
    "MM02_AK10",
    "MM02_AK11",
    "MM02_AK13",
    "MM02_AK14",
]
# experimental conditions ("trial_name" column in dataframe)
conditions = [
    "1.5_left",
    "1.5_right",
    "2.5_left",
    "2.5_right",
    "4_left",
    "4_right",
    "7_left",
    "7_right",
]
speeds = [1.5, 2.5, 4.0, 7.0]
measures = ["err_pos_p1", "err_pos_p2", "err_pos_walk"]
measure_names = ["1st pointing", "2nd pointing", "trajectory end"]

# load data
# csv file contains excel metatag (sep=,) in first line, hence we skip one row
df_joined = pd.read_csv("speed_data_joined.csv", skiprows=1)

"""
####################### check for possible turn direction effects #############
"""
data_right = df_joined[df_joined["is_right"] == True]
data_left = df_joined[df_joined["is_right"] == False]

if (do_stats):   
    # omnibus test across all conditions for all three performance measures
    print("results for kruskal wallis omnibus test left vs. right:")
    for measure in measures:
        kruskal = scipy.stats.kruskal(
            *[group[measure].values for name, group in df_joined.groupby("trial_name")]
        )
        print(f"   ==> kruskal {measure}: p:{kruskal.pvalue}, H:{kruskal.statistic}")
    # u test left vs right for all three performance measures
    print("results for u-test left vs. right:")
    for measure in measures:
        utest = scipy.stats.mannwhitneyu(data_left[measure], data_right[measure])
        print(f"   ==> p-val left unequal right for {measure}: {utest.pvalue}")
"""
####################### create medians df #####################################
"""
# create dataframe with medians for each participant
df_medians = pd.DataFrame(columns=["ppid", "velocity", "measure", "value"])
for ppid in IDs:
    for velo in speeds:
        for measure in measures:
            # get median value
            _slice = df_joined.loc[
                (df_joined["ppid"] == ppid) & (df_joined["velocity"] == velo)
            ][measure]
            value = _slice.median()
            # assign to output
            df_temp = pd.DataFrame(
                [[ppid, velo, measure, value]], columns=df_medians.columns
            )
            df_medians = pd.concat([df_medians, df_temp], ignore_index=True)

"""
####################### compute pairwise cohen's d ############################
"""
if (do_stats):   
    pairs = [(1.5,2.5),(1.5,4),(1.5,7),(2.5,4),(2.5,7),(4,7)]
    for (c1,c2) in pairs:
        d1 = df_medians.loc[(df_medians["velocity"] == c1) & (df_medians["measure"] == "err_pos_walk")]
        d2 = df_medians.loc[(df_medians["velocity"] == c2) & (df_medians["measure"] == "err_pos_walk")]
        coh_d = cohend(d2.value, d1.value)
        print(f"cohen's d for {c1} vs. {c2}: {coh_d}")
"""
####################### create overview figure ################################
"""
# prep figure
fig1 = plt.figure(figsize=(12,8))
ax = fig1.gca()
sns.boxplot(
    ax=ax,
    x="velocity",
    y="value",
    hue="measure",
    data=df_medians,
    notch=True,
    palette="light:salmon",
)
# format for readability
ax.tick_params(axis="both", which="major", labelsize=16)
ax.tick_params(axis="both", which="minor", labelsize=12)
ax.set_ylabel("position error [vm]", fontsize=20)
ax.set_xlabel("avatar translation velocity [vm/s]", fontsize=20)
handles, labels = ax.get_legend_handles_labels()
ax.legend(handles=handles, labels=measure_names)

if (do_stats):   
    # do kruskal to check if measures differ
    print("results for kruskal across measures (median data):")
    kruskal_measure = scipy.stats.kruskal(
        *[group.values for name, group in df_medians.groupby("measure")["value"]]
    )
    print(f"   ==> p:{kruskal_measure.pvalue}, H:{kruskal_measure.statistic}")

"""
####################### save 2.5m/s data for comparison with other dataset ####
"""
df_medians[df_medians.velocity == 2.5].to_csv("0_obj_comparison_data.csv", index=False)