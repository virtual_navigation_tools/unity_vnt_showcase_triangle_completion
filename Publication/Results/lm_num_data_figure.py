# -*- coding: utf-8 -*-
"""
:AUTHORS: Martin M. Müller
:ORGANIZATION: Department of Neurobiology, Bielefeld University
:CONTACT: martin.mueller@uni-bielefeld.de
:SINCE: Mon Aug  8 13:16:10 2022
"""
# external tools
import pandas as pd
import seaborn as sns
import scipy
import matplotlib.pyplot as plt
from statannotations.Annotator import Annotator
from IPython import get_ipython


from numpy import var, mean, sqrt
from pandas import Series

# from https://stackoverflow.com/questions/21532471/how-to-calculate-cohens-d-in-python
def cohend(d1: Series, d2: Series) -> float:

    # calculate the size of samples
    n1, n2 = len(d1), len(d2)

    # calculate the variance of the samples
    s1, s2 = var(d1, ddof=1), var(d2, ddof=1)

    # calculate the pooled standard deviation
    s = sqrt(((n1 - 1) * s1 + (n2 - 1) * s2) / (n1 + n2 - 2))

    # calculate the means of the samples
    u1, u2 = mean(d1), mean(d2)

    # return the effect size
    return (u1 - u2) / s

def remove_failed_trials(df):
    _len = len(df)
    df = df[df.goalEstimateY_ptrial1 != -99]
    df = df[df.goalEstimateY_ptrial2 != -99]
    df = df[df.goalEstimateY != -99]
    print(f"   ==> dropped {_len - len(df)} rows: trial failed")
    return df


def fix_ID_column(df, correct_ppid):
    if df["ppid"].unique() != correct_ppid:
        print(
            f"   ==> fixing ppid column for dataset {correct_ppid}. WARNING: derived columns will still have old ID"
        )
        df["ppid"] = correct_ppid
    return df


"""
####################### clear workspace #######################################
"""
plt.close("all")
get_ipython().magic("reset -sf")

"""
####################### toggle stats ##########################################
"""
do_stats = False

"""
####################### prep vars #############################################
"""
# participant IDs
IDs = [
    "MM03_RL01",
    "MM03_RL06",
    "MM03_RL08",
    "MM03_RL10",
    "MM03_RL11",
    "MM03_RL12",
    "MM03_RL13",
    "MM03_RL14",
    "MM03_RL15",
    "MM03_RL16",
]
# experimental conditions ("trial_name" column in dataframe)
conditions = [
    "1obj_left",
    "1obj_right",
    "2obj_left",
    "2obj_right",
    "3obj_left",
    "3obj_right",
    "manyobj_left",
    "manyobj_right",
]
num_lm = [1, 2, 3]
measures = ["err_pos_p1", "err_pos_p2", "err_pos_walk"]
measure_names = ["1st pointing", "2nd pointing", "trajectory end"]

# load data
# csv file contains excel metatag (sep=,) in first line, hence we skip one row
df_joined = pd.read_csv("lm_num_data_joined.csv", skiprows=1)

"""
####################### create medians df #####################################
"""
# create dataframe with medians for each participant
df_medians = pd.DataFrame(columns=["ppid", "num_lm", "measure", "value"])
for ppid in IDs:
    for num in num_lm:
        for measure in measures:
            # get median value
            _slice = df_joined.loc[
                (df_joined["ppid"] == ppid) & (df_joined["num_lm"] == num)
            ][measure]
            value = _slice.median()
            # assign to output
            df_temp = pd.DataFrame(
                [[ppid, num, measure, value]], columns=df_medians.columns
            )
            df_medians = pd.concat([df_medians, df_temp], ignore_index=True)
"""
####################### check for possible turn direction effects #############
"""
data_right = df_joined[df_joined["is_right"] == True]
data_left = df_joined[df_joined["is_right"] == False]

if (do_stats):   
    # omnibus test across all conditions for all three performance measures
    print("results for kruskal wallis omnibus test left vs. right:")
    for measure in measures:
        kruskal = scipy.stats.kruskal(
            *[group[measure].values for name, group in df_joined.groupby("trial_name")]
        )
        print(f"   ==> kruskal {measure}: p:{kruskal.pvalue}, H:{kruskal.statistic}")
    
    # post hoc pairwise checks for each condition
    for (measure,name) in zip(measures,measure_names):
        fig_tmp = plt.figure(f"left vs right: {name}")
        ax = fig_tmp.gca()
        sns.boxplot(
            ax=ax, x="num_lm", y=measure,hue ="is_right", data=df_joined, notch=True, palette="light:b",
        )
    
        # tutorial for annotator: https://github.com/trevismd/statannotations/blob/master/usage/example.ipynb
        pairs = [((1, True), (1, False)),
                 ((2, True), (2, False)),
                 ((3, True), (3, False)),
                 ((123, True), (123, False))]
        annotator = Annotator(ax, pairs, data=df_joined, x="num_lm", y=measure, hue="is_right")
        annotator.configure(comparisons_correction="BH", test='Mann-Whitney', text_format='star', loc='inside')
        annotator.apply_and_annotate()
    
    
    # u test left vs right for all three performance measures
    print("results for u-test left vs. right:")
    for measure in measures:
        utest = scipy.stats.mannwhitneyu(data_left[measure], data_right[measure])
        print(f"   ==> p-val left unequal right for {measure}: {utest.pvalue}")
"""
####################### compute pairwise cohen's d ############################
"""
if (do_stats):
    pairs = [(1,2),(1,3),(2,3)]
    for (c1,c2) in pairs:
        d1 = df_medians.loc[(df_medians["num_lm"] == c1) & (df_medians["measure"] == "err_pos_walk")]
        d2 = df_medians.loc[(df_medians["num_lm"] == c2) & (df_medians["measure"] == "err_pos_walk")]
        coh_d = cohend(d1.value, d2.value)
        print(f"cohen's d for {c1} vs. {c2}: {coh_d}")

"""
####################### load 0 obj data for comparison ########################
"""
# csv file contains excel metatag (sep=,) in first line, hence we skip one row
vel_data = pd.read_csv("0_obj_comparison_data.csv", skiprows=0)
vel_data["num_lm"] = int(0)
num_lm = [0, 1, 2, 3]



"""
####################### create overview figure ################################
"""
# reformat data for plot
df_plot = df_medians.append(vel_data)

# prep figure
fig1 = plt.figure(figsize=(12,8))
ax = fig1.gca()


sns.boxplot(
    ax=ax,
    x="num_lm",
    y="value",
    hue="measure",
    data=df_plot,
    notch=True,
    palette="light:seagreen",
)


# format for readability
ax.tick_params(axis="both", which="major", labelsize=16)
ax.tick_params(axis="both", which="minor", labelsize=12)
ax.set_ylabel("position error [vm]", fontsize=20)
ax.set_xlabel("number of landmarks in the scene", fontsize=20)
handles, labels = ax.get_legend_handles_labels()
ax.legend(handles=handles, labels=measure_names)

"""
####################### measure comparisons ###################################
"""
if (do_stats):
    # do kruskal to check if measures differ
    print("results for kruskal across measures (median data):")
    kruskal_measure = scipy.stats.kruskal(
        *[group.values for name, group in df_medians.groupby("measure")["value"]]
    )
    print(f"   ==> p:{kruskal_measure.pvalue}, H:{kruskal_measure.statistic}")

"""
####################### create second figure from which to crop annotations ###
"""
if(do_stats):
    # prep figure
    fig2 = plt.figure()
    ax = fig2.gca()
    sns.boxplot(
        ax=ax, x="num_lm", y="value", data=df_medians, notch=True, palette="light:b",
    )
    
    # tutorial for annotator: https://github.com/trevismd/statannotations/blob/master/usage/example.ipynb
    pairs = [(num_lm[0], num_lm[1]),
             (num_lm[1], num_lm[2]),
             (num_lm[0], num_lm[2])]
    annotator = Annotator(ax, pairs, data=df_medians, x="num_lm", y="value")
    annotator.configure(comparisons_correction="BH", test='Mann-Whitney', text_format='star', loc='inside')
    annotator.apply_and_annotate()
    
    # format for readability
    ax.tick_params(axis="both", which="major", labelsize=16)
    ax.tick_params(axis="both", which="minor", labelsize=12)
    ax.set_ylabel("position error [vm]", fontsize=20)
    ax.set_xlabel("number of landmarks in the scene", fontsize=20)