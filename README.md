<img src="https://gitlab.ub.uni-bielefeld.de/virtual_navigation_tools/unity_vnt_showcase_triangle_completion/-/raw/main/vnt_logo_hex_showcase.svg" align="right" width="550px"/>


# Showcase Repository

<br>

This is the project repository for a virtual triangle completion task developed using the [VNT](https://gitlab.ub.uni-bielefeld.de/virtual_navigation_tools/unity_vnt_main).


In this repository you will find


1) A unity project to run the virtual triangle completion task,

2) settings files to modify the parameters of the task,

3) anonymised results of the showcase study, as presented in our recent publication in PLOS ONE at https://doi.org/10.1371/journal.pone.0293536
</div>
The unity project serves as a showcase for implementing virtual navigation experiments using the VNT and can be used as a template for other similar tasks, like the one presented in our [recent paper on navigation in cluttered environments](https://www.frontiersin.org/articles/10.3389/fnbeh.2024.1399716/full).


<br>
<br>

### Visit the [Wiki](https://gitlab.ub.uni-bielefeld.de/virtual_navigation_tools/unity_vnt_showcase_triangle_completion/-/wikis/home) to learn more about the showcase repository!
<br clear="right"/>
